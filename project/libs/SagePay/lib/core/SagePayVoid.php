<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayVoid
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayVoid extends SagePayTransaction
{
	/**
	 * _vpsTxId
	 * 
	 * The VPS Transaction ID for the transaction which was sent back by the Sage Pay system when the original transaction was registered.
	 * 
	 * @var string; max 38 characters;
	 * @access private
	 */
	private $_vpsTxId;
	
	/**
	 * _securityKey
	 * 
	 * The SecurityKey of the original transaction.
	 * 
	 * @var string; max 10 characters;
	 * @access private
	 */
	private $_securityKey;
	
	/**
	 * _txAuthNo
	 * 
	 * The TxAuthNo of the original transaction as returned by the Sage Pay system.
	 * 
	 * @var long int;
	 * @access private
	 */
	private $_txAuthNo;
	
	/**
     * __construct 
     * 
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::__construct("VOID");
	}
	
	/**
     * getVPSTxId 
     * 
     * @access public
     * @return string
     */
	public function getVPSTxId()
	{
		return $this->_vpsTxId;
	}
	
	/**
     * getSecurityKey 
     * 
     * @access public
     * @return string
     */
	public function getSecurityKey()
	{
		return $this->_securityKey;
	}
	
	/**
     * getSTxAuthNo 
     * 
     * @access public
     * @return long int
     */
	public function getTxAuthNo()
	{
		return $this->_txAuthNo;
	}
	
	/**
     * setVPSTxId 
     * 
     * @param string $vpsTxId
     * @access public
     * @return void
     */
	public function setVPSTxId($vpsTxId)
	{
		$this->_vpsTxId = $vpsTxId;
	}
	
	/**
     * setSecurityKey 
     * 
     * @param string $securityKey
     * @access public
     * @return void
     */
	public function setSecurityKey($securityKey)
	{
		$this->_securityKey = $securityKey;
	}
	
	/**
     * setTxAuthNo 
     * 
     * @param long int $txAuthNo
     * @access public
     * @return void
     */
	public function setTxAuthNo($txAuthNo)
	{
		$this->_txAuthNo = $txAuthNo;
	}
	
	/**
     * setVoidDetails 
     * 
     * @param string $vpsTxId 
     * @param string $securityKey
     * @param long int $txAuthNo 
     * @access public
     * @return void
     */
	public function setVoidDetails($vpsTxId, $securityKey, $txAuthNo)
	{
		$this->_vpsTxId = $vpsTxId;
		$this->_securityKey = $securityKey;
		$this->_txAuthNo = $txAuthNo;
	}
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{
		$this->validateDetails();
		$string = parent::getPreparedData();
		$string .= '&VPSTxId='.urlencode($this->_vpsTxId);		
		$string .= '&SecurityKey='.urlencode($this->_securityKey);
		$string .= '&TxAuthNo='.urlencode($this->_txAuthNo);
		return $string;
	}

	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
		try 
		{
			if(!isset($this->_vpsTxId))
			{
				throw new exception ("VPSTxId is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_vpsTxId, 38))
			{
				throw new exception ("VPSTxId must be upto 38 characters");
			}
			
			if(!isset($this->_securityKey))
			{
				throw new exception ("SecurityKey is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_securityKey, 10))
			{
				throw new exception ("SecurityKey must be upto 10 characters");
			}
			
			if(!isset($this->_txAuthNo))
			{
				throw new exception ("TxAuthNo is not set"); 
			}
			
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
}
?>