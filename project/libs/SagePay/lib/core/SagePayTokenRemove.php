<?php 
/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    SagePayToken
 * @author     Nikolai Senkevich nikolais@madesimplegroup.com
 * @version    2011-05-28
 */

class SagePayTokenRemove extends SagePayTransaction
{	
	
    /**
     *
     * @var string 
     */
	private $_token;
	
	/**
     * __construct 
     * 
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::__construct("REMOVETOKEN");	
	}
	
	
	/**
     * getToken 
     * 
     * @access public
     * @return string
     */
	public function getToken()
	{
		return $this->_token;		
	}
	
	/**
     *
     * @param type $token 
     */
	public function setToken($token)
	{
		$this->_token = $token;
	}
	
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{		
	 	$this->validateDetails();	 	
	 	$string = parent::getPreparedData();
	 	$string .= "&Token=".urlencode($this->_token);	
	 	return $string;
	}

	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
	 	try 
		{			
			if(!isset($this->_token))
			{
				throw new exception ("Token is not set"); 
			}
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
}
?>