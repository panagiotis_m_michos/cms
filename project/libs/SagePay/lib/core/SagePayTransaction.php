<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayTransaction
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayTransaction
{
	/**
	 * _txType
	 * 
	 * Transaction type
	 * 
	 * @var string; max 15 characters;
	 * @access private
	 */
	private $_txType;
	
	/**
	 * _vendorTxCode
	 * 
	 * Unique Vendor Transaction Code
	 * 
	 * @var string; max 40 characters
	 * @access private
	 */
	private $_vendorTxCode;
	
	/**
     * __construct 
     * 
     * @param string $txType
     * @access public
     * @return void
     */
	public function __construct($txType)
	{		
		$this->setTxType($txType);
		$this->_vendorTxCode = self::generateVendorCode();		
	}
	
	/**
     * getTxType 
     * 
     * @access public
     * @return string
     */
	public function getTxType()
	{
		return $this->_txType;
	}
	
	/**
     * getVendorCode 
     * 
     * @access public
     * @return string
     */
	public function getVendorCode()
	{
		return $this->_vendorTxCode;
	}
	
	/**
     * setVendorCode 
     * 
     * @param string $vendorTxCode
     * @access public
     * @return void
     */
	public function setVendorCode($vendorTxCode)
	{
		if(SagePayDirect::validateLength($vendorTxCode, 40))
		{
			$this->_vendorTxCode = $vendorTxCode;
		}
		else
		{
			trigger_error("Vendor code must be upto 40 characters", E_USER_NOTICE);
		}
	}
	
	/**
     * setTxType 
     * 
     * @param string $txType
     * @access public
     * @return void
     */
	public function setTxType($txType)
	{
		$txType = strtoupper($txType);
		if($txType != ("PAYMENT"||"DEFERRED"||"AUTHENTICATE"||"RELEASE"||"ABORT"||"REFUND"||"REPEAT"||"REPEATDEFERRED"||"VOID"||"MANUAL"||"DIRECTREFUND"||"AUTHORISE"||"CANCEL"||"TOKEN"||"REMOVETOKEN"))
		{
			trigger_error('This type of action is not valid', E_USER_NOTICE);
		}
		else
		{
			$this->_txType = $txType;
		}
	}
	
	/**
     * generateVendorCode 
     * 
     * @static
     * @access private
     * @return string
     */
	private static function generateVendorCode()
	{
		$time =  date ("Y-m-d H:i:s");
		$code = 'REF'.md5($time).mt_rand();
		$code = SagePayDirect::cutString($code, 40);
		return $code;		
	}
	
	/**
     * getPreparedData
     * 
     * gets data for sending to SagePay
     * 
     * @access protected
     * @return string
     */
	protected function getPreparedData()
	{
		$string = "&TxType=".urlencode($this->_txType);
		$string .= "&VendorTxCode=".urlencode($this->_vendorTxCode);
		return $string;
	}
	
	/**
     * addItem2Basket
     * 
     * @param string $description
     * @param int $quantity
     * @param mixed $$itemValue
     * @param mixed $itemTax
     * @param mixed $itemTotal
     * @param float $lineTotal 
     * @access protected
     * @return string
     */
	protected function addItem2Basket($description, $quantity, $itemValue, $itemTax, $itemTotal, $lineTotal)
	{				
		return ':'.$description.
			   ':'.isset($quantity)? $quantity : ''.
			   ':'.isset($itemValue)? $itemValue : ''.
			   ':'.isset($itemTax)? $itemTax : ''.
			   ':'.isset($itemTotal)? $itemTotal : ''.
			   ':'.isset($lineTotal)? $lineTotal : round($quantity*$itemTotal, 2);
	}
	
	
}
?>