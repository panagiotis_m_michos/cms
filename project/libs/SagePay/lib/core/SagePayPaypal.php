<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayPaypal
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayPaypal extends SagePayTransaction
{	
	
	/**
	 * _amount
	 * 
	 * Amount of the payment.
	 * 
	 * @var float; 1.00 to 100,000.00
	 * @access private
	 */
	private $_amount;
	
	/**
	 * _currency
	 * 
	 * Three-letter currency code to ISO 4217 e.g: �GBP�, �EUR� and �USD�
	 * 
	 * @var string; max 3 characters
	 * @access private
	 */	
	private $_currency = SagePayDirect::SAGEPAY_CURRENCY;
	
	/**
	 * _description
	 * 
	 * Free text description of the payment
	 * 
	 * @var string; max 100 characters
	 * @access private
	 */
	private $_description;
	
	/**
	 * _paypalCallbackURL
	 * 
	 * Full qualified domain name of the URL to which customers are redirected upon completion of a PayPal transaction
	 *
	 * @var string; max 255 characters;
	 * @access private
	 */
	private $_paypalCallbackURL;
	
	/**
	 * _customerEMail
	 * 
	 * The customer's e-mail address.
	 *
	 * @var string; max 255 characters; OPTIONAL
	 * @access private
	 */
	private $_customerEMail;
	
	/**
	 * _basket
	 * 
	 * basket details
	 *
	 * @var string; max 7500 characters; OPTIONAL
	 * @access private
	 */
	private $_basket;
	
	/**
	 * _giftAidPayment
	 * 
	 * 0 = This transaction is not a Gift Aid charitable donation (default)
	 * 1 = This payment is a Gift Aid charitable donation and the customer has AGREED to donate the tax.
	 *
	 * @var boolean; OPTIONAL
	 */
	private $_giftAidPayment;
	
	/**
	 * _clientIPAddress
	 * 
	 * The IP address of the client connecting to your server making the payment.
	 *
	 * @var string; max 15 characters; OPTIONAL
	 * @access private
	 */
	private $_clientIPAddress;
	
	/**
	 * _deliveryAddress
	 * 
	 * Delivery address details
	 * 
	 * @var object; OPTIONAL
	 * @access private
	 */	
	private $_deliveryAddress;
	
	/**
	 * _billingAddress
	 * 
	 * Billing address details
	 * 
	 * @var object; OPTIONAL
	 * @access private
	 */
	private $_billingAddress;
	
	/**
     * __construct 
     * 
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::__construct("PAYMENT");		
	}
	
	/**
     * getAmount 
     * 
     * @access public
     * @return float
     */
	public function getAmount()
	{
		return $this->_amount;
	}

	/**
     * getCurrency 
     * 
     * @access public
     * @return string
     */
	public function getCurrency()
	{
		return $this->_currency;
	}
	
	/**
     * getDescription 
     * 
     * @access public
     * @return string
     */
	public function getDescription()
	{
		return $this->_description;
	}
	
	/**
     * getPaypalCallbackURL 
     * 
     * @access public
     * @return string
     */
	public function getPaypalCallbackURL()
	{
		return $this->_paypalCallbackURL;
	}
	
	/**
     * getCustomerEMail 
     * 
     * @access public
     * @return string
     */
	public function getcustomerEMail()
	{
		return $this->_customerEMail;
	}
	
	/**
     * getBasket 
     * 
     * @access public
     * @return string
     */
	public function getBasket()
	{
		return $this->_basket;
	}
	
	/**
     * getGiftAidPayment 
     * 
     * @access public
     * @return boolean
     */
	public function getGiftAidPayment()
	{
		return $this->_giftAidPayment;
	}
	
	/**
     * getClientIPAddress 
     * 
     * @access public
     * @return string
     */
	public function getClientIPAddress()
	{
		return $this->_clientIPAddress;
	}
	
	/**
     * getDeliveryAddress 
     * 
     * @access public
     * @return object
     */
	public function getDeliveryAddress()
	{
		return $this->_deliveryAddress;
	}
	
	/**
     * getBillingAddress 
     * 
     * @access public
     * @return object
     */
	public function getBillingAddress()
	{
		return $this->_billingAddress;
	}
	
	/**
     * setAmount 
     * 
     * @param float $amount
     * @access public
     * @return void
     */
	public function setAmount($amount)
	{
		$this->_amount = $amount;
	}

	/**
     * setCurrency 
     * 
     * @param string $currency
     * @access public
     * @return void
     */
	public function setCurrency($currency)
	{
		$this->_currency = $currency;
	}
	
	/**
     * setDescription 
     * 
     * @param string $description
     * @access public
     * @return void
     */
	public function setDescription($description)
	{
		$this->_description = $description;
	}
	
	/**
     * setPaypalCallbackURL 
     * 
     * @param string $url
     * @access public
     * @return void
     */
	public function setPaypalCallbackURL($url)
	{
		$this->_paypalCallbackURL = $url;
	}
	
	/**
     * setCustomerEMail 
     * 
     * @param string $email
     * @access public
     * @return void
     */
	public function setCustomerEMail($email)
	{
		$this->_customerEMail = $email;
	}
	
	/**
     * setBasket 
     * 
     * @param array $basket
     * @access public
     * @return void
     */
	public function setBasket($basket)
	{
		$array = array();
		foreach($basket as $value)
		{			
			$array[] = parent::addItem2Basket($value['description'], 
											  isset($value['quantity'])? $value['quantity'] : null, 
											  isset($value['itemValue'])? $value['itemValue'] : null, 
											  isset($value['itemTax'])? $value['itemTax'] : null, 
											  isset($value['itemTotal'])? $value['itemTotal'] : null,
											  isset($value['lineTotal'])? $value['lineTotal'] : null);
		}
		
		$this->_basket = (string)count($array);
		foreach ($array as $value)
		{
			$this->_basket .= $value;
		}
	}
	
	/**
     * setGiftAidPayment 
     * 
     * @param boolean $giftAidPayment
     * @access public
     * @return void
     */
	public function setGiftAidPayment($giftAidPayment)
	{
		$this->_giftAidPayment = $giftAidPayment;
	}
	
	/**
     * setClientIPAddress 
     * 
     * @access public
     * @return void
     */
	public function setClientIPAddress()
	{
		$this->_clientIPAddress = $_SERVER['REMOTE_ADDR'];
	}
	
	/**
     * setDeliveryAddress 
     * 
     * @param string $firstnames 
     * @param string $surname
     * @param string $line1
     * @param string $city
     * @param string $postcode
     * @param string $country 
     * @access public
     * @return void
     */	
	public function setDeliveryAddress($firstnames, $surname, $line1, $city, $postcode, $country)
	{
		require_once("SagePayAddress.php");
		$this->_deliveryAddress = new SagePayAddress();
		$this->_deliveryAddress->setAddressDetails($firstnames, $surname, $line1, $city, $postcode, $country);
	}
	
	/**
     * setBillingAddress 
     * 
     * @param string $firstnames 
     * @param string $surname
     * @param string $line1
     * @param string $city
     * @param string $postcode
     * @param string $country 
     * @access public
     * @return void
     */	
	public function setBillingAddress($firstnames, $surname, $line1, $city, $postcode, $country)
	{
		require_once("SagePayAddress.php");
		$this->_billingAddress = new SagePayAddress();
		$this->_billingAddress->setAddressDetails($firstnames, $surname, $line1, $city, $postcode, $country);
	}
	
	/**
     * setPaypalDetails 
     * 
     * sets action's compulsory data
     * 
     * @param string $description 
     * @param foat $amount   
     * @param string $paypalCallbackURL  
     * @access public
     * @return void
     */
	public function setPaypalDetails($description, $amount, $paypalCallbackURL)
	{
		$this->_description = $description;
		$this->_amount = $amount;		
		$this->_paypalCallbackURL = $paypalCallbackURL;
	}
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{
		$this->validateDetails();
		$string = parent::getPreparedData();
		$string .= "&Amount=".urlencode($this->_amount);
	 	$string .= "&Currency=".urlencode($this->_currency);
	 	$string .= "&Description=".urlencode($this->_description);
	 	$string .= "&CardType=PAYPAL";
	 	$string .= "&PayPalCallbackURL=".urlencode($this->_paypalCallbackURL);
		if(isset($this->_basket))
		{			
			$string .= "&Basket=".urlencode($this->_basket);		
		}
		if(isset($this->_customerEMail))
		{
			$string .= "&CustomerEMail=".urlencode($this->_customerEMail);
		}
		if(isset($this->_clientIPAddress))
		{
			$string .= "&ClientIPAddress=".urlencode($this->_clientIPAddress);  
		}
		if(isset($this->_giftAidPayment))
		{
			$string .= "&GiftAidPayment=".urlencode($this->_giftAidPayment);
		}
		if(isset($this->_accountType))
		{
			$string .= "&AccountType=".urlencode($this->_accountType);
		}
		if(isset($this->_deliveryAddress))
		{
			$this->_deliveryAddress->validateDetails();
			$string .= "&DeliverySurname=".urlencode($this->_deliveryAddress->getSurname());
			$string .= "&DeliveryFirstnames=".urlencode($this->_deliveryAddress->getFirstnames());
			$string .= "&DeliveryAddress1=".urlencode($this->_deliveryAddress->getLine1());
			$string .= "&DeliveryAddress2=".urlencode($this->_deliveryAddress->getLine2());
			$string .= "&DeliveryCity=".urlencode($this->_deliveryAddress->getCity());
			$string .= "&DeliveryPostCode=".urlencode($this->_deliveryAddress->getPostcode());
			$string .= "&DeliveryCountry=".urlencode($this->_deliveryAddress->getCountry());
			$string .= "&DeliveryState=".urlencode($this->_deliveryAddress->getState());		
			$string .= "&DeliveryPhone=".urlencode($this->_deliveryAddress->getPhone());			
		}
		if(isset($this->_billingAddress))
		{
			$this->_billingAddress->validateDetails();
			$string .= "&BillingFirstnames=".urlencode($this->_billingAddress->getFirstnames());
			$string .= "&BillingSurname=".urlencode($this->_billingAddress->getSurname());
			$string .= "&BillingAddress1=".urlencode($this->_billingAddress->getLine1());
			$string .= "&BillingAddress2=".urlencode($this->_billingAddress->getLine2());
			$string .= "&BillingCity=".urlencode($this->_billingAddress->getCity());
			$string .= "&BillingPostCode=".urlencode($this->_billingAddress->getPostcode());
			$string .= "&BillingCountry=".urlencode($this->_billingAddress->getCountry());
			$string .= "&BillingState=".urlencode($this->_billingAddress->getState());		
			$string .= "&BillingPhone=".urlencode($this->_billingAddress->getPhone());			
		}		
		return $string;
	}
	
	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
		try 
		{
			if(!isset($this->_amount))
			{
				throw new exception ("Amount is not set"); 
			}
			elseif(($this->_amount < 1.00) || ($this->_amount > 100000.00))
			{
				throw new exception ("Amount must be more than 1.00 but less than 100,000.00");
			}
			
			if(!isset($this->_description))
			{
				throw new exception ("Description is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 100))
			{
				throw new exception ("Description must be upto 100 characters");
			}
			
			if(!isset($this->_currency))
			{
				throw new exception ("Currency is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 3))
			{
				throw new exception ("Currency must be upto 3 characters");
			}
			
			if(!isset($this->_paypalCallbackURL))
			{
				throw new exception ("Paypal Callback URL is not set");
			}
			elseif(SagePayDirect::validateLength($this->_paypalCallbackURL, 255))
			{
				throw new exception ("Paypal Callback URL must be upto 255 characters");
			}
			
			if(isset($this->_customerEMail) && SagePayDirect::validateLength($this->_customerEMail, 255))
			{			
			 	throw new exception ("Customer Email must be upto 255 characters");			
			}
			
			if(isset($this->_clientIPAddress) && SagePayDirect::validateLength($this->_clientIPAddress, 15))
			{				
				throw new exception ("Client IP Address must be upto 15 characters");
			}
			
			if(isset($this->_basket) && SagePayDirect::validateLength($this->_basket, 7500))
			{			
				throw new exception ("Basket content must be upto 7500 characters");
			}
			
			if(isset($this->_giftAidPayment) && ($this->_giftAidPayment != (0||1)))
			{
				throw new exception ("Gift Aid Payment type is not recognised");
			}			
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
	
}
?>