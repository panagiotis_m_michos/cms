<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayDirectRefund
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SapePayDirectRefund extends SagePayTransaction
{
	/**
	 * _amount
	 * 
	 * Amount for the transaction
	 * 
	 * @var float with a range 1.00 to 100,000.00;
	 * @access private
	 */
	private $_amount;
	
	/**
	 * _currency
	 * 
	 * Three-letter currency code to ISO 4217 e.g: �GBP�, �EUR� and �USD�
	 * 
	 * @var string; max 3 characters
	 * @access private
	 */
	private $_currency = SagePayDirect::SAGEPAY_CURENCY;
	
	/**
	 * _description
	 * 
	 * Free text description of the manual payment.
	 * 
	 * @var string; max 100 characters
	 * @access private
	 */
	private $_description;
	
	/**
	 * _card
	 * 
	 * Card details
	 * 
	 * @var object
	 * @access private
	 */
	private $_card;
	
	/**
	 * _accountType
	 * 
	 * Type of vendor's account; 
	 * E = Use the e-commerce merchant account (SagePay default).
	 * C = Use the continuous authority merchant account (if present).
	 * M = Use the mail order, telephone order account (if present).
	 * 
	 * @var char; OPTIONAL
	 * @access private
	 */	
	private $_accountType;	
	
	/**
     * __construct 
     *     
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::__construct("DIRECTREFUND");
		require_once("SagePayCard.php");
		$this->_card = new SagePayCard();		
	}
	
	/**
     * getAmount 
     * 
     * @access public
     * @return float
     */
	public function getAmount()
	{
		return $this->_amount;
	}
	
	/**
     * getCurrency 
     * 
     * @access public
     * @return string
     */
	public function getCurrency()
	{
		return $this->_currency;
	}
	
	/**
     * getDescription 
     * 
     * @access public
     * @return string
     */
	public function getDescription()
	{
		return $this->_description;
	}
	
	/**
     * getCard 
     * 
     * @access public
     * @return object
     */
	public function getCard()
	{
		return $this->_card;
	}
	
	/**
     * getAccountType 
     * 
     * @access public
     * @return char
     */
	public function getAccountType()
	{
		return $this->_accountType;
	}
	
	/**
     * setAmount 
     * 
     * @param float $amount
     * @access public
     * @return void
     */
	public function setAmount($amount)
	{
		$this->_amount = $amount;
	}
	
	/**
     * setCurrency 
     * 
     * @param string $currency
     * @access public
     * @return void
     */
	public function setCurrency($currency)
	{
		$this->_currency = $currency;
	}
	
	/**
     * setDescription 
     * 
     * @param string $description
     * @access public
     * @return void
     */
	public function setDescription($description)
	{
		$this->_description = $description;		
	}
	
	/**
     * setCard 
     * 
     * @param object $card
     * @access public
     * @return void
     */
	public function setCard($card)
	{
		$this->_card = $card;
	}
	
	/**
     * setDirectRefundDetails
     * 
     * sets Direct Refund compulsory details 
     * 
     * @param float $amount
     * @param string $description
     * @access public
     * @return void
     */
	public function setDirectRefundDetails($amount, $description)
	{
		$this->_amount = $amount;
		$this->_description = $description;
	}
	
	/**
     * setCardDetails 
     * 
     * @param string $cardholderName 
     * @param string $cardType
     * @param int $cardNumber
     * @param int $expiryDate 
     * @access public
     * @return void
     */
	public function setCardDetails($cardholderName, $cardType, $cardNumber, $expiryDate)
	{
		$this->_card->setCardDetails($cardholderName, $cardType, $cardNumber, $expiryDate);
	}
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{
		$this->validateDetails();
		$string = parent::getPreparedData();
		$string .= "&Amount=".urlencode($this->_amount);
	 	$string .= "&Currency=".urlencode($this->_currency);
	 	$string .= "&Description=".urlencode($this->_description);
		if(isset($this->_accountType))
		{
			$string .= "&AccountType=".urlencode($this->_accountType);
		}
		$string .= $this->_card->getPreparedData();
		return $string;
	}
	
	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
		try 
		{
			if(!isset($this->_amount))
			{
				throw new exception ("Amount is not set"); 
			}
			elseif(($this->_amount < 1.00) || ($this->_amount > 100000.00))
			{
				throw new exception ("Amount must be more than 1.00 but less than 100,000.00");
			}
			
			if(!isset($this->_description))
			{
				throw new exception ("Description is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 100))
			{
				throw new exception ("Description must be upto 100 characters");
			}
			
			if(!isset($this->_currency))
			{
				throw new exception ("Currency is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 3))
			{
				throw new exception ("Currency must be upto 3 characters");
			}
			
			if(isset($this->_accountType))
			{
				if($this->_accountType != ('E'||'C'||'M'))
				{
					throw new exception ("Unrecognised account type");
				}
			}
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
}
?>