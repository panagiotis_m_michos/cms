<?php

class SagePayTokenPayment extends SagePayTransaction
{

    /**
     * _amount
     *
     * Amount of the payment.
     *
     * @var float; 1.00 to 100,000.00
     * @access private
     */
    private $_amount;
    /**
     * _currency
     *
     * Three-letter currency code to ISO 4217 e.g: �GBP�, �EUR� and �USD�
     *
     * @var string; max 3 characters
     * @access private
     */
    private $_currency = SagePayDirect::SAGEPAY_CURRENCY;
    /**
     * _description
     *
     * Free text description of the payment
     *
     * @var string; max 100 characters
     * @access private
     */
    private $_description;

    /**
     * _card
     *
     * card details
     *
     * @var object of SagePayCard;
     * @access private
     */
    private $_token;

    /**
     * _storeToken
     *
     * card details
     *
     * @var object of SagePayCard;
     * @access private
     */
    private $_storeToken;

    /**
     * _cv2
     *
     * exstra 3 digit security
     *
     * @var object of SagePayCard;
     * @access private
     */
    private $_cv2;

    /**
     * $_billingAddress
     *
     * Billing address details
     *
     * @var object
     * @access private
     */
    private $_billingAddress;

    /**
     * _deliveryAddress
     *
     * Delivery address details
     *
     * @var object
     * @access private
     */
    private $_deliveryAddress;

    /**
     * _customerEMail
     *
     * The customer's e-mail address.
     *
     * @var string; max 255 characters; OPTIONAL
     * @access private
     */
    private $_customerEMail;

    /**
     * _basket
     *
     * basket details
     *
     * @var string; max 7500 characters; OPTIONAL
     * @access private
     */
    private $_basket;

    /**
     * _giftAidPayment
     *
     * 0 = This transaction is not a Gift Aid charitable donation (default)
     * 1 = This payment is a Gift Aid charitable donation and the customer has AGREED to donate the tax.
     *
     * @var boolean; OPTIONAL
     */
    private $_giftAidPayment;

    /**
     * _applyAVSCV2
     *
     * 0 = If AVS/CV2 enabled then check them. If rules apply, use rules
     * 1 = Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules. (Continued overleaf.....)
     * 2 = Force NO AVS/CV2 checks even if enabled on account.
     * 3 = Force AVS/CV2 checks even if not enabled for the account but DON’T apply any rules.
     *
     * @var boolean; OPTIONAL
     */
    private $_applyAVSCV2;

    /**
     * _apply3DSecure
     *
     * flag
     * 0 = If 3D-Secure checks are possible and rules allow, perform the checks and apply the authorisation rules (default).
     * 1 = Force 3D-Secure checks for this transaction only (if your account is 3D-enabled) and apply rules for authorisation.
     * 2 = Do not perform 3D-Secure checks for this transaction only and always authorise.
     * 3 = Force 3D-Secure checks for this transaction (if your account is 3D-enabled) but ALWAYS obtain an auth code, irrespective of rule base.
     *
     * @var int; OPTIONAL
     * @access private
     */
    private $_apply3DSecure;

    /**
     * _clientIPAddress
     *
     * The IP address of the client connecting to your server making the payment.
     *
     * @var string; max 15 characters; OPTIONAL
     * @access private
     */
    private $_clientIPAddress;

    /**
     * _accountType
     *
     * Type of vendor's account;
     * E = Use the e-commerce merchant account (default).
     * C = Use the continuous authority merchant account (if present).
     * M = Use the mail order, telephone order account (if present).
     *
     * @var char; OPTIONAL
     * @access private
     */
    private $_accountType;

    /**
     * __construct
     *
     * @access public
     * @return SagePayTokenPayment
     */
    public function __construct()
    {
        parent::__construct("PAYMENT");
        require_once("SagePayAddress.php");
        $this->_billingAddress = new SagePayAddress();
        $this->_deliveryAddress = new SagePayAddress();
    }

    /**
     * getAmount
     *
     * @access public
     * @return float
     */
    public function getAmount()
    {
        return $this->_amount;
    }

    /**
     * getCurrency
     *
     * @access public
     * @return string
     */
    public function getCurrency()
    {
        return $this->_currency;
    }

    /**
     * getDescription
     *
     * @access public
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * getToken
     *
     * @access public
     * @return object
     */
    public function getToken()
    {
        return $this->_token;
    }

    /**
     * getToken
     *
     * @access public
     * @return object
     */
    public function getStoreToken()
    {
        return $this->_storeToken;
    }

    /**
     * getCV2
     *
     * @access public
     * @return object
     */
    public function getCV2()
    {
        return $this->_cv2;
    }

    /**
     * getBillingAddress
     *
     * @access public
     * @return object
     */
    public function getBillingAddress()
    {
        return $this->_billingAddress;
    }

    /**
     * getDeliveryAddress
     *
     * @access public
     * @return object
     */
    public function getDeliveryAddress()
    {
        return $this->_deliveryAddress;
    }

    /**
     * getCustomerEMail
     *
     * @access public
     * @return string
     */
    public function getCustomerEMail()
    {
        return $this->_customerEMail;
    }

    /**
     * getBasket
     *
     * @access public
     * @return string
     */
    public function getBasket()
    {
        return $this->_basket;
    }

    /**
     * getGiftAidPayment
     *
     * @access public
     * @return boolean
     */
    public function getGiftAidPayment()
    {
        return $this->_giftAidPayment;
    }

    /**
     * getApplyAVSCV2
     *
     * @access public
     * @return boolean
     */
    public function getApplyAVSCV2()
    {
        return $this->_applyAVSCV2;
    }

    /**
     * getApply3DSecure
     *
     * @access public
     * @return int
     */
    public function getApply3DSecure()
    {
        return $this->_apply3DSecure;
    }

    /**
     * getClientIPAddress
     *
     * @access public
     * @return string
     */
    public function getClientIPAddress()
    {
        return $this->_clientIPAddress;
    }

    /**
     * getAccountType
     *
     * @access public
     * @return int
     */
    public function getAccountType()
    {
        return $this->_accountType;
    }

    /**
     * setAmount
     *
     * @param float $amount
     * @access public
     * @return void
     */
    public function setAmount($amount)
    {
        $this->_amount = $amount;
    }

    /**
     * setCurrency
     *
     * @param string $currency
     * @access public
     * @return void
     */
    public function setCurrency($currency)
    {
        $this->_currency = $currency;
    }

    /**
     * setDescription
     *
     * @param string $description
     * @access public
     * @return void
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }


    /**
     *
     * @param type $token
     */
    public function setToken($token)
    {
        $this->_token = $token;
    }

    /**
     *
     * @param $storeToken
     */
    public function setStoreToken($storeToken)
    {
        $this->_storeToken = $storeToken;
    }

    /**
     *
     * @param $cv2
     */
    public function setCV2($cv2)
    {
        $this->_cv2 = $cv2;
    }

    /**
     * @param SagePayAddress $address
     */
    public function setBillingAddress1(SagePayAddress $address)
    {
        $this->_billingAddress = $address;
    }

    /**
     * @param SagePayAddress $address
     */
    public function setDeliveryAddress1(SagePayAddress $address)
    {
        $this->_deliveryAddress = $address;
    }

    /**
     * setBillingAddress
     *
     * @param string $firstnames
     * @param string $surname
     * @param string $line1
     * @param string $city
     * @param string $postcode
     * @param string $country
     * @internal param null $state
     * @access public
     * @return void
     */
    public function setBillingAddress($firstnames, $surname, $line1, $city, $postcode, $country)
    {
        $this->_billingAddress->setAddressDetails($firstnames, $surname, $line1, $city, $postcode, $country);
    }

    /**
     * setDeliveryAddress
     *
     * @param string $firstnames
     * @param string $surname
     * @param string $line1
     * @param string $city
     * @param string $postcode
     * @param string $country
     * @access public
     * @return void
     */
    public function setDeliveryAddress($firstnames, $surname, $line1, $city, $postcode, $country)
    {
        $this->_deliveryAddress->setAddressDetails($firstnames, $surname, $line1, $city, $postcode, $country);
    }

    /**
     * setCustomerEMail
     *
     * @param string $email
     * @access public
     * @return void
     */
    public function setCustomerEMail($email)
    {
        $this->_customerEMail = $email;
    }


    /**
     * setBasket
     *
     * @param array $basket
     * @access public
     * @return void
     */
    public function setBasket($basket)
    {
        $array = array();
        foreach ($basket as $value) {
            $array[] = parent::addItem2Basket($value['description'], isset($value['quantity']) ? $value['quantity'] : null, isset($value['itemValue']) ? $value['itemValue'] : null, isset($value['itemTax']) ? $value['itemTax'] : null, isset($value['itemTotal']) ? $value['itemTotal'] : null, isset($value['lineTotal']) ? $value['lineTotal'] : null);
        }

        $this->_basket = (string)count($array);
        foreach ($array as $value) {
            $this->_basket .= $value;
        }
    }

    /**
     * setGiftAidPayment
     *
     * @param boolean $giftAidPayment
     * @access public
     * @return void
     */
    public function setGiftAidPayment($giftAidPayment)
    {
        $this->_giftAidPayment = $giftAidPayment;
    }


    /**
     * setApplyAVSCV2
     *
     * @param $applyAVSCV2
     * @access public
     * @return void
     */
    public function setApplyAVSCV2($applyAVSCV2)
    {
        $this->_applyAVSCV2 = $applyAVSCV2;
    }

    /**
     * setApply3DSecure
     *
     * @param int $apply3DSecure
     * @access public
     * @return void
     */
    public function setApply3DSecure($apply3DSecure)
    {
        $this->_apply3DSecure = $apply3DSecure;
    }

    /**
     * setClientIPAddress
     *
     * @access public
     * @return void
     */
    public function setClientIPAddress()
    {
        $this->_clientIPAddress = $_SERVER['REMOTE_ADDR'];
    }

    /**
     * setAccountType
     *
     * @param int $accountType
     * @access public
     * @return void
     */
    public function setAccountType($accountType)
    {
        $this->_accountType = $accountType;
    }

    /**
     * setPaymentDetails
     *
     * sets action's compulsory data
     *
     * @param string $description
     * @param float $amount
     * @access public
     * @return void
     */
    public function setPaymentDetails($description, $amount)
    {
        $this->_description = $description;
        $this->_amount = $amount;
    }

    /**
     * getPreparedData
     *
     * gets action's data for sending to SagePay
     *
     * @access public
     * @return string
     */
    public function getPreparedData()
    {
        $this->validateDetails();
        $string = parent::getPreparedData();
        $string .= "&Amount=" . urlencode($this->_amount);
        $string .= "&Currency=" . urlencode($this->_currency);
        $string .= "&Description=" . urlencode($this->_description);
        $string .= "&Token=" . urlencode($this->_token);
        $string .= "&StoreToken=" . urlencode($this->_storeToken);

        if (isset($this->_cv2)) {
            $string .= "&CV2=" . urlencode($this->_cv2);
        }

        $string .= "&BillingFirstnames=" . urlencode($this->_billingAddress->getFirstnames());
        $string .= "&BillingSurname=" . urlencode($this->_billingAddress->getSurname());
        $string .= "&BillingAddress1=" . urlencode($this->_billingAddress->getLine1());
        $string .= "&BillingAddress2=" . urlencode($this->_billingAddress->getLine2());
        $string .= "&BillingCity=" . urlencode($this->_billingAddress->getCity());
        $string .= "&BillingPostCode=" . urlencode($this->_billingAddress->getPostcode());
        $string .= "&BillingCountry=" . urlencode($this->_billingAddress->getCountry());
        $string .= "&BillingState=" . urlencode($this->_billingAddress->getState());
        $string .= "&BillingPhone=" . urlencode($this->_billingAddress->getPhone());

        $string .= "&DeliverySurname=" . urlencode($this->_deliveryAddress->getSurname());
        $string .= "&DeliveryFirstnames=" . urlencode($this->_deliveryAddress->getFirstnames());
        $string .= "&DeliveryAddress1=" . urlencode($this->_deliveryAddress->getLine1());
        $string .= "&DeliveryAddress2=" . urlencode($this->_deliveryAddress->getLine2());
        $string .= "&DeliveryCity=" . urlencode($this->_deliveryAddress->getCity());
        $string .= "&DeliveryPostCode=" . urlencode($this->_deliveryAddress->getPostcode());
        $string .= "&DeliveryCountry=" . urlencode($this->_deliveryAddress->getCountry());
        $string .= "&DeliveryState=" . urlencode($this->_deliveryAddress->getState());
        $string .= "&DeliveryPhone=" . urlencode($this->_deliveryAddress->getPhone());

        if (isset($this->_basket)) {
            $string .= "&Basket=" . urlencode($this->_basket);
        }
        if (isset($this->_customerEMail)) {
            $string .= "&CustomerEMail=" . urlencode($this->_customerEMail);
        }
        if (isset($this->_clientIPAddress)) {
            $string .= "&ClientIPAddress=" . urlencode($this->_clientIPAddress);
        }
        if (isset($this->_giftAidPayment)) {
            $string .= "&GiftAidPayment=" . urlencode($this->_giftAidPayment);
        }
        if (isset($this->_accountType)) {
            $string .= "&AccountType=" . urlencode($this->_accountType);
        }
        if (isset($this->_applyAVSCV2)) {
            $string .= "&ApplyAVSCV2=" . urlencode($this->_applyAVSCV2);
        }

        if (isset($this->_apply3DSecure)) {
            $string .= "&Apply3DSecure=" . urlencode($this->_apply3DSecure);
        }

        return $string;
    }

    /**
     * validateDetails
     *
     * validates action's data before sending to SagePay
     *
     * @access private
     * @throws Exception
     * @return boolean
     */
    private function validateDetails()
    {
        try {
            if (!isset($this->_amount)) {
                throw new exception("Amount is not set");
            } elseif (($this->_amount < 1.00) || ($this->_amount > 100000.00)) {
                throw new exception("Amount must be more than 1.00 but less than 100,000.00");
            }

            if (!isset($this->_description)) {
                throw new exception("Description is not set");
            } elseif (SagePayDirect::validateLength($this->_description, 100)) {
                throw new exception("Description must be upto 100 characters");
            }

            if (!isset($this->_currency)) {
                throw new exception("Currency is not set");
            } elseif (SagePayDirect::validateLength($this->_currency, 3)) {
                throw new exception("Currency must be upto 3 characters");
            }

            if (isset($this->_customerEMail) && SagePayDirect::validateLength($this->_customerEMail, 255)) {
                throw new exception("Customer Email must be upto 255 characters");
            }

            if (isset($this->_clientIPAddress) && SagePayDirect::validateLength($this->_clientIPAddress, 15)) {
                throw new exception("Client IP Address must be upto 15 characters");
            }

            if (isset($this->_basket) && SagePayDirect::validateLength($this->_basket, 7500)) {
                throw new exception("Basket content must be upto 7500 characters");
            }

            if (isset($this->_giftAidPayment) && ($this->_giftAidPayment != (0 || 1))) {
                throw new exception("Gift Aid Payment type is not recognised");
            }

            if (isset($this->_accountType) && ($this->_accountType != ('E' || 'C' || 'M'))) {
                throw new exception("Account type is not recognised");
            }

            if (isset($this->_apply3DSecure) && ($this->_apply3DSecure != (0 || 1 || 2 || 3))) {
                throw new exception("Apply 3D Secure flag is not recognised");
            }
            $this->_deliveryAddress->validateDetails();
        } catch (exception $exception) {
            throw new Exception($exception->getMessage());
        }
        return true;
    }

}

?>