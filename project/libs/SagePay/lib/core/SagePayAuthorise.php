<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayAuthorise
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayAuthorise extends SagePayTransaction
{		
	/**
	 * _description
	 * 
	 * Free text description of authorisation.
	 * 
	 * @var string; max 100 characters
	 * @access private
	 */
	private $_description;
	
	/**
	 * _amount
	 * 
	 * Amount of the authorisation.
	 * 
	 * @var float; 1.00 to 100,000.00
	 * @access private
	 */
	private $_amount;
	
	/**
	 * _relatedVPSTxId
	 * 
	 * VPSTxId of the authenticate transaction against which the authorisation is required.
	 * 
	 * @var string; max 38 characters
	 * @access private
	 */
	private $_relatedVPSTxId;
	
	/**
	 * _relatedVendorTxCode
	 * 
	 * VendorTxCode of the authenticate transaction against which the authorisation is required.
	 * 
	 * @var string; max 40 characters
	 * @access private
	 */
	private $_relatedVendorTxCode;
	
	/**
	 * _relatedSecurityKey
	 * 
	 * The SecurityKey of the authenticate transaction sent back by the Sage Pay System when the transaction was registered.
	 * 
	 * @var string; max 10 characters
	 * @access private
	 */
	private $_relatedSecurityKey;
	
	/**
	 * _applyAVSCV2
	 * 
	 * Flag
	 * 0 = If AVS/CV2 enabled then check them. If rules apply, use rules. (default)
	 * 1 = Force AVS/CV2 checks even if not enabled for the account. If rules apply, use rules.
	 * 2 = Force NO AVS/CV2 checks even if enabled on account.
	 * 3 = Force AVS/CV2 checks even if not enabled for the account but don't apply any rules.
	 * 
	 * @var int; OPTIONAL
	 * @access private
	 */
	private $_applyAVSCV2;
	
	/**
     * __construct 
     * 
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::__construct("AUTHORISE");
	}
	
	/**
     * getDescription 
     * 
     * @access public
     * @return string
     */
	public function getDescription()
	{
		return $this->_description;
	}
	
	/**
     * getAmount 
     * 
     * @access public
     * @return float
     */
	public function getAmount()
	{
		return $this->_amount;		
	}
	
	/**
     * getRelatedVPSTxId 
     * 
     * @access public
     * @return string
     */
	public function getRelatedVPSTxId()
	{
		return $this->_relatedVPSTxId;
	}
	
	/**
     * getRelatedVendorTxCode 
     * 
     * @access public
     * @return string
     */
	public function getRelatedVendorTxCode()
	{
		return $this->_relatedVendorTxCode;
	}
	
	/**
     * getRelatedSecurityKey 
     * 
     * @access public
     * @return string
     */
	public function getRelatedSecurityKey()
	{
		return $this->_relatedSecurityKey;
	}
	
	/**
     * getApplyAVSCV2 
     * 
     * @access public
     * @return int
     */
	public function getApplyAVSCV2()
	{
		return $this->_applyAVSCV2;
	}
	
	/**
     * setDescription 
     * 
     * @param string $description
     * @access public
     * @return void
     */
	public function setDescription($description)
	{
		$this->_description = $description;
	}
	
	/**
     * setAmount 
     * 
     * @param float $amount
     * @access public
     * @return void
     */
	public function setAmount($amount)
	{
		$this->_amount = $amount;
	}
	
	/**
     * setRelatedVPSTxId 
     * 
     * @param string $relatedVPSTxId
     * @access public
     * @return void
     */
	public function setRelatedVPSTxId($relatedVPSTxId)
	{
		$this->_relatedVPSTxId = $relatedVPSTxId;
	}
	
	/**
     * setRelatedVendorTxCode 
     * 
     * @param string $relatedVendorTxCode
     * @access public
     * @return void
     */
	public function setRelatedVendorTxCode($relatedVendorTxCode)
	{
		$this->_relatedVendorTxCode = $relatedVendorTxCode;
	}
	
	/**
     * setRelatedSecurityKey 
     * 
     * @param string $relatedSecurityKey
     * @access public
     * @return void
     */
	public function setRelatedSecurityKey($relatedSecurityKey)
	{
		$this->_relatedSecurityKey = $relatedSecurityKey;
	}
	
	/**
     * setApplyAVSCV2 
     * 
     * @param int $applyAVSCV2
     * @access public
     * @return void
     */
	public function setApplyAVSCV2($applyAVSCV2)
	{
		$this->_applyAVSCV2 = $applyAVSCV2;
	}
	
	/**
     * setAuthoriseDetails
     * 
     * @param string $description
     * $param float $amount
     * @param string $relatedVPSTxId
     * @param string $relatedVendorTxCode
     * @param string $relatedSecurityKey
     * @access public
     * @return void
     */
	public function setAuthoriseDetails($description, $amount, $relatedVPSTxId, $relatedVendorTxCode, $relatedSecurityKey)
	{
		$this->_description = $description;
		$this->_amount = $amount;
		$this->_relatedVPSTxId = $relatedVPSTxId;
		$this->_relatedVendorTxCode = $relatedVendorTxCode;
		$this->_relatedSecurityKey = $relatedSecurityKey;
	}
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{
		$string = parent::getPreparedData();
		$string .= "&Amount=".urlencode($this->_amount);		
		$string .= "&Description=".urlencode($this->_description);
		$string .= "&RelatedVPSTxId=".urlencode($this->_relatedVPSTxId);
		$string .= "&RelatedVendorTxCode=".urlencode($this->_relatedVendorTxCode);
		$string .= "&RelatedSecurityKey=".urlencode($this->_relatedSecurityKey);
		if(isset($this->_applyAVSCV2))
		{
			$string .= "&ApplyAVSCV2=".urlencode($this->_applyAVSCV2);
		}
		return $string;
	}
	
	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
		try 
		{
			if(!isset($this->_amount))
			{
				throw new exception ("Amount is not set"); 
			}
			elseif(($this->_amount < 1.00) || ($this->_amount > 100000.00))
			{
				throw new exception ("Amount must be more than 1.00 but less than 100,000.00");
			}
			
			if(!isset($this->_description))
			{
				throw new exception ("Description is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 100))
			{
				throw new exception ("Description must be upto 100 characters");
			}
			
			if(!isset($this->_relatedVPSTxId))
			{
				throw new exception ("RelatedVPSTxId is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_relatedVPSTxId, 38))
			{
				throw new exception ("RelatedVPSTxId must be upto 38 characters");
			}
			
			if(!isset($this->_relatedVendorTxCode))
			{
				throw new exception ("RelatedVendorTxCode is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_relatedVendorTxCode, 40))
			{
				throw new exception ("RelatedVendorTxCode must be upto 40 characters");
			}
			
			if(!isset($this->_relatedSecurityKey))
			{
				throw new exception ("RelatedSecurityKey is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_relatedSecurityKey, 10))
			{
				throw new exception ("RelatedSecurityKey must be upto 10 characters");
			}
			
			if(isset($this->_applyAVSCV2))
			{
				if(($this->_applyAVSCV2 < 0) || ($this->_applyAVSCV2 > 3))
				{
					throw new exception ("Unknown ApplyAVSCV2 value"); 
				}
			}
			
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
}
?>