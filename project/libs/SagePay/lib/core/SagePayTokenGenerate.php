<?php 

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    SagePayToken
 * @author     Nikolai Senkevich nikolais@madesimplegroup.com
 * @version    2011-05-28
 */

class SagePayTokenGenerate extends SagePayTransaction
{	
	/**
	 * _currency
	 * 
	 * Three-letter currency code to ISO 4217 e.g: �GBP�, �EUR� and �USD�
	 * 
	 * @var string; max 3 characters
	 * @access private
	 */
	private $_currency = SagePayDirect::SAGEPAY_CURRENCY;

	
    /**
	 * _card
	 * 
	 * card details
	 *
	 * @var object of SagePayCard;
	 * @access private
	 */
	private $_card;
	
	/**
     * __construct 
     * 
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::__construct("TOKEN");	
	}
	
	
	/**
     * getCurrency 
     * 
     * @access public
     * @return string
     */
	public function getCurrency()
	{
		return $this->_currency;		
	}
	
	
	/**
     * getCard 
     * 
     * @access public
     * @return object
     */
	public function getCard()
	{
		return $this->_card;
	}
	
	
	/**
     * setCurrency 
     * 
     * @param string $currency
     * @access public
     * @return void
     */
	public function setCurrency($currency)
	{
		$this->_currency = $currency;
	}
	
	
	/**
     * setCard 
     * 
     * @param string $card 
     * @access public
     * @return void
     */
	public function setCard($card)
	{
		$this->_card = $card;
	}
	
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{		
	 	$this->validateDetails();	 	
	 	$string = parent::getPreparedData();
	 	$string .= "&Currency=".urlencode($this->_currency);	
		$string .= $this->_card->getPreparedData();	
	 	return $string;
	}

	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
	 	try 
		{			
			if(!isset($this->_currency))
			{
				throw new exception ("Currency is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_currency, 3))
			{
				throw new exception ("Currency must be upto 3 characters");
			}
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
}
?>