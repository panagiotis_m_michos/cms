<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayManual
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayManual extends SagePayTransaction
{
	/**
	 * _amount
	 * 
	 * Amount for the transaction
	 * 
	 * @var float with a range 1.00 to 100,000.00;
	 * @access private
	 */
	private $_amount;
	
	/**
	 * _currency
	 * 
	 * Three-letter currency code to ISO 4217 e.g: �GBP�, �EUR� and �USD�
	 * 
	 * @var string; max 3 characters
	 * @access private
	 */
	private $_currency = SagePayDirect::SAGEPAY_CURRENCY;
	
	/**
	 * _description
	 * 
	 * Free text description of goods or services being purchased
	 * 
	 * @var string; max 100 characters
	 * @access private
	 */
	private $_description;
	
	/**
	 * _card
	 * 
	 * Card details
	 * 
	 * @var object
	 * @access private
	 */
	private $_card;

	/**
	 * _authCode
	 * 
	 * The manual Authorisation Code obtained from your acquirer during Code 10 checks, for example.
	 * 
	 * @var string; max 15 characters
	 * @access private
	 */
	private $_authCode;
	
	/**
	 * _billingAddress
	 * 
	 * The Card Holder�s Billing Address without the Post/Zip code.
	 * 
	 * @var string; max 200 characters
	 * @access private
	 */
	private $_billingAddress;
	
	/**
	 * _billingPostcode
	 * 
	 * Free format field for the customer�s Delivery Address without the Post/Zip code
	 * 
	 * @var string; max 10 characters
	 * @access private
	 */
	private $_billingPostcode;
	
	/**
	 * _deliveryAddress
	 * 
	 * The Post code or Zip code of the customer�s delivery address
	 * 
	 * @var string; max 200 characters; OPTIONAL
	 * @access private
	 */
	private $_deliveryAddress;
	
	/**
	 * _deliveryPostcode
	 * 
	 * The delivery address without the Post/Zip code.
	 * 
	 * @var string; max 10 characters; OPTIONAL
	 * @access private
	 */
	private $_deliveryPostcode;
	
	/**
	 * _customerName
	 * 
	 * The name of the customer.
	 * 
	 * @var string; max 100 characters; OPTIONAL
	 * @access private
	 */
	private $_customerName;
	
	/**
	 * _contactNumber
	 * 
	 * The telephone number on which to contact the customer.
	 * 
	 * @var string; max 20 characters; OPTIONAL
	 * @access private
	 */
	private $_contactNumber;
	
	/**
	 * _contactFax
	 * 
	 * The fax number on which to contact.
	 * 
	 * @var string; max 20 characters; OPTIONAL
	 * @access private
	 */
	private $_contactFax;
	
	/**
	 * _customerEMail
	 * 
	 * The customer�s e-mail address
	 * 
	 * @var string; max 255 characters; OPTIONAL
	 * @access private
	 */
	private $_customerEMail;
	
	/**
	 * _basket
	 * 
	 * The shopping basket contents.
	 * 
	 * @var string; max 7500 characters; OPTIONAL
	 * @access private
	 */
	private $_basket;
	
	/**
	 * _giftAidPayment
	 * 
	 * Flag
	 * 0 = This transaction is not a Gift Aid charitable donation(default)
	 * 1 = This payment is a Gift Aid charitable donation and the customer has AGREED to donate the tax.
	 * 
	 * @var boolean; OPTIONAL
	 * @access private
	 */
	private $_giftAidPayment;
	
	/**
	 * _accountType
	 * 
	 * Type of vendor's account; 
	 * E = Use the e-commerce merchant account (default).
	 * C = Use the continuous authority merchant account (if present).
	 * M = Use the mail order, telephone order account (if present).
	 * 
	 * @var char; OPTIONAL
	 * @access private
	 */	
	private $_accountType;
	
	/**
     * __construct 
     * 
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::__construct("MANUAL");
		require_once("SagePayCard.php");
		$this->_card = new SagePayCard();		
	}
	
	/**
     * getAmount 
     * 
     * @access public
     * @return float
     */
	public function getAmount()
	{
		return $this->_amount;
	}
	
	/**
     * getCurrency 
     * 
     * @access public
     * @return string
     */
	public function getCurrency()
	{
		return $this->_currency;
	}
	
	/**
     * getDescription 
     * 
     * @access public
     * @return string
     */
	public function getDescription()
	{
		return $this->_description;
	}
	
	/**
     * getCard 
     * 
     * @access public
     * @return object
     */
	public function getCard()
	{
		return $this->_card;
	}
	
	/**
     * getAuthCode 
     * 
     * @access public
     * @return string
     */	
	public function getAuthCode()
	{
		return $this->_authCode;
	}

	/**
     * getBillingAddress 
     * 
     * @access public
     * @return string
     */
	public function getBillingAddress()
	{
		return $this->_billingAddress;
	}
	
	/**
     * getBillingPostcode 
     * 
     * @access public
     * @return string
     */
	public function getBillingPostcode()
	{
		return $this->_billingPostcode;
	}

	/**
     * getDeliveryAddress 
     * 
     * @access public
     * @return string
     */
	public function getDeliveryAddress()
	{
		return $this->_deliveryAddress;
	}
	
	/**
     * getDeliveryPostcode 
     * 
     * @access public
     * @return string
     */
	public function getDeliveryPostcode()
	{
		return $this->_deliveryAddress;
	}
	
	/**
     * getCustomerName 
     * 
     * @access public
     * @return string
     */
	public function getCustomerName()
	{
		return $this->_customerName;
	}
	
	/**
     * getContactNumber 
     * 
     * @access public
     * @return string
     */
	public function getContactNumber()
	{
		return $this->_contactNumber;
	}
	
	/**
     * getContactFax 
     * 
     * @access public
     * @return string
     */
	public function getContactFax()
	{
		return $this->_contactFax;
	}
	
	/**
     * getCustomerEMail 
     * 
     * @access public
     * @return string
     */
	public function getCustomerEMail()
	{
		return $this->_customerEMail;
	}
	
	/**
     * getBasket 
     * 
     * @access public
     * @return string
     */
	public function getBasket()
	{
		return $this->_basket;
	}
	
	/**
     * getGiftAidPayment 
     * 
     * @access public
     * @return boolean
     */
	public function getGiftAidPayment()
	{
		return $this->_giftAidPayment;
	}
	
	/**
     * getAccountType 
     * 
     * @access public
     * @return char
     */
	public function getAccountType()
	{
		return $this->_accountType;
	}
	
	/**
     * setAmount 
     * 
     * @param float $amount
     * @access public
     * @return void
     */
	public function setAmount($amount)
	{
		$this->_amount = $amount;
	}
	
	/**
     * setCurrency 
     * 
     * @param string $currency
     * @access public
     * @return void
     */
	public function setCurrency($currency)
	{
		$this->_currency = $currency;
	}
	
	/**
     * setDescription 
     * 
     * @param string $description
     * @access public
     * @return void
     */
	public function setDescription($description)
	{
		$this->_description = $description;
	}
	
	/**
     * setCard 
     * 
     * @param string $cardholderName 
     * @param string $cardType
     * @param int $cardNumber
     * @param int $expiryDate 
     * @access public
     * @return void
     */
	public function setCard($cardholderName, $cardType, $cardNumber, $expiryDate)
	{
		$this->_card->setCardDetails($cardholderName, $cardType, $cardNumber, $expiryDate);
	}
	
	/**
     * setAuthCode 
     * 
     * @param string $code  
     * @access public
     * @return void
     */
	public function setAuthCode($code)
	{
		$this->_authCode = $code;
	}
	
	/**
     * setBillingAddress 
     * 
     * @param string $address
     * @access public
     * @return void
     */
	public function setBillingAddress($address)
	{
		$this->_billingAddress = $address;
	}
	
	/**
     * setBillingPostcode 
     * 
     * @param string $postcode
     * @access public
     * @return void
     */
	public function setBillingPostcode($postcode)
	{
		$this->_billingPostcode = $postcode;
	}
	
	/**
     * setDeliveryAddress 
     * 
     * @param string $address
     * @access public
     * @return void
     */
	public function setDeliveryAddress($address)
	{
		$this->_deliveryAddress = $address;
	}
	
	/**
     * setDeliveryPostcode 
     * 
     * @param string $postcode
     * @access public
     * @return void
     */
	public function setDeliveryPostcode($postcode)
	{
		$this->_deliveryPostcode = $postcode;
	}
	
	/**
     * setCustomerName 
     * 
     * @param string $name
     * @access public
     * @return void
     */
	public function setCustomerName($name)
	{
		$this->_customerName = $name;
	}
	
	/**
     * setContactNumber 
     * 
     * @param string $number
     * @access public
     * @return void
     */
	public function setContactNumber($number)
	{
		$this->_contactNumber = $number;
	}
	
	/**
     * setContactFax 
     * 
     * @param string $fax
     * @access public
     * @return void
     */
	public function setContactFax($fax)
	{
		$this->_contactFax = $fax;
	}
	
	/**
     * setCustomerEMail 
     * 
     * @param string $email
     * @access public
     * @return void
     */
	public function setCustomerEMail($email)
	{
		$this->_customerEMail = $mail;
	}
	
	/**
     * setBasket 
     * 
     * @param array $basket
     * @access public
     * @return void
     */
	public function setBasket($basket)
	{
		$array = array();
		foreach($basket as $value)
		{			
			$array[] = parent::addItem2Basket($value['description'], 
											  isset($value['quantity'])? $value['quantity'] : null, 
											  isset($value['itemValue'])? $value['itemValue'] : null, 
											  isset($value['itemTax'])? $value['itemTax'] : null, 
											  isset($value['itemTotal'])? $value['itemTotal'] : null,
											  isset($value['lineTotal'])? $value['lineTotal'] : null);
		}
		
		$this->_basket = (string)count($array);
		foreach ($array as $value)
		{
			$this->_basket .= $value;
		}
	}
	
	/**
     * setGiftAidPayment 
     * 
     * @param boolean $giftAidPayment
     * @access public
     * @return void
     */
	public function setGiftAidPayment($giftAidPayment)
	{
		$this->_giftAidPayment = $giftAidPayment;
	}
	
	/**
     * setAccountType 
     * 
     * @param char $accountType
     * @access public
     * @return void
     */
	public function setAccountType($accountType)
	{
		$this->_accountType = $accountType;
	}
	
	/**
     * setManualPaymentDetails 
     * 
     * sets action's compulsory data
     * 
     * @param string $description 
     * @param foat $amount   
     * @param string $paypalCallbackURL  
     * @access public
     * @return void
     */
	public function setManualPaymentDetails($description, $amount)
	{
		$this->_description = $description;
		$this->_amount = $amount;
	}
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{
		$this->validateDetails();
		$string = parent::getPreparedData();
	 	$string .= "&Amount=".urlencode($this->_amount);
	 	$string .= "&Currency=".urlencode($this->_currency);
	 	$string .= "&Description=".urlencode($this->_description);
	 	if(isset($this->_basket))
		{						
			$string .= "&Basket=".urlencode($this->_basket);		
		}
		if(isset($this->_customerEMail))
		{
			$string .= "&CustomerEMail=".urlencode($this->_customerEMail);
		}		
		if(isset($this->_giftAidPayment))
		{
			$string .= "&GiftAidPayment=".urlencode($this->_giftAidPayment);
		}
		if(isset($this->_accountType))
		{
			$string .= "&AccountType=".urlencode($this->_accountType);
		}
		if(isset($this->_authCode))
		{
			$string .= "&AuthCode=".urlencode($this->_authCode);		
		}
		if(isset($this->_billingAddress))
		{
			$string .= "&BillingAddress=".urlencode($this->_billingAddress);
		}
		if(isset($this->_billingPostcode))
		{
			$string .= "&BillingPostCode=".urlencode($this->_billingPostcode);
		}
		if(isset($this->_deliveryAddress))
		{
			$string .= "&DeliveryAddress=".urlencode($this->_deliveryAddress);
		}
		if(isset($this->_deliveryPostcode))
		{
			$string .= "&DeliveryPostCode=".urlencode($this->_deliveryPostcode);
		}
		if(isset($this->_customerName))
		{
			$string .= "&CustomerName=".urlencode($this->_customerName);
		}
		if(isset($this->_contactNumber))
		{
			$string .= "&ContactNumber=".urlencode($this->_contactNumber);
		}
		if(isset($this->_contactFax))
		{
			$string .= "&ContactFax=".urlencode($this->_contactFax);
		}		
		$string .= $this->_card->getPreparedData();	
	 	return $string;
	}
	
	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
		try 
		{
			if(!isset($this->_amount))
			{
				throw new exception ("Amount is not set"); 
			}
			elseif(($this->_amount < 1.00) || ($this->_amount > 100000.00))
			{
				throw new exception ("Amount must be more than 1.00 but less than 100,000.00");
			}
			
			if(!isset($this->_description))
			{
				throw new exception ("Description is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 100))
			{
				throw new exception ("Description must be upto 100 characters");
			}
			
			if(!isset($this->_currency))
			{
				throw new exception ("Currency is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 3))
			{
				throw new exception ("Currency must be upto 3 characters");
			}
			
			if(isset($this->_customerEMail) && SagePayDirect::validateLength($this->_customerEMail, 255))
			{			
			 	throw new exception ("Customer Email must be upto 255 characters");			
			}
			
			if(isset($this->_basket) && SagePayDirect::validateLength($this->_basket, 7500))
			{			
				throw new exception ("Basket content must be upto 7500 characters");
			}
			
			if(isset($this->_giftAidPayment) && ($this->_giftAidPayment != (0||1)))
			{
				throw new exception ("Gift Aid Payment type is not recognised");
			}
			
			if(isset($this->_accountType) && ($this->_accountType != ('E'||'C'||'M')))
			{
				throw new exception ("Account type is not recognised");
			}
			
			if(isset($this->_authCode) && SagePayDirect::validateLength($this->_authCode, 15))
			{			
			 	throw new exception ("AuthCode must be upto 15 characters");			
			}
			
			if(isset($this->_billingAddress) && SagePayDirect::validateLength($this->_billingAddress, 200))
			{			
			 	throw new exception ("Billing Address must be upto 200 characters");			
			}
			
			if(isset($this->_billingPostcode) && SagePayDirect::validateLength($this->_billingPostcode, 10))
			{			
			 	throw new exception ("Billing PostCode must be upto 10 characters");			
			}
			
			if(isset($this->_deliveryAddress) && SagePayDirect::validateLength($this->_deliveryAddress, 200))
			{			
			 	throw new exception ("Delivery Address must be upto 200 characters");			
			}
			
			if(isset($this->_deliveryPostcode) && SagePayDirect::validateLength($this->_deliveryPostcode, 10))
			{			
			 	throw new exception ("Delivery PostCode must be upto 10 characters");			
			}
			
			if(isset($this->_customerName) && SagePayDirect::validateLength($this->_customerName, 100))
			{			
			 	throw new exception ("Customer Name must be upto 100 characters");			
			}
			
			if(isset($this->_contactNumber) && SagePayDirect::validateLength($this->_contactNumber, 20))
			{			
			 	throw new exception ("Contact Number must be upto 20 characters");			
			}
			
			if(isset($this->_contactFax) && SagePayDirect::validateLength($this->_contactFax, 20))
			{			
			 	throw new exception ("Contact Fax must be upto 20 characters");			
			}
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
	
}
?>