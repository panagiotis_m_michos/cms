<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    SagePayToken
 * @author     Nikolai Senkevich nikolais@madesimplegroup.com
 * @version    2011-05-28
 */

class SagePayTokenCard
{
	/**
	 * _cardholderName
	 * 
	 * The card holder's name
	 * 
	 * @var string; max 50 characters
	 * @access private
	 */
	private $_cardholderName;
	
	/**
	 * _cardType
	 * 
	 * The card type: "VISA" | "MC" - MasterCard | "DELTA" | "SOLO" | "MAESTRO" | "UKE" - Visa Electron | "AMEX" | "DC" - DINERS | "JCB" | "LASER" | "PAYPAL"
	 * 
	 * @var string; max 15 characters
	 * @access private
	 */
	private $_cardType;
	
	/**
	 * _cardNumber
	 * 
	 * The credit or debit card number with no spaces
	 * 
	 * @var int; max 20 characters
	 * @access private
	 */
	private $_cardNumber;

	/**
	 * _startDate
	 * 
	 * The start date (required for some Maestro, Solo and Amex) in MMYY format
	 * 
	 * @var int; 4 characters; OPTIONAL
	 * @access private
	 */
	private $_startDate;
	
	/**
	 * _expiryDate
	 * 
	 * The Expiry date (required for ALL cards) in MMYY format.
	 * 
	 * @var int; 4 characters
	 * @access private
	 */
	private $_expiryDate;
	
	/**
	 * _issueNumber
	 * 
	 * The card Issue Number (some Maestro and Solo cards only)
	 * 
	 * @var int; max 2 characters; OPTIONAL
	 * @access private
	 */
	private $_issueNumber;
	
	/**
	 * _cvv
	 * 
	 * The extra security 3 digits on the signature strip of the card, or the extra 4 digits on the front for AMEX
	 * 
	 * @var int; max 4 characters; OPTIONAL
	 * @access private
	 */
	private $_cvv;		
	
	/**
     * __construct 
     *     
     * @access public
     * @return void
     */
	public function __construct()
	{		
		
	}
	
	/**
     * getCardholderName 
     * 
     * @access public
     * @return string
     */
	public function getCardholderName()
	{
		return $this->_cardholderName;
	}
	
	/**
     * getCardType 
     * 
     * @access public
     * @return string
     */
	public function getCardType()
	{
		return $this->_cardType;
	}
	
	/**
     * getCardNumber 
     * 
     * @access public
     * @return int
     */
	public function getCardNumber()
	{
		return $this->_cardNumber;
	}

	/**
     * getStartDate 
     * 
     * @access public
     * @return int
     */
	public function getStartDate()
	{
		return $this->_startDate;
	}
	
	/**
     * getExpiryDate 
     * 
     * @access public
     * @return int
     */
	public function getExpiryDate()
	{
		return $this->_expiryDate;
	}
	
	/**
     * getIssueNumber 
     * 
     * @access public
     * @return int
     */
	public function getIssueNumber()
	{
		return $this->_issueNumber;		
	}
	
	/**
     * getCVV 
     * 
     * @access public
     * @return int
     */
	public function getCVV()
	{
		return $this->_cvv;
	}
	
	
	/**
     * setCardholderName 
     * 
     * @param string $cardholderName
     * @access public
     * @return void
     */
	public function setCardholderName($cardholderName)
	{
		$this->_cardholderName = $cardholderName;
	}
	
	/**
     * setCardType 
     * 
     * @param string $cardType
     * @access public
     * @return void
     */
	public function setCardType($cardType)
	{
		$this->_cardType = $cardType;
	}
	
	/**
     * setCardNumber 
     * 
     * @param int $cardNumber
     * @access public
     * @return void
     */
	public function setCardNumber($cardNumber)
	{
		$this->_cardNumber = $cardNumber;
	}
	
	/**
     * setStartDate 
     * 
     * @param int $startDate
     * @access public
     * @return void
     */
	public function setStartDate($startDate)
	{
		$this->_startDate = $startDate;
	}
	
	/**
     * setExpiryDate 
     * 
     * @param int $expiryDate
     * @access public
     * @return void
     */
	public function setExpiryDate($expiryDate)
	{
		$this->_expiryDate = $expiryDate;
	}	
	
	/**
     * setIssueNumber 
     * 
     * @param int $issueNumber
     * @access public
     * @return void
     */
	public function setIssueNumber($issueNumber)
	{
		$this->_issueNumber = $issueNumber;
	}	
	
	/**
     * setCVV 
     * 
     * @param int $cvv
     * @access public
     * @return void
     */
	public function setCVV($cvv)
	{
		$this->_cvv = $cvv;
	}
	
	
	/**
     * setCardDetails 
     * 
     * @param string $cardholderName 
     * @param string $cardType
     * @param int $cardNumber
     * @param int $expiryDate 
     * @param int $cvv 
     * @param int $startDate 
     * @param int $issueNumber 
     * @access public
     * @return void
     */
	public function setCardDetails($cardholderName, $cardType, $cardNumber, $expiryDate, $cvv, $startDate = NULL, $issueNumber= NULL)
	{
		$cardType = strtoupper($cardType);
		if($cardType != ("VISA"||"MC"||"DELTA"||"SOLO"||"MAESTRO"||"UKE"||"AMEX"||"DC"||"JCB"||"LASER"))
		{
			trigger_error("This type of card is not suported", E_USER_NOTICE);
		}	
        if ($cardType == SagePayDirect::CARD_MAESTRO) {
            $this->_startDate = $startDate;
        }
                
        $this->_cvv = $cvv;
		$this->_cardholderName = $cardholderName;
		$this->_cardType = $cardType;
		$this->_cardNumber = $cardNumber;
		$this->_expiryDate = $expiryDate;
        $this->_issueNumber = $issueNumber;
	}
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{	
		$this->validateDetails();
		$string = "&CardHolder=".urlencode($this->_cardholderName);
		$string .= "&CardType=".urlencode($this->_cardType);
		$string .= "&CardNumber=".urlencode($this->_cardNumber);
		if(isset($this->_startDate))
		{
			$string .= "&StartDate=".urlencode($this->_startDate);
		}
		$string .= "&ExpiryDate=".urlencode($this->_expiryDate);
		if(isset($this->_issueNumber))
		{
			$string .= "&IssueNumber=".urlencode($this->_issueNumber);
		}
		if(isset($this->_cvv))
		{
			$string .= "&CV2=".urlencode($this->_cvv);
		}

		return $string;
	}

	/**
     * validateDetails
     * 
     * validates card's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
//		if(isset($this->_billingAddress))
//		{
//			$this->_billingAddress->validateDetails();
//		}
		try 
		{
			if(!isset($this->_cardholderName))
			{
				throw new exception ("Cardholder name is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_cardholderName, 50))
			{
				throw new exception ("Cardholder name must be upto 50 characters");
			}
			
			if(!isset($this->_cardType))
			{
				throw new exception ("Card type is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_cardType, 15))
			{
				throw new exception ("Card type must be upto 15 characters");
			}
			
			if(!isset($this->_cardNumber))
			{
				throw new exception ("Card number is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_cardNumber, 20))
			{
				throw new exception ("Card number must be upto 20 characters");
			}
			
			if(isset($this->_startDate))
			{
				if(SagePayDirect::validateLength($this->_startDate, 4))
				{
					throw new exception ("Start date must be upto 4 characters in MMYY format");					
				}
			}
			
			if(!isset($this->_expiryDate))
			{
				throw new exception ("Expiry date is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_expiryDate, 4))
			{
				throw new exception ("Expiry date must be upto 4 characters in MMYY format");
			}			
			
			if(isset($this->_issueNumber))
			{
				if(SagePayDirect::validateLength($this->_issueNumber, 2))
				{
					throw new exception ("Issue number must be upto 2 characters");					
				}
			}
			
			if(isset($this->_cvv))
			{
				if(SagePayDirect::validateLength($this->_cvv, 4))
				{
					throw new exception ("Card verification value must be upto 4 characters");					
				}
			}					
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}
		return true;
	
	}
	
	public function __sleep() {
		$this->_cardNumber = substr($this->_cardNumber, -4, 4);
		return array('_cardholderName', '_cardNumber');
	}
}
?>