<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+
 */

/**
 * SagePayAddress
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayAddress
{
	/**
	 * _firstnames
	 * 
	 * First names of the person at the address
	 * 
	 * @var string; max 20 characters
	 * @access private
	 */
	private $_firstnames;
	
	/**
	 * _surname
	 * 
	 * Surname of the person at the address
	 * 
	 * @var string; max 20 characters
	 * @access private
	 */
	private $_surname;
	
	/**
	 * _line1
	 * 
	 * First line of the address
	 * 
	 * @var string; max 100 characters
	 * @access private
	 */
	private $_line1;
	
	/**
	 * _line2
	 * 
	 * Second line of the address
	 * 
	 * @var string; max 100 characters; OPTIONAL
	 * @access private
	 */
	private $_line2;
	
	/**
	 * _city
	 * 
	 * City component of the address
	 * 
	 * @var string; max 40 characters
	 * @access private
	 */
	private $_city;
	
	/**
	 * _postcode
	 * 
	 * The Post/Zip code of the address
	 * 
	 * @var string; max 10 characters
	 * @access private
	 */
	private $_postcode;
	
	/**
	 * _country
	 * 
	 * ISO 3166-1 country code of the address
	 * 
	 * @var string; max 2 characters
	 * @access private
	 */
	private $_country;
	
	/**
	 * _state
	 * 
	 * State code for US customers only
	 * 
	 * @var string; max 2 characters; OPTIONAL
	 * @access private
	 */
	private $_state;
	
	/**
	 * _phone
	 * 
	 * phone number at address
	 * 
	 * @var string; max 20 characters; OPTIONAL
	 * @access private
	 */
	private $_phone;
	
	/**
     * __construct 
     *     
     * @access public
     * @return void
     */	
	public function __construct(){}	
	
	/**
     * getFirstnames 
     * 
     * @access public
     * @return string
     */
	public function getFirstnames()
	{
		return $this->_firstnames;
	}
	
	/**
     * getSurname 
     * 
     * @access public
     * @return string
     */	
	public function getSurname()
	{
		return $this->_surname;
	}
	
	/**
     * getLine1 
     * 
     * @access public
     * @return string
     */	
	public function getLine1()
	{
		return $this->_line1;
	}
	
	/**
     * getLine2 
     * 
     * @access public
     * @return string
     */
	public function getLine2()
	{
		return $this->_line2;
	}
	
	/**
     * getCity 
     * 
     * @access public
     * @return string
     */
	public function getCity()
	{
		return $this->_city;
	}
	
	/**
     * getPostcode 
     * 
     * @access public
     * @return string
     */
	public function getPostcode()
	{
		return $this->_postcode;
	}
	
	/**
     * getCountry 
     * 
     * @access public
     * @return string
     */
	public function getCountry()
	{
		return $this->_country;
	}
	
	/**
     * getState 
     * 
     * @access public
     * @return string
     */
	public function getState()
	{
		return $this->_state;
	}
	
	/**
     * getPhone 
     * 
     * @access public
     * @return string
     */
	public function getPhone()
	{
		return $this->_phone;
	}
	
	/**
     * setFirstnames 
     * 
     * @param string $firstnames
     * @access public
     * @return void
     */
	public function setFirstnames($firstnames)
	{
		$this->_firstnames = $firstnames;
	}
	
	/**
     * setSurname 
     * 
     * @param string $surname
     * @access public
     * @return void
     */
	public function setSurname($surname)
	{
		$this->_surname = $surname;
	}
	
	/**
     * setLine1 
     * 
     * @param string $line1
     * @access public
     * @return void
     */
	public function setLine1($line1)
	{
		$this->_line1 = $line1;
	}
	
	/**
     * setLine2 
     * 
     * @param string $line2
     * @access public
     * @return void
     */
	public function setLine2($line2)
	{
		$this->_line2 = $line2;
	}
	
	/**
     * setCity 
     * 
     * @param string $city
     * @access public
     * @return void
     */
	public function setCity($city)
	{
		$this->_city = $city;
	}
	
	/**
     * setPostcode 
     * 
     * @param string $postcode
     * @access public
     * @return void
     */
	public function setPostcode($postcode)
	{
		$this->_postcode = $postcode;
	}
	
	/**
     * setCountry 
     * 
     * @param string $country
     * @access public
     * @return void
     */
	public function setCountry($country)
	{
		$this->_country = $country;
	}
	
	/**
     * setState 
     * 
     * @param string $state
     * @access public
     * @return void
     */
	public function setState($state)
	{
		$this->_state = $state;
	}
	
	/**
     * setPhone 
     * 
     * @param string $phone
     * @access public
     * @return void
     */
	public function setPhone($phone)
	{
		$this->_phone = $phone;
	}

	/**
     * setAddressDetails
     *
     * @param string $firstnames
     * @param string $surname
     * @param string $line1
     * @param string $city
     * @param string $postcode
     * @param string $country
     * @access public
     * @return void
     */
	public function setAddressDetails($firstnames, $surname, $line1, $city, $postcode, $country)
	{
		$this->_firstnames = $firstnames;
		$this->_surname = $surname;		
		$this->_line1 = $line1;
		$this->_city = $city;
		$this->_postcode = $postcode;
		$this->_country = $country;
	}
	
	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access public
     * @return boolean
     */
	public function validateDetails()
	{		
		try 
		{
			if(!isset($this->_firstnames))
			{
				throw new exception ("Firstnames is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_firstnames, 20))
			{
				throw new exception ("Firstnames must be upto 20 characters");
			}
			
			if(!isset($this->_surname))
			{
				throw new exception ("Surname is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_surname, 20))
			{
				throw new exception ("Surname must be upto 20 characters");
			}
			
			if(!isset($this->_line1))
			{
				throw new exception ("AddressLine1 is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_line1, 100))
			{
				throw new exception ("AddressLine1 must be upto 100 characters");
			}
			
			if(isset($this->_line2))
			{
				if(SagePayDirect::validateLength($this->_line2, 100))
				{
					throw new exception ("AddressLine2 must be upto 100 characters");					
				}
			}
			
			if(!isset($this->_city))
			{
				throw new exception ("City is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_city, 40))
			{
				throw new exception ("City must be upto 40 characters");
			}
			
			if(!isset($this->_postcode))
			{
				throw new exception ("Post code is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_postcode, 10))
			{
				throw new exception ("Post code must be upto 10 characters");
			}
			
			if(!isset($this->_country))
			{
				throw new exception ("Country code is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_country, 2))
			{
				throw new exception ("Country code must be 2 characters");
			}
			
			if(isset($this->_state))
			{
				if(SagePayDirect::validateLength($this->_state, 2))
				{
					throw new exception ("State must be 2 characters");					
				}
			}
			
			if(isset($this->_phone))
			{
				if(SagePayDirect::validateLength($this->_phone, 20))
				{
					throw new exception ("Phone must be upto 20 characters");					
				}
			}					
		}
		catch (exception $exception)
		{
            throw new Exception($exception->getMessage());
		}
		return true;
	}
}
?>