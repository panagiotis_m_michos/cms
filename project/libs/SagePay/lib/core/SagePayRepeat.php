<?php 
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Zygimantas Daraska - sidabrinis@gmail.com                    |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * SagePayRepeat
 * 
 * @package SagePayDirect
 * @version 1.0
 * @copyright 2009 Zygimantas Daraska
 * @author Zygimantas Daraska <sidabrinis@gmail.com> 
 * @license GNU General Public License
 */

class SagePayRepeat extends SagePayTransaction
{
	/**
	 * _amount
	 * 
	 * Amount of the repeat payment.
	 * 
	 * @var float; 1.00 to 100,000.00
	 * @access private
	 */
	private $_amount;	
	
	/**
	 * _description
	 * 
	 * Free text description of the repeat payment
	 * 
	 * @var string; max 100 characters
	 * @access private
	 */
	private $_description;

	/**
	 * _currency
	 * 
	 * Three-letter currency code to ISO 4217 e.g: �GBP�, �EUR� and �USD�
	 * 
	 * @var string; max 3 characters
	 * @access private
	 */
	private $_currency = SagePayDirect::SAGEPAY_CURRENCY;
	
	/**
	 * _relatedVPSTxId
	 * 
	 * VPSTxId of the transaction against which the Repeat is required.
	 * 
	 * @var string; max 38 characters
	 * @access private
	 */
	private $_relatedVPSTxId;
	
	/**
	 * _relatedVendorTxCode
	 * 
	 * VendorTxCode for the original transaction to Repeat against.
	 * 
	 * @var string; max 40 characters
	 * @access private
	 */
	private $_relatedVendorTxCode;
	
	/**
	 * _relatedSecurityKey
	 * 
	 * The SecurityKey of the original transaction sent back by the Sage Pay System when the transaction was registered.
	 * 
	 * @var string; max 10 characters
	 * @access private
	 */
	private $_relatedSecurityKey;
	
	/**
	 * _relatedTxAuthNo
	 * 
	 * The TxAuthNo of the original transaction as returned by the Sage Pay system when it was authorised.
	 * 
	 * @var long int;
	 * @access private
	 */
	private $_relatedTxAuthNo;
	
	/**
     * __construct 
     *     
     * @access public
     * @return void
     */
	public function __construct()
	{
		parent::_construct("REPEAT");	
	}
	
	/**
     * getAmount 
     * 
     * @access public
     * @return float
     */
	public function getAmount()
	{
		return $this->_amount;
	}
	
	/**
     * getDescription 
     * 
     * @access public
     * @return string
     */
	public function getDescription()
	{
		return $this->_description;
	}
	
	/**
     * getCurrency 
     * 
     * @access public
     * @return string
     */
	public function getCurrency()
	{
		return $this->_currency;
	}
	
	/**
     * getRelatedVPSTxId 
     * 
     * @access public
     * @return string
     */
	public function getRelatedVPSTxId()
	{
		return $this->_relatedVPSTxId;
	}
	
	/**
     * getRelatedVendorTxCode 
     * 
     * @access public
     * @return string
     */
	public function getRelatedVendorTxCode()
	{
		return $this->_relatedVendorTxCode;
	}
	
	/**
     * getRelatedSecurityKey 
     * 
     * @access public
     * @return string
     */
	public function getRelatedSecurityKey()
	{
		return $this->_relatedSecurityKey;
	}
	
	/**
     * getRelatedTxAuthNo 
     * 
     * @access public
     * @return long int
     */
	public function getRelatedTxAuthNo()
	{
		return $this->_relatedTxAuthNo;
	}
	
	/**
     * setAmount 
     * 
     * @param float $amount
     * @access public
     * @return void
     */
	public function setAmount($amount)
	{
		$this->_amount = $amount;
	}
	
	/**
     * setDescription 
     * 
     * @param string $description
     * @access public
     * @return void
     */
	public function setDescription($description)
	{
		$this->_description = $description;
	}
	
	/**
     * setCurrency 
     * 
     * @param string $currency
     * @access public
     * @return void
     */
	public function setCurrency($currency)
	{
		$this->_currency = currency;
	}
	
	/**
     * setRelatedVPSTxId 
     * 
     * @param string $relatedVPSTxId
     * @access public
     * @return void
     */
	public function setRelatedVPSTxId($relatedVPSTxId)
	{
		$this->_relatedVPSTxId = $relatedVPSTxId;
	}
	
	/**
     * setRelatedVendorTxCode 
     * 
     * @param string $relatedVendorTxCode
     * @access public
     * @return void
     */
	public function setRelatedVendorTxCode($relatedVendorTxCode)
	{
		$this->_relatedVendorTxCode = $relatedVendorTxCode;
	}
	
	/**
     * setRelatedSecurityKey 
     * 
     * @param string $relatedSecurityKey
     * @access public
     * @return void
     */
	public function setRelatedSecurityKey($relatedSecurityKey)
	{
		$this->_relatedSecurityKey = $relatedSecurityKey;
	}
	
	/**
     * setRelatedTxAuthNo 
     * 
     * @param long int $relatedTxAuthNo
     * @access public
     * @return void
     */
	public function setRelatedTxAuthNo($relatedTxAuthNo)
	{
		$this->_relatedTxAuthNo = $relatedTxAuthNo;
	}
	
	/**
     * setRepeatDetails 
     * 
     * * sets action's all compulsory data
     * 
     * @param string $description 
     * @param foat $amount
     * @param string $relatedVPSTxId
     * @param string $relatedVendorTxCode
     * @param string $relatedSecurityKey
     * @param long int $relatedTxAuthNo     
     * @access public
     * @return void
     */
	public function setRepeatDetails($description, $amount, $relatedVPSTxId, $relatedVendorTxCode, $relatedSecurityKey, $relatedTxAuthNo)
	{
		$this->_description = $description;
		$this->_amount = $amount;
		$this->_relatedVPSTxId = $relatedVPSTxId;
		$this->_relatedVendorTxCode = $relatedVendorTxCode;
		$this->_relatedSecurityKey = $relatedSecurityKey;
		$this->_relatedTxAuthNo = $relatedTxAuthNo;
	}
	
	/**
     * getPreparedData
     * 
     * gets action's data for sending to SagePay
     * 
     * @access public
     * @return string
     */
	public function getPreparedData()
	{
		$this->validateDetails();
		$string = parent::getPreparedData();
		$string .= '&Amount='.urlencode($this->_amount);
		$string .= '&Currency='.urlencode($this->_currency);
		$string .= '&Description='.urlencode($this->_description);
		$string .= '&RelatedVPSTxId='.urlencode($this->_relatedVPSTxId);
		$string .= '&RelatedVendorTxCode='.urlencode($this->_relatedVendorTxCode);
		$string .= '&RelatedSecurityKey='.urlencode($this->_relatedSecurityKey);
		$string .= '&RelatedTxAuthNo='.urlencode($this->_relatedTxAuthNo);
		return $string;
	}
	
	/**
     * validateDetails
     * 
     * validates action's data before sending to SagePay
     * 
     * @access private
     * @return boolean
     */
	private function validateDetails()
	{
		try 
		{
			if(!isset($this->_amount))
			{
				throw new exception ("Amount is not set"); 
			}
			elseif(($this->_amount < 1.00) || ($this->_amount > 100000.00))
			{
				throw new exception ("Amount must be more than 1.00 but less than 100,000.00");
			}
			
			if(!isset($this->_description))
			{
				throw new exception ("Description is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 100))
			{
				throw new exception ("Description must be upto 100 characters");
			}
			
			if(!isset($this->_currency))
			{
				throw new exception ("Currency is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_description, 3))
			{
				throw new exception ("Currency must be upto 3 characters");
			}
			
			if(!isset($this->_relatedVPSTxId))
			{
				throw new exception ("RelatedVPSTxId is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_relatedVPSTxId, 38))
			{
				throw new exception ("RelatedVPSTxId must be upto 38 characters");
			}
			
			if(!isset($this->_relatedVendorTxCode))
			{
				throw new exception ("RelatedVendorTxCode is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_relatedVendorTxCode, 40))
			{
				throw new exception ("RelatedVendorTxCode must be upto 40 characters");
			}
			
			if(!isset($this->_relatedSecurityKey))
			{
				throw new exception ("RelatedSecurityKey is not set"); 
			}
			elseif(SagePayDirect::validateLength($this->_relatedSecurityKey, 10))
			{
				throw new exception ("RelatedSecurityKey must be upto 10 characters");
			}
			
			if(!isset($this->_relatedTxAuthNo))
			{
				throw new exception ("RelatedTxAuthNo is not set");
			}			
		}
		catch (exception $exception)
		{
			trigger_error($exception->getMessage(), E_USER_NOTICE);
		}		
		return true;
	}
}
?>