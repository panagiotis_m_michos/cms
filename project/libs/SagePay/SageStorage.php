<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    SageService.php 2012-04-25 razvanp@madesimplegroup.com
 */


class SageStorage {
    /**
	 * authentication information will be stored here
	 * @var array
	 */
	private $storage;

	public function __construct(&$var) {
		$this->storage = &$var;
	}
	
	public function clear() {
		$this->storage = array();
	}

	/**
	 * get value from key
	 * @param string $key
	 * @return mixed
	 */
	public function get($key) {
		return isset($this->storage[$key]) ? unserialize($this->storage[$key]) : false;
	}

	/**
	 * use persistent array like $_SESION['transaction']
	 * storage is required for 3d authentication to keep track of transaction
	 * identity
	 * @param array $var
	 */
	public function setStorage(&$var) {
		$this->storage = &$var;
	}

	/**
	 * save value inside storage
	 * @param string $key
	 * @param mixed $value
	 */
	public function save($key, $value) {
		$this->storage[$key] = serialize($value);
	}
}


