<?php

/**
 * CMS
 *
 * Copyright (c) 2008, 2009 Stanislav Bazik (http://flue-ex.com)
 *
 * @copyright  Copyright (c) 2008, 2009 Stanislav Bazik
 * @license    GPL
 * @link       http://flue-ex.com
 * @category   Project
 * @package    CMS
 * @author     Razvan Preda
 * @version    SageService.php 2012-04-25 razvanp@madesimplegroup.com
 */


class SageFactory {
    /**
	 * @return Storage
	 */
	public function createStorage($namespace = NULL) {
		$namespace = FApplication::$session->getNamespace(PaymentViaPhoneAdminControler::$nameSpace);
		if (empty($namespace->transaction))
			$namespace->transaction = array();
		$storage = new SageStorage($namespace->transaction);
		return $storage;
	}
    
    public function createGateway(Storage $storage = null) {
		$storage = $storage ? $storage : $this->createStorage();
		$config = FApplication::$config['sage'];
		$sage = new SagePayment(new Identity($config['vendor']), $storage, $config['system']);
		return $sage;
	}
    
}

