<?php

/**
 * cms
 *
 * @license    GPL
 * @category   Project
 * @package    cms
 * @author     Razvan Preda <razvanp@madesimplegroup.com>
 * @version    TokenCardDetails.php 28-Aug-2012 
 */

class TokenCardDetails extends Exception {
	
    /**
     * @var array 
     */
    public $data;
    
    /**
     * @var SagePayAddress 
     */
    private $address;
    
    /**
     * @param type $data
     * @param SagePayAddress $address
     */
    public function __construct($data, SagePayAddress $address) {
        $this->data = $data;
        $this->address = $address;
    }
    
   /**
    * @return string
    */
    public function getFirstNames() 
    {
        $cardholderName = isset($this->data['cardholder']) ? $this->data['cardholder'] : FALSE;
        if (!$cardholderName) {
            return 'M';
        }
        $names = explode(' ', trim($cardholderName));
        if (count($names) > 1) {
            return substr(trim($cardholderName), 0, '-'.strlen(array_pop($names)));
        } else {
            return 'M';
        }
    
    }

    /**
     * @return null
     */
    public function getSurname() {
        if (isset($this->data['cardholder']) && !empty($this->data['cardholder']) && !is_int($this->data['cardholder'])) {
            $names = explode(' ', trim($this->data['cardholder']));
            //var_dump(count($names));
            if (count($names) == 1) {
                return $names[0];
            }
            return array_pop($names);
        }
        
        return NULL;
        
    }
    
    /**
     * @return string | null
     */
    public function getAddresLine1() {
        if (isset($this->data['address1']) && !empty($this->data['address1'])) {
            return substr(trim($this->data['address1']), 0, 100);
        } 
        return NULL;
    }
    
    /**
     * @return null | string
     */
    public function getAddresLine2() {
        if (isset($this->data['address2']) && isset($this->data['address3'])) {
            return substr(trim($this->data['address2'] . ' '. $this->data['address3']), 0, 100);
        } 
        return NULL;
    }
    
    /**
     * @return null | string
     */
    public function getCity() {
        if (isset($this->data['town']) && !empty($this->data['town'])) {
            return substr(trim(ucfirst($this->data['town']) ), 0, 40);
        } 
        return NULL;
    }
    
    /**
     * @return null | string
     */
    public function getPostCode() {
        if (isset($this->data['postcode']) && !empty($this->data['postcode'])) {
            return substr(trim($this->data['postcode'] ), 0, 10);
        } 
        return NULL;
    }
    
    /**
     * @return null | string
     */
    public function getCountry() {
        if (isset($this->data['country']) && !empty($this->data['country'])) {
            return substr(trim(strtoupper($this->data['country']) ), 0, 2);
        } 
        return NULL;
    }
    
    /**
     * @return null | string
     */
    public function getState() {
        if (isset($this->data['billingState']) && !empty($this->data['billingState']) && $this->data['billingState'] <> '?') {
            return substr(trim(strtoupper($this->data['billingState']) ), 0, 2);
        } 
        return NULL;
    }
    
    /**
     * @return null | string
     */
    public function getPhone() {
         if (isset($this->data['phone']) && !empty($this->data['phone'])) {
            return substr(trim($this->data['phone']), 0, 20);
        } 
        return NULL;
    }
    
    protected function setAddress() {
        $this->address->setFirstnames($this->getFirstNames());
        $this->address->setSurname($this->getSurname());
        $this->address->setLine1($this->getAddresLine1());
        $this->address->setLine2($this->getAddresLine2());
        $this->address->setCity($this->getCity());
        $this->address->setCountry($this->getCountry());
        $this->address->setState($this->getState());
        $this->address->setPostcode($this->getPostCode());
        $this->address->setPhone($this->getPhone());
        
        $this->address->validateDetails();
    }
    
    /**
     * 
     * @return SagePayAddress
     */
    public function getAddress() {
        $this->setAddress();
        return $this->address;
    }
    
}