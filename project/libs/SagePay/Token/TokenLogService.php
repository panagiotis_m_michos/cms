<?php

class TokenLogService
{
    /**
     * @param $token
     * @param $post
     * @param string $status
     * @param int $tokenLogId
     * @return int|null
     */
    public function processTokenLog($token, $post, $status = TokenLogs::TOKEN_STATUS_INCOLPLETE, $tokenLogId = 0)
    {

        if ($tokenLogId == 0 && $token) {
            return $this->saveTokenLog($token, $post, $status);
        }

        if ($token) {
            $tokenLog = new TokenLogs($tokenLogId);
            $tokenLog->tokenStatus = $status;
            $tokenLog->save();
        }

        if (isset($_SESSION['token']) && $this->getTokenLogId()) {
            $tokenLog = new TokenLogs($this->getTokenLogId());
            $tokenLog->tokenStatus = $status;
            $tokenLog->save();
        }

        $this->removeTokenLogId();
        return NULL;
    }

    /**
     * @param type $token
     * @param $post
     * @param type $status
     * @return int id
     */
    public function saveTokenLog($token, $post, $status)
    {

        $tokenLog = new TokenLogs();
        $tokenLog->tokenStatus = $status;
        $tokenLog->token = $token;
        $tokenLog->cardNumber = $post['cardShortNumber'];
        $tokenLog->name = $post['cardholder'];
        $tokenLog->address = $post['address1'] . ', ' . $post['town'] . ', ' . $post['postcode'] . ', ' . $post['country'];
        $tokenLog->email = isset($post['emails']) ? $post['emails'] : NULL;
        $tokenLog->dtc = new DateTime;
        $tokenLog->dtm = new DateTime;

        return $tokenLog->save();
    }

    /**
     * @return int $tokenLogId
     */
    public function getTokenLogId()
    {
        return isset($_SESSION['tokenLogId']) ? $_SESSION['tokenLogId'] : NULL;
    }

    /**
     * @param int $tokenLogId
     */
    public function saveTokenLogId($tokenLogId)
    {
        $_SESSION['tokenLogId'] = $tokenLogId;
    }

    /**
     * @return void
     */
    public function removeTokenLogId()
    {
        if (isset($_SESSION['tokenLogId'])) {
            unset($_SESSION['tokenLogId']);
        }
    }

    /**
     * @param type $response
     * @param type $token
     * @param type $post
     */
    public function processRemovedTokens($response, $token, $post)
    {
        if (trim($response['Status']) == 'OK') {
            $this->processTokenLog($token, $post, TokenLogs::TOKEN_STATUS_REMOVED);
        } else {
            $this->processTokenLog($token, $post, TokenLogs::TOKEN_STATUS_REMOVED_ERROR);
        }
    }

}
