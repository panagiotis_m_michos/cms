<?php

/**
 * cms
 *
 * @license    GPL
 * @category   Project
 * @package    cms
 * @author     Razvan Preda <razvanp@madesimplegroup.com>
 * @version    TokenCardInformations.php 28-Aug-2012 
 */

class TokenCardInformations {
    
    //put your code here
    
    public $data;
    
    /**
     * @var SagePayTokenCard
     */
    protected $cardMapper;
    
    /**
     * 
     * @param type $data
     * @param SagePayTokenCard
     */
    public function __construct($data, $cardMapper) {
        $this->data = $data;
        $this->cardMapper = $cardMapper;
    }
    
    /**
     * @return $cardType
     */
    public function getCardType() {
        $allowedCardType = SagePayDirect::getCardTypes();
        if (isset($this->data['cardType']) && array_key_exists(String::upper(trim($this->data['cardType'])), $allowedCardType)) {
            return $this->data['cardType'];
        }
        throw new TokenCardInformationsException('Invalid Card Type');
    }
    
    /**
     * @return $cardholderName
     */
    public function getCardholderName() {
        // Names
        if (isset($this->data['cardholder']) && !empty($this->data['cardholder']) && !is_int($this->data['cardholder'])) {
            
            $names = explode(' ', trim($this->data['cardholder']));
            if (count($names) > 1) {
                return substr(trim($this->data['cardholder']), 0, 50);
            } else {
                return substr(trim('M '. $this->data['cardholder']), 0, 50);
            }
        }
        throw new TokenCardInformationsException('Invalid Card Holder');
    }
    
    /**
     * 
     * @return int card number
     * @throws TokenCardInformationsException
     */
    public function getCardNumber()
    {
        $cardNumber = trim($this->data['cardNumber']);
        if (isset($cardNumber) && !empty($cardNumber) && strlen($cardNumber) >= 10 && strlen($cardNumber) <= 19) {
                return $cardNumber;
        }
        throw new TokenCardInformationsException('Invalid Card Number');
    }   
    
    /**
     * 
     * @return int card number
     * @throws TokenCardInformationsException
     */
    public function getCardExpiryDate()
    {
        $cardExpiryDate = $this->data['expiryDate'];
        if (isset($cardExpiryDate) && !empty($cardExpiryDate) && is_array($cardExpiryDate)) {
            if ($this->getFormatDate($cardExpiryDate)) {
                return $this->getFormatDate($cardExpiryDate);
            }
        }
        throw new TokenCardInformationsException('Invalid Card Date');
    }
    
    /**
     * 
     * @return int card number
     * @throws TokenCardInformationsException
     */
    public function getCardCV2()
    {
        $cardCV2 = $this->data['CV2'];
        if (isset($cardCV2) && !empty($cardCV2) && strlen($cardCV2) > 2 && strlen($cardCV2) < 5) {
            return $cardCV2;
        }
        throw new TokenCardInformationsException('Invalid card verification number');
    }
    
    /**
     * @return $cardValidFrom | null
     */
    public function getCardValidFrom()
    {
        $cardType = $this->getCardType();
        if ($cardType == SagePayDirect::CARD_MAESTRO) {
            $cardValidFrom = $cardCV2 = $this->data['validFrom'];
            if (isset($cardValidFrom) && !empty($cardValidFrom) && is_array($cardValidFrom)) {
                if ($this->getFormatDate($cardValidFrom)) {
                    return $this->getFormatDate($cardValidFrom);
                } else {
                    //throw new TokenCardInformationsException('Card Start Date Incorect');
                    return NULL;
                }
            }
        }
        return NULL;
    }
    
    /**
     * @return $issueNumber | null
     */
    public function getCardIssueNumber() 
    {
        $issueNumber = $this->data['issueNumber'];
        if (isset($issueNumber) && !empty($issueNumber)) {
            return $issueNumber;
        }
        return NULL;
    }
    
    /**
     * @param type $formDate
     * @return string
     */
    public function getFormatDate($formDate) {
        if (isset($formDate['m']) && is_int( (int) $formDate['m']) && (int) $formDate['m'] > 0) {
            $m = (int) $formDate['m'] < 10 && (int) $formDate['m'] > 0 ? str_pad($formDate['m'], 2, '0', STR_PAD_LEFT) : $formDate['m'];
        } 
            
        if (isset($formDate['y']) && is_int( (int) $formDate['y']) && strlen($formDate['y']) == 4 && (int) $formDate['y'] > 0) {
            $y = substr($formDate['y'], -2, 2);
        }

        if ( isset($m) && isset($y) ) {
            return $m.$y;
        } 
        
        return NULL;
    }
    
    /**
     * set card details
     */
    protected function mapCard() {
        
        $this->cardMapper->setCardType($this->getCardType());
        $this->cardMapper->setCardholderName($this->getCardholderName());
        $this->cardMapper->setCardNumber($this->getCardNumber());
        $this->cardMapper->setCVV($this->getCardCV2());
        $this->cardMapper->setExpiryDate($this->getCardExpiryDate());
        $this->cardMapper->setIssueNumber($this->getCardIssueNumber());
        $this->cardMapper->setStartDate($this->getCardValidFrom());
    }
    
    public function getCard() {
        $this->mapCard();
        return $this->cardMapper;
    }
    
}

