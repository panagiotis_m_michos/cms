<?php

use Utils\Date;

class DirectTokenPayment
{

    /**
     * @var SagePayDirect 
     */
    private $sage;
    /**
     *
     * @var arraye 
     */
    private $post;
    
    /**
     * 
     * @param SagePayDirect $sageDirect
     * @param array $post
     */
    public function __construct(SagePayDirect $sageDirect, $post)
    {
        $this->sage = $sageDirect;
        $this->post = $post;
    }

    /**
     * @return string $token
     */
    public function getDirectPaymentToken()
    {
        $sagePayCardInfo = new TokenCardInformations($this->post, new SagePayTokenCard());
        $sagePayTokencard = $sagePayCardInfo->getCard();
        $response = $this->registerToken($sagePayTokencard);
        return $this->handleRegisterTokenResponse($response);
    }
    
    
    /**
     * 
     * @param SagePayTokenCard $cardInfo
     * @return type
     */
    public function registerToken(SagePayTokenCard $cardInfo) 
    {
        $registerTokenAction = new SagePayTokenGenerate();
        $registerTokenAction->setCard($cardInfo);
        $this->sage->setAction($registerTokenAction);
        return $this->sage->doAction();
    }
    
    /**
     * 
     * @param type $response
     * @return $token
     * @throws RegisterTokenResponseException
     */
    public function handleRegisterTokenResponse($response) 
    {
        if (!empty($response) && isset($response['Status'])) {
            if (trim(String::upper($response['Status'])) == 'OK') {
                return trim(str_replace(array('{','}'), '', $response['Token']));
            }
            throw new Exception($response['Status'] . ' ' .$response['StatusDetail']);
        }
        throw new RegisterTokenResponseException(SageService::NO_SAGE_PAY_RESPONSE);
    }
    
    /**
     * @param type $token
     */
    public function removeTransactionToken($token = NULL)
    {
        if (isset($token)) {
            $tokenSage = new SagePayTokenRemove();
            $tokenSage->setToken($token);
            $this->sage->setAction($tokenSage);
            $response =  $this->sage->doMakeRequest();
            return $response;
            //PaymentModel::tokenPaymentsLogs($response['Status'], $response['StatusDetail'], $_SESSION['token'], 'FAILED');
        }
    }
    
    /**
     * @return \SagePayTokenPayment
     */
    public function createTokenAction($token, $basket, $storeToken = 1)
    {
        
        $tokenPayment = new SagePayTokenPayment();
        
        //set Token key
        $tokenPayment->setToken($token);
        $tokenPayment->setStoreToken($storeToken);

        //set address 
        $sagePayCardDetails = new TokenCardDetails($this->post, new SagePayAddress());    
        $billingAddress = $sagePayCardDetails->getAddress();
        $deliveryAddress = $sagePayCardDetails->getAddress();
        $tokenPayment->setBillingAddress1($billingAddress);
        $tokenPayment->setDeliveryAddress1($deliveryAddress);
  
        //set token email
        if (isset($this->post['emails']) && !empty($this->post['emails'])) {
            $tokenPayment->setCustomerEMail($this->post['emails']);
        }
        //set details
        $tokenPayment->setPaymentDetails("CMS Token System", $basket->getPrice('total'));
        return $tokenPayment;
    }
    
    /**
     * @param Customer $customer
     */
    public function handleOldToken(Customer $customer, $post)
    {
        $tokenModel = new TokensModel();
        if ($tokenModel->getCustomerToken($customer)) {
            $oldToken = $tokenModel->getCustomerToken($customer);
            $token = $oldToken->token; 
            $oldToken->delete();
            return $token ? $token : NULL;
        }
        return NULL;
    }


    /**
     * @param Customer $customer
     * @param $post
     * @param Tokens $token
     */
    public function saveDirectPaymentToken(Customer $customer, $post, $token = NULL)
    {
         
        $oldToken = NULL;
        if ($token) {
            $oldToken = $this->handleOldToken($customer, $post);
            //this comes as array(m => 1, y => 2009)
            $cardExpiryDate = Date::create('mY', str_pad(implode('', $post['expiryDate']), 6, '0', STR_PAD_LEFT));
            $cardExpiryDate->modify('last day of this month');
            $tokenRecord = new Tokens();
            $tokenRecord->customerId  = $customer->getId();
            $tokenRecord->token       = $token;
            $tokenRecord->cardNumber  = $post['cardShortNumber'];
            $tokenRecord->cardHolder  = $post['cardholder'];
            $tokenRecord->cardType  = $post['cardType'];
            $tokenRecord->cardExpiryDate  = $cardExpiryDate->format('Y-m-d');
            $tokenRecord->firstname   = $customer->firstName;
            $tokenRecord->surname     = $customer->lastName;
            $tokenRecord->address1    = $post['address1'];
            $tokenRecord->address2    = isset($post['address2']) ? $post['address2'] : '';
            $tokenRecord->city        = $post['town'];
            $tokenRecord->state       = $post['billingState'] != '?' ? $post['billingState'] : '';
            $tokenRecord->postCode    = $post['postcode'];
            $tokenRecord->country     = $post['country'];
            $tokenRecord->isCurrent = TRUE;
            $tokenRecord->save();
            if (isset($_SESSION['token'])) {
                unset($_SESSION['token']);
            }
        }
        
        //log old token
        if ($oldToken) {
            $response = $this->removeTransactionToken($oldToken);
            $tokenLogService = new TokenLogService($post);
            $tokenLogService->processRemovedTokens($response, $oldToken, $post);
        }
    }

    /**
     *
     * @param type $token
     * @param Basket $basket
     * @param int $storeToken
     * @param bool $disable3dSecure
     * @return type
     * @throws SageAuthenticationException
     */
    public function doDirectTokenAction($token, Basket $basket, $storeToken = 1, $disable3dSecure = FALSE)
    {
        $backUrl = FApplication::$router->secureLink(PaymentControler::SAGE_AUTH_VALIDATION, array('sagepay-3d'=>1, 'storeToken' => $storeToken));
        $tokenPayment = $this->createTokenAction($token, $basket, $storeToken);
        if ($disable3dSecure) {
            $tokenPayment->setApply3DSecure(2);
        }

        $this->sage->setAction($tokenPayment);
        return $this->sage->doAction($backUrl);
    }
}
