<?php

require_once PROJECT_DIR . '/libs/Paypal/paypal.php';

use Loggers\PaypalLogger;
use Payment\PaypalEmailer;

class PaypalService
{
    const PAYPAL_ACK_SUCCESS_RESPONSE = 'Success';
    const PAYPAL_ACK_FAILURE_RESPONSE = 'Failure';

    /**
     * @var string Session
     */
    public static $nameSpace = 'paymentPaypalRequestDetails';

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var FForm
     */
    private $form;

    /**
     * @var array
     */
    private $query;

    /**
     * @var array
     */
    private $post;

    /**
     * @var array
     */
    private $credentials = array();

    /**
     * @param Basket $basket
     * @param FForm $form
     * @param array $query
     * @param array $post
     */
    public function __construct(Basket $basket, FForm $form, array $query, $post)
    {
        $this->basket = $basket;
        $this->form = $form;
        $this->query = $query;
        $this->post = $post;
        $this->credentials = FApplication::$config['paypal'];
    }

    /**
     * @param PaymentService $paymentService
     * @throws PaypalException
     * @return array
     */
    public function process(PaymentService $paymentService)
    {
        if (!isset($this->query['token'])) {
            //https://cms/page1223en.html?token=EC-46D62651JV472025R&PayerID=5GSDTEDFS2HZG
            $session = FApplication::$session->getNameSpace(self::$nameSpace);
            $session->paypalEmailNewCustomer = $this->post['emails'];
            $this->paypalSetExpressCheckout();

        } else {
            $response = $this->paypalDoExpressCheckoutPayment();
            $responseDetails = $this->paypalGetExpressCheckoutPayment();

            if (!isset($response['AMT'])) {
                throw new PaypalException("Sorry, transaction couldn't be completetd. Please try again later.");
            }

            $basketPrice = $this->basket->getPrice('total');
            $responsePrice = urldecode($response['AMT']);
            if ($basketPrice != $responsePrice) {
                throw new PaypalException("Please note, you added extra item(s) to your basket during the payment process and these item(s) were charged for.");
            }

            $paymentResponse = $this->getPaymentResponse($paymentService, $response, $responseDetails);
            $paymentService->processCustomerInformations($paymentResponse);
            $paymentService->completeTransaction($paymentResponse);
        }
    }

    /**
     * @param PaymentService $paymentService
     * @param array $response
     * @param array $responseDetails
     * @return PaymentResponse
     * @throws PaypalNoResponseException
     */
    private function getPaymentResponse(PaymentService $paymentService, $response, $responseDetails)
    {
        $session = FApplication::$session->getNameSpace(self::$nameSpace);
        $paymentResponse = new PaymentResponse();
        $paymentResponse->setTypeId(Transaction::TYPE_PAYPAL);
        if ($response['ACK'] == self::PAYPAL_ACK_SUCCESS_RESPONSE && isset($response['TRANSACTIONID'])) {
            $paymentResponse->setPaymentOrderCode($response['TRANSACTIONID']);
            $paymentResponse->setPaymentTransactionTime(urldecode($response['ORDERTIME']));
            if ($responseDetails['ACK'] == self::PAYPAL_ACK_SUCCESS_RESPONSE) {
                //$paymentResponse->setPaymentHolderEmail($responseDetails['EMAIL'] ? urldecode($responseDetails['EMAIL']) : NULL);
                $paymentResponse->setPaymentHolderEmail($session->paypalEmailNewCustomer);
                $paymentResponse->setPaymentHolderFirstName(
                    isset($responseDetails['FIRSTNAME']) ? urldecode($responseDetails['FIRSTNAME']) : NULL
                );
                $paymentResponse->setPaymentHolderLastName(
                    isset($responseDetails['LASTNAME']) ? urldecode($responseDetails['LASTNAME']) : NULL
                );
                $paymentResponse->setPaymentHolderAddressStreet(
                    isset($responseDetails['SHIPTOSTREET']) ? urldecode($responseDetails['SHIPTOSTREET']) : NULL
                );
                $paymentResponse->setPaymentHolderAddressCity(
                    isset($responseDetails['SHIPTOCITY']) ? urldecode($responseDetails['SHIPTOCITY']) : NULL
                );
                $paymentResponse->setPaymentHolderAddressState(
                    isset($responseDetails['SHIPTOSTATE']) ? urldecode($responseDetails['SHIPTOSTATE']) : NULL
                );
                $paymentResponse->setPaymentHolderAddressPostCode(
                    isset($responseDetails['SHIPTOZIP']) ? urldecode($responseDetails['SHIPTOZIP']) : NULL
                );
                $paymentResponse->setPaymentHolderAddressCountry(
                    isset($responseDetails['SHIPTOCOUNTRYCODE']) ? urldecode($responseDetails['SHIPTOCOUNTRYCODE']) : NULL
                );
            }
            $paymentResponse->setPaymentTransactionDetails(serialize($response));
            unset($session->paypalEmailNewCustomer);
            return $paymentResponse;
        } else {
            throw new PaypalNoResponseException('No response!');
        }
    }

    /**
     * Provides set express checkout method
     *
     * @return void
     * @throws Exception
     */
    private function paypalSetExpressCheckout()
    {
        try {
            $paypal = new SetExpressCheckout($this->basket->getPrice('total'));
            $paypal->setPayPalNVP("API_UserName", $this->credentials['apiUsername']);
            $paypal->setPayPalNVP("API_Password", $this->credentials['apiPassword']);
            $paypal->setPayPalNVP("API_Signature", $this->credentials['apiSignature']);
            $paypal->setPayPalNVP("environment", $this->credentials['environment']);
            $paypal->setNVP("RETURNURL", $this->getReturnUrl());
            $paypal->setNVP("CANCELURL", $this->getCancelUrl());
            $paypal->setNVP("L_NAME0", 'Companies Made Simple');
            $paypal->setNVP("L_AMT0", $this->basket->getPrice('total'));
            $paypal->setNVP("L_QTY0", 1);
            $paypal->setNVP("NOSHIPPING", 2);
            $paypal->setNVP("CURRENCYCODE", 'GBP');
            $paypal->setNVP("CUSTOM", 'COMPANIESMADESIMPLE');
            $paypal->getResponse();
        } catch (Exception $e) {
            throw new PaypalFailureException($e->getMessage());
        }
    }

    /**
     * Provides do express checkout
     *
     * @return array
     * @throws Exception
     */
    private function paypalDoExpressCheckoutPayment()
    {
        try {
            if (empty($this->query['PayerID'])) {
                throw new Exception('You have cancelled your PayPal payment!');
            }
            $paypal = new DoExpressCheckoutPayment($this->basket->getPrice('total'));
            $paypal->setPayPalNVP("API_UserName", $this->credentials['apiUsername']);
            $paypal->setPayPalNVP("API_Password", $this->credentials['apiPassword']);
            $paypal->setPayPalNVP("API_Signature", $this->credentials['apiSignature']);
            $paypal->setPayPalNVP("environment", $this->credentials['environment']);
            $paypal->setNVP("L_NAME0", 'Companies Made Simple');
            $paypal->setNVP("L_AMT0", $this->basket->getPrice('total'));
            $paypal->setNVP("L_QTY0", 1);
            $paypal->setNVP("NOSHIPPING", 2);
            $paypal->setNVP("CURRENCYCODE", 'GBP');
            $paypal->setNVP("CUSTOM", 'COMPANIESMADESIMPLE');
            return $paypal->getResponse();
        } catch (Exception $e) {
            throw new PaypalFailureException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Provides do express checkout
     *
     * @return array
     * @throws Exception
     */
    private function paypalGetExpressCheckoutPayment()
    {
        try {
            $paypal = new GetExpressCheckoutDetails();
            $paypal->setPayPalNVP("API_UserName", $this->credentials['apiUsername']);
            $paypal->setPayPalNVP("API_Password", $this->credentials['apiPassword']);
            $paypal->setPayPalNVP("API_Signature", $this->credentials['apiSignature']);
            $paypal->setPayPalNVP("environment", $this->credentials['environment']);
            return $paypal->getResponse();
        } catch (Exception $e) {
            throw new PaypalFailureException($e->getMessage());
        }
    }

    /**
     * @return string
     */
    private function getReturnUrl()
    {
        $router = FApplication::$router;
        $protocol = FApplication::$httpRequest->uri->scheme . '://';
        $host = FApplication::$httpRequest->uri->host;
        $hostUri = $router->link(PaymentControler::SAGE_PAGE);
        $returnUrl = sprintf("%s%s%s", $protocol, $host, $hostUri);
        return $returnUrl;
    }

    /**
     * @return string
     */
    private function getCancelUrl()
    {
        $router = FApplication::$router;
        $protocol = FApplication::$httpRequest->uri->scheme . '://';
        $host = FApplication::$httpRequest->uri->host;
        $hostUri = $router->link(PaymentControler::SAGE_PAGE);
        $cancelUrl = sprintf("%s%s%s", $protocol, $host, $hostUri);
        return $cancelUrl;
    }

}
