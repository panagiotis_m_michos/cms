<?php

function __autoload($className=null) {
	$classes = array (
        'QConfig.php', 
        'QInflector.php', 
        'QTool.php', 
        'QGoogleGraph.php', 
        'QVizualisationGoogleGraph.php', 
        'QApikeyGoogleGraph.php', 
	);

	foreach($classes as $class) {
		include_once($class);
	}

	include_once($className.".php");

}