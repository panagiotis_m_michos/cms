<?php

/**
 * TODO
 *
 */
interface QIGoogleGraphInter{
    
    /**
     * interface design for render method
     *
     */
    public function render();
    
    /**
     * interface design for columns setter
     *
     * @param array $columns
     */
    public function setColumns($columns = array());

    /**
     * interface design for values setter
     *
     * @param array $values
     */
    public function setValues($values = array());
    
}
