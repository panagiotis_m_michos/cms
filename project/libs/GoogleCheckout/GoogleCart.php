<?php
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Peter Reisinger - p.reisinger@gmail.com                      |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * include item 
 */
require_once('GoogleItem.php');

/**
 * GoogleCart 
 * 
 * @package googlecheckout
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
class GoogleCart {
    const CART_FILE         = '/xml/shoppingcart.xml';
    /**
     * development
     *
     * indicates if test account should be used
     *
     * @var boolean
     * @access private
     */
    private $development    = true;

    /**
     * merchantID
     * 
     * to be obtained from google
     *
     * @var string
     * @access private
     */
    private $merchantID     = '';

    /**
     * merchantKey
     *
     * to be obtained from google
     *
     * @var string
     * @access private
     */
    private $merchantKey    = '';

    /**
     * testID
     *
     * to be obtained from google
     * this is development merchantID
     *
     * @var string
     * @access private
     */
    private $testID         = '412118552222978';

    /**
     * testKey
     *
     * to be obtained from google
     * this is development merchantKey
     *
     * @var string
     * @access private
     */
    private $testKey        = 'pZMQ-t8vawDo0OfhI1Mw2A';

    /**
     * server
     *
     * holds url for requests
     *
     * @var string
     * @access private
     */
    private $server;

    /**
     * items 
     *
     * hodls all item tags
     * 
     * @var array
     * @access private
     */
    private $items          = array();

    /**
     * __construct 
     * 
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->server = ($this->development) ?
            'https://sandbox.google.com/checkout':'https://checkout.google.com';
    }

    /**
     * setLive 
     *
     * set true if you want live
     * set false if you test
     * 
     * @param boolean $live 
     * @access public
     * @return void
     */
    public function setLive($live)
    {
        if ($live) {
            $this->development  = false;
            $this->server       = 'https://checkout.google.com';
        } else {
            $this->development  = true;
            $this->server       = 'https://sandbox.google.com/checkout';
        }
    }

    /**
     * getButton
     *
     * @param string $size size ex: 'w=180&h=46'
     * @param string $style trans|white background color
     * @param string $variant text|disabled clickable or disabled
     * @param string $loc en_GB|en_US GB
     * @static
     * @access private
     * @return string link for button
     *
     * TODO check if passed values are correct
     * if no then thorw exception
     */
    private static function getButton
        ($size, $style = 'trans', $variant = 'text', $loc = 'en_GB')
    {
        $url    = ($this->development) ?
            'http://sandbox.google.com/checkout':'http://checkout.google.com';
        $style  = ($style == 'trans') ? 'style=trans' : 'style=white';
        $variant= ($variant == 'text') ? 'variant=text' : 'variant=disabled';
        $loc    = ($loc = 'en_GB') ? 'loc=en_GB' : 'loc=en_US';

        $link   = $url . '/buttons/checkout.gif?merchant_id=';
        $link  .= ($this->development) ? $this->testID : $this->merchantID;
        $link  .= '&' . $size . '&' . $style;
        $link  .= '&' . $variant . '&' . $loc;

        return $link;
    }

    /**
     * getLargeButton 
     *
     * returns link for button to be used
     * inside src attribute
     * button is 180 x 46
     * 
     * @param string $style trans|white default trans
     * @param string $variant text|disabled default disabled 
     * @param string $loc en_GB|en_US default en_GB
     * @static
     * @access public
     * @return string
     */
    public static function getLargeButton($style = 'trans', $variant = 'text', $loc = 'en_GB')
    {
        return self::getButton('w=180&h=46', $style, $variant, $loc);
    }

    /**
     * getMediumButton 
     *
     * returns link for button to be used
     * inside src attribute
     * button is 168 x 44
     * 
     * @param string $style trans|white default trans
     * @param string $variant text|disabled default disabled 
     * @param string $loc en_GB|en_US default en_GB
     * @static
     * @access public
     * @return void
     */
    public static function getMediumButton($style = 'trans', $variant = 'text', $loc = 'en_GB')
    {
        return self::getButton('w=168&h=44', $style, $variant, $loc);
    }

    /**
     * getSmallButton 
     *
     * returns link for button to be used
     * inside src attribute
     * button is 160 x 43
     * 
     * @param string $style trans|white default trans
     * @param string $variant text|disabled default disabled 
     * @param string $loc en_GB|en_US default en_GB
     * @static
     * @access public
     * @return void
     */
    public static function getSmallButton($style = 'trans', $variant = 'text', $loc = 'en_GB')
    {
        return self::getButton('w=160&h=43', $style, $variant, $loc);
    }

    public function addItem(GoogleItem $item)
    {
        $this->items[] = $item;
    }

    /**
     * TODO - handle errors!!!
     */
    public function doCheckout()
    {
        if (empty($this->items)) {
            trigger_error('You need to add items before calling doCheckout()', E_USER_ERROR);
        }

        $request = simplexml_load_file(dirname(__FILE__).self::CART_FILE);

        // --- get all items ---
        $items = '';
        foreach($this->items as $value) {
            // --- remove xml declaration ---
            $items .= trim(preg_replace('/<\?xml.*\?>/', '', $value->getItem(), 1));
        }

        // --- insert items ---
        $request = trim(str_replace('<?items ?>', $items, $request->asXml()));

        //echo $request;exit;

        //---                           ---//
        // send xml - $request using https //
        //---                           ---//

        //get username and password, plus merchant id
        $pass = ($this->development) ? "$this->testID:$this->testKey" :
            "$this->merchantID:$this->merchantKey";
        $merchantID = ($this->development) ? $this->testID : $this->merchantID;

        $ch=curl_init(
            $this->server . '/api/checkout/v2/merchantCheckout/Merchant/' .
            $merchantID
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $pass);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $request);
        $result = curl_exec($ch);
        curl_close($ch);

        //get url where to redirect from response
        $result = simplexml_load_string($result);
        if (isset($result->{'error-message'})) {
            trigger_error((string) $result->{'error-message'}, E_USER_ERROR);
        }
        
        $url = (string) $result->{'redirect-url'};

        //redirect to google page
        header("Location: $url");exit;
    }
}
