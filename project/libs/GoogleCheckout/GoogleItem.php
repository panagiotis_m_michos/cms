<?php
/*
+-------------------------------------------------------------------------------+
|   Copyright 2009 Peter Reisinger - p.reisinger@gmail.com                      |
|                                                                               |
|   This program is free software: you can redistribute it and/or modify        |
|   it under the terms of the GNU General Public License as published by        |
|   the Free Software Foundation, either version 3 of the License, or           |
|   (at your option) any later version.                                         |
|                                                                               |
|   This program is distributed in the hope that it will be useful,             |
|   but WITHOUT ANY WARRANTY; without even the implied warranty of              |
|   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               |
|   GNU General Public License for more details.                                |
|                                                                               |
|   You should have received a copy of the GNU General Public License           |
|   along with this program.  If not, see <http://www.gnu.org/licenses/>.       |
+-------------------------------------------------------------------------------+ 
 */

/**
 * GoogleItem 
 * 
 * @package googlecheckout
 * @version $id$
 * @copyright 2009 Peter Reisinger
 * @author Peter Reisinger <p.reisinger@gmail.com> 
 * @license GNU General Public License
 */
class GoogleItem {

    /**
     * name 
     *
     * item-name
     * 
     * @var string
     * @access private
     */
    private $name;

    /**
     * description 
     *
     * item-description
     * 
     * @var string
     * @access private
     */
    private $description;

    /**
     * price 
     *
     * unit-price
     * 
     * @var float
     * @access private
     */
    private $price;

    /**
     * quantity 
     *
     * quantity
     * 
     * @var integer minimal allowed value is 1
     * @access private
     */
    private $quantity;

    /**
     * currency 
     *
     * attribute currency of unit-price tag
     * 
     * @var string
     * @access private
     */
    private $currency;

    /**
     * itemId 
     *
     * merchant-item-id
     * 
     * @var string
     * @access private
     */
    private $itemId;

    /**
     * privateData 
     *
     * merchant-private-item-data
     * 
     * @var string
     * @access private
     */
    private $privateData;

    /**
     * weight 
     *
     * item-weight
     * 
     * @var string
     * @access private
     */
    private $weight;

    /**
     * digitalContent 
     *
     * digital-content
     * 
     * @var array
     * @access private
     */
    private $digitalContent = array();

    /**
     * taxTableSelector 
     * 
     * @var string
     * @access private
     */
    private $taxTableSelector;

    /**
     * __construct 
     * 
     * @param string $name item-name
     * @param string $description item-description
     * @param float $price unit-price
     * @param integer $quantity quantity default 1
     * @param string $currency default is GBP
     * @access public
     * @return void
     */
    public function  __construct($name, $description, $price, $quantity = 1, $currency = 'USD')
    {
        if (!is_int($quantity) && $quantity < 1) {
            trigger_error
                ('quantity has to be integer bigger than 0', E_USER_ERROR);
        }
        $this->name         = $name;
        $this->description  = $description;
        $this->price        = $price;
        $this->quantity     = $quantity;
        $this->currency     = $currency;
    }

    /**
     * setItemId 
     * 
     * @param string $itemId merchant-item-id 
     * @access public
     * @return void
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * setPrivateData 
     * 
     * @param string $privateData merchant-private-item-data
     * @access public
     * @return void
     */
    public function setPrivateData($privateData)
    {
        $this->privateData = $privateData;
    }

    /**
     * setWeight 
     * 
     * @param string $weight item-weight
     * @access public
     * @return void
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * setDigitalContent 
     *
     * sets digital-content
     * http://code.google.com/apis/checkout/developer/Google_Checkout_Digital_Delivery.html
     * 
     * @param string $description instructions for downloading a digital content item
     * @param string $url URI from which the customer can download the purchased content
     * @param string $display OPTIMISTIC|PESIMISTIC default is PESIMISTIC
     * @param boolean $email merchant will send email to the buyer explaining how to access the digital content
     * @param string $key key needed to download or unlock a digital content item
     * @access public
     * @return void
     */
    public function setDigitalContent($description, $url = null, $display = 'OPTIMISTIC', $email = false, $key = null)
    {
        $this->digitalContent['description']        = $description;
        $this->digitalContent['display-disposition']= $display;
        $this->digitalContent['email-delivery']     = $email;
        if (!is_null($key)) {
            $this->digitalContent['key']            = $key;
        }
        if (!is_null($url)) {
            $this->digitalContent['url']            = $url;
        }
    }

    /**
     * setTaxTableSelector 
     * 
     * @param string $taxTableSelector tax-table-selector
     * @access public
     * @return void
     */
    public function setTaxTableSelector($taxTableSelector)
    {
        if (!is_string($taxTableSelector) &&
            strlen($taxTableSelector) > 255) {
            trigger_error('taxTableSelector - max 255 chars', E_USER_ERROR);
            }
        $this->taxTableSelector = $taxTableSelector;
    }

    /**
     * getItem 
     * 
     * @access public
     * @return xml
     */
    public function getItem()
    {
        $xml = new SimpleXMLElement('<item></item>');

        $xml->addChild('item-name', $this->name);
        $xml->addChild('item-description', $this->description);
        $xml->addChild('unit-price', $this->price);
        $xml->{'unit-price'}->addAttribute('currency', $this->currency);
        $xml->addChild('quantity', $this->quantity);

        if (isset($this->itemId)) {
            $xml->addChild('merchant-item-id', $this->itemId);
        }

        if (isset($this->privateData)) {
            $xml->addChild('merchant-private-item-data', $this->privateData);
        }

        if (isset($this->weight)) {
            $xml->addChild('item-weight', $this->weight);
        }

        if (!empty($this->digitalContent)) {
            $xml->addChild('digital-content', $this->digitalContent);
            foreach($this->digitalContent as $key => $value) {
                $xml->{'digital-content'}->addChild($key, $value);
            }
        }

        if (isset($this->taxTableSelector)) {
            $xml->addAttribute('tax-table-selector', $this->taxTableSelector);
        }

        return $xml->asXml();
    }
}
