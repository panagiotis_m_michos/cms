<?php

class CHAdapter extends Zend_Db_Adapter_Pdo_Mysql
{
    // tables
    const CH_ADDRESS_CHANGE = 'ch_address_change';
    const CH_BARCLAYS = 'ch_barclays';
    const CH_BARCLAYS_SOLETRADER = 'ch_barclays_soletrader';
    const CH_COMPANY = 'ch_company';
    const CH_COMPANY_CAPITAL = 'ch_company_capital';
    const CH_COMPANY_CAPITAL_SHARES = 'ch_company_capital_shares';
    const CH_COMPANY_MEMBER = 'ch_company_member';
    const CH_FORM_SUBMISSION = 'ch_form_submission';
    const CH_FORM_SUBMISSION_ERROR = 'ch_form_submission_error';
    
    
    /** @var array */
    static public $tables = Array(
        self::CH_ADDRESS_CHANGE,
        self::CH_BARCLAYS,
        self::CH_BARCLAYS_SOLETRADER,
        self::CH_COMPANY,
        self::CH_COMPANY_CAPITAL,
        self::CH_COMPANY_CAPITAL_SHARES,
        self::CH_COMPANY_MEMBER,
        self::CH_FORM_SUBMISSION,
        self::CH_FORM_SUBMISSION_ERROR,
    );
    
    public function generateDate($table, array $bind, $type = TRUE) 
    {
        if(!in_array($table, self::$tables)) {
            return $bind;
        }
        
        if($type) {
            $bind['dtc'] = date('Y-m-d H:i:s');
            $bind['dtm'] = date('Y-m-d H:i:s');
        }else{
            $bind['dtm'] = date('Y-m-d H:i:s');
        }
        
        return $bind;
    }
    
    /**
     * Inserts a table row with specified data.
     *
     * @param mixed $table The table to insert data into.
     * @param array $bind Column-value pairs.
     * @return int The number of affected rows.
     */
    public function insert($table, array $bind)
    {
        $bind = $this->generateDate($table, $bind);
        
        // extract and quote col names from the array keys
        $cols = array();
        $vals = array();

        foreach ($bind as $col => $val) {
            $cols[] = $this->quoteIdentifier($col, TRUE);
            if ($val instanceof Zend_Db_Expr) {
                $vals[] = $val->__toString();
                unset($bind[$col]);
            } else {
                $vals[] = '?';
            }
        }

        // build the statement
        $sql = "INSERT INTO "
             . $this->quoteIdentifier($table, TRUE)
             . ' (' . implode(', ', $cols) . ') '
             . 'VALUES (' . implode(', ', $vals) . ')';

        // execute the statement and return the number of affected rows
        $stmt = $this->query($sql, array_values($bind));
        $result = $stmt->rowCount();
        return $result;
    }

    /**
     * Updates table rows with specified data based on a WHERE clause.
     *
     * @param  mixed        $table The table to update.
     * @param  array        $bind  Column-value pairs.
     * @param  mixed        $where UPDATE WHERE clause(s).
     * @return int          The number of affected rows.
     */
    public function update($table, array $bind, $where = '')
    {
        $bind = $this->generateDate($table, $bind, FALSE);
        
        /**
         * Build "col = ?" pairs for the statement,
         * except for Zend_Db_Expr which is treated literally.
         */
        $set = array();
        foreach ($bind as $col => $val) {
            if ($val instanceof Zend_Db_Expr) {
                $val = $val->__toString();
                unset($bind[$col]);
            } else {
                $val = '?';
            }
            $set[] = $this->quoteIdentifier($col, TRUE) . ' = ' . $val;
        }

        $where = $this->_whereExpr($where);

        /**
         * Build the UPDATE statement
         */
        $sql = "UPDATE "
             . $this->quoteIdentifier($table, TRUE)
             . ' SET ' . implode(', ', $set)
             . (($where) ? " WHERE $where" : '');
        /**
         * Execute the statement and return the number of affected rows
         */
        $stmt = $this->query($sql, array_values($bind));
        $result = $stmt->rowCount();
        return $result;
    }
}

