<?php

use Monolog\Registry;
use Psr\Log\LogLevel;

/**
 * Class to handle old Debug class calls
 */
class Debug extends OldDebug
{
    /**
     * @param mixed $message
     * @param string $priority
     */
    public static function log($message, $priority = NULL)
    {
        $level = $priority ? $priority : LogLevel::INFO;
        if ($message instanceof Exception) {
            $message = (string) $message;
        }
        Registry::getInstance('application')->log($level, (string) $message);
    }

    public static function enable($productionMode = NULL, $logFile = NULL, $email = NULL)
    {

    }

    public static function enableProfiler()
    {

    }
}