<?php

namespace CompanyHouse\Filling\Responses\Internal;

class Rejection
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $instanceNumber;

    /**
     * @param string $code
     * @param string $description
     */
    public function __construct($code, $description)
    {
        $this->setCode($code);
        $this->setDescription($description);
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getInstanceNumber()
    {
        return $this->instanceNumber;
    }

    /**
     * @param string $instanceNumber
     */
    public function setInstanceNumber($instanceNumber)
    {
        $this->instanceNumber = $instanceNumber;
    }

}
