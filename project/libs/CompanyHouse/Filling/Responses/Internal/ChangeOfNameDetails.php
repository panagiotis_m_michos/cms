<?php

namespace CompanyHouse\Filling\Responses\Internal;

class ChangeOfNameDetails
{
    /**
     * @var string
     */
    private $docRequestKey;


    public function __construct($docRequestKey)
    {
        $this->setDocRequestKey($docRequestKey);
    }

    /**
     * @return string
     */
    public function getDocRequestKey()
    {
        return $this->docRequestKey;
    }

    /**
     * @param string $docRequestKey
     */
    public function setDocRequestKey($docRequestKey)
    {
        $this->docRequestKey = $docRequestKey;
    }
}

