<?php

namespace CompanyHouse\Filling\Responses\Internal;

class IncorporationDetails
{
    /**
     * @var string
     */
    private $docRequestKey;

    /**
     * @var string
     */
    private $incorporationDate;

    /**
     * @var string
     */
    private $authenticationCode;


    /**
     * @param string $docRequestKey
     * @param string $incorporationDate
     * @param string $authenticationCode
     */
    public function __construct($docRequestKey, $incorporationDate, $authenticationCode)
    {
        $this->setDocRequestKey($docRequestKey);
        $this->setIncorporationDate($incorporationDate);
        $this->setAuthenticationCode($authenticationCode);
    }

    /**
     * @return string
     */
    public function getDocRequestKey()
    {
        return $this->docRequestKey;
    }

    /**
     * @param string $docRequestKey
     */
    public function setDocRequestKey($docRequestKey)
    {
        $this->docRequestKey = $docRequestKey;
    }

    /**
     * @return string
     */
    public function getIncorporationDate()
    {
        return $this->incorporationDate;
    }

    /**
     * @param string $incorporationDate
     */
    public function setIncorporationDate($incorporationDate)
    {
        $this->incorporationDate = $incorporationDate;
    }

    /**
     * @return string
     */
    public function getAuthenticationCode()
    {
        return $this->authenticationCode;
    }

    /**
     * @param string $authenticationCode
     */
    public function setAuthenticationCode($authenticationCode)
    {
        $this->authenticationCode = $authenticationCode;
    }
}
