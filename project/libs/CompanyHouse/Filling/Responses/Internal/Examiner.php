<?php

namespace CompanyHouse\Filling\Responses\Internal;

class Examiner
{
    /**
     * @var string
     */
    private $telephone;

    /**
     * @var string
     */
    private $comment;

    public function __construct($telephone)
    {
        $this->setTelephone($telephone);
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
}

