<?php

namespace CompanyHouse\Filling\Responses;

use CompanyHouse\Filling\Responses\Internal\IncorporationDetails;
use CompanyHouse\Filling\Responses\Internal\ChangeOfNameDetails;
use CompanyHouse\Filling\Responses\Internal\Examiner;
use CompanyHouse\Filling\Responses\Internal\Rejection;
use Utils\XmlElement;

class ResponseFactory
{
    /**
     * @param XmlElement $xml
     * @return SubmissionStatus
     */
    public function createSubmissionStatus(XmlElement $xml)
    {
        $submissionStatus = new SubmissionStatus($xml->getValue('SubmissionNumber'), $xml->getValue('StatusCode'));
        $submissionStatus->setCompanyNumber($xml->getValue('CompanyNumber'));
        if (!empty($xml->IncorporationDetails)) {
            $detailsXml = $xml->IncorporationDetails;
            $incorporationDetails = new IncorporationDetails(
                $detailsXml->getValue('DocRequestKey'),
                $detailsXml->getValue('IncorporationDate'),
                $detailsXml->getValue('AuthenticationCode')
            );
            $submissionStatus->setIncorporationDetails($incorporationDetails);
        }
        if (!empty($xml->ChangeOfNameDetails)) {
            $detailsXml = $xml->ChangeOfNameDetails;
            $changeOfNameDetails = new ChangeOfNameDetails(
                $detailsXml->getValue('DocRequestKey')
            );
            $submissionStatus->setChangeOfNameDetails($changeOfNameDetails);
        }
        if (!empty($xml->Examiner)) {
            $detailsXml = $xml->Examiner;
            $examiner = new Examiner(
                $detailsXml->getValue('Telephone')
            );
            $examiner->setComment($detailsXml->getValue('Comment'));
            $submissionStatus->setExaminer($examiner);
        }
        if (!empty($xml->Rejections)) {
            $detailsXml = $xml->Rejections;
            foreach ($detailsXml->Reject as $xmlRejection) {
                $rejection = new Rejection(
                    $xmlRejection->getValue('RejectCode'),
                    $xmlRejection->getValue('Description')
                );
                $rejection->setInstanceNumber($xmlRejection->getValue('InstanceNumber'));
                $submissionStatus->addRejection($rejection);
            }
            $submissionStatus->setRejectReference($detailsXml->getValue('RejectReference'));
        }
        return $submissionStatus;
    }
}
