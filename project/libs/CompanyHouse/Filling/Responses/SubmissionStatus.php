<?php

namespace CompanyHouse\Filling\Responses;

use CompanyHouse\Filling\Responses\Internal\IncorporationDetails;
use CompanyHouse\Filling\Responses\Internal\ChangeOfNameDetails;
use CompanyHouse\Filling\Responses\Internal\Examiner;
use CompanyHouse\Filling\Responses\Internal\Rejection;

class SubmissionStatus
{
    const STATUS_ACCEPT = 'ACCEPT';
    const STATUS_REJECT = 'REJECT';
    const STATUS_PENDING = 'PENDING';
    const STATUS_INTERNAL_FAILURE = 'INTERNAL_FAILURE';
    const STATUS_PARKED = 'PARKED';

    /**
     * @var string
     */
    private $submissionNumber;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $companyNumber;

    /**
     * @var Rejection[]
     */
    private $rejections = array();

    /**
     * @var string
     */
    private $rejectReference;

    /**
     * @var IncorporationDetails 
     */
    private $incorporationDetails;

    /**
     * @var ChangeOfNameDetails 
     */
    private $changeOfNameDetails;

    /**
     * @var Examiner 
     */
    private $examiner;

    public function __construct($submissionNumber, $status)
    {
        $this->setSubmissionNumber($submissionNumber);
        $this->setStatus($status);
    }

    /**
     * @return string
     */
    public function getSubmissionNumber()
    {
        return $this->submissionNumber;
    }

    /**
     * @param string $submissionNumber
     */
    public function setSubmissionNumber($submissionNumber)
    {
        $this->submissionNumber = $submissionNumber;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCompanyNumber()
    {
        return $this->companyNumber;
    }

    /**
     * @param string $companyNumber
     */
    public function setCompanyNumber($companyNumber)
    {
        $this->companyNumber = $companyNumber;
    }

    /**
     * @return Rejection
     */
    public function getRejections()
    {
        return $this->rejections;
    }

    /**
     * @param Rejection $rejection
     */
    public function addRejection(Rejection $rejection)
    {
        $this->rejections[] = $rejection;
    }


    /**
     * @return string
     */
    public function getRejectReference()
    {
        return $this->rejectReference;
    }

    /**
     * @param string $rejectReference
     */
    public function setRejectReference($rejectReference)
    {
        $this->rejectReference = $rejectReference;
    }

    /**
     * @return IncorporationDetails
     */
    public function getIncorporationDetails()
    {
        return $this->incorporationDetails;
    }

    /**
     * @param IncorporationDetails $incorporationDetails
     */
    public function setIncorporationDetails(IncorporationDetails $incorporationDetails)
    {
        $this->incorporationDetails = $incorporationDetails;
    }

    /**
     * @return ChangeOfNameDetails
     */
    public function getChangeOfNameDetails()
    {
        return $this->changeOfNameDetails;
    }

    /**
     * @param ChangeOfNameDetails $changeOfNameDetails
     */
    public function setChangeOfNameDetails(ChangeOfNameDetails $changeOfNameDetails)
    {
        $this->changeOfNameDetails = $changeOfNameDetails;
    }

    /**
     * @return Examiner
     */
    public function getExaminer()
    {
        return $this->examiner;
    }

    /**
     * @param Examiner $examiner
     */
    public function setExaminer(Examiner $examiner)
    {
        $this->examiner = $examiner;
    }

    /**
     * @return array
     */
    public function getRejectionDescriptions()
    {
        $descriptions = array();
        foreach ($this->getRejections() as $rejection) {
            $descriptions[] = $rejection->getDescription();
        }
        return $descriptions;
    }

}
