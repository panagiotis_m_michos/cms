<?php

/**
 * CMS
 *
 * @license    GPL
 * @category   Project
 * @package    String
 * @author     Tomas Jakstas tomasj@madesimplegroup.com
 * @version    2011-10-13
 */

class RandString {

	/**
	 * generate random string
	 * @param string length 
	 * @param charset string
	 * @return string
	 */
	public static function generate($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
		$str = '';
		$count = strlen($charset);
		while ($length--) {
			$str .= $charset[mt_rand(0, $count-1)];
		}
		return $str;
	}
}