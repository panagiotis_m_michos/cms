<?php

namespace PackageModule\Controllers;

use HomeControler;
use PackageModule\Forms\FivePoundPackageQuestionnaireData;
use PackageModule\Forms\FivePoundPackageQuestionnaireForm;
use Services\ControllerHelper;
use Services\SessionService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Validator\Validator;
use TemplateModule\Renderers\IRenderer;

class FivePoundsFormationPackageController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var SessionService
     */
    private $sessionService;

    /**
     * @param IRenderer $renderer
     * @param ControllerHelper $controllerHelper
     * @param SessionService $sessionService
     */
    public function __construct(
        IRenderer $renderer,
        ControllerHelper $controllerHelper,
        SessionService $sessionService
    )
    {
        $this->renderer = $renderer;
        $this->controllerHelper = $controllerHelper;
        $this->sessionService = $sessionService;
    }

    /**
     * @return RedirectResponse
     */
    public function details()
    {
        if ($this->currentCustomerHasCompanies()) {
            return new RedirectResponse($this->controllerHelper->getLink(HomeControler::HOME_PAGE));
        }

        return $this->renderer->render();
    }

    /**
     * @return RedirectResponse
     */
    public function questionnaire()
    {
        if ($this->currentCustomerHasCompanies()) {
            return new RedirectResponse($this->controllerHelper->getLink(HomeControler::HOME_PAGE));
        }

        $questionnaire = $this->controllerHelper->buildForm(
            new FivePoundPackageQuestionnaireForm,
            new FivePoundPackageQuestionnaireData
        );

        $showEligible = FALSE;
        $showNotEligible = FALSE;
        if ($questionnaire->isSubmitted()) {
            if ($questionnaire->isValid() && $questionnaire->getData()->isEligible()) {
                $showEligible = TRUE;
            } else {
                $showNotEligible = TRUE;
            }
        }

        $variables = [
            'questionnaire' => $questionnaire->createView(),
            'showEligible' => $showEligible,
            'showNotEligible' => $showNotEligible,
        ];

        return $this->renderer->render($variables);
    }

    /**
     * @return bool
     */
    private function currentCustomerHasCompanies()
    {
        $customer = $this->sessionService->getLoggedInCustomerOrNull();

        return $customer && $customer->hasCompanies();
    }
}
