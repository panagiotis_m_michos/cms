<?php

namespace PackageModule\Controllers;

use FeefoModule\IFeefo;
use PackageModule\Emailers\CallbackEmailer;
use PackageModule\Forms\CallbackData;
use PackageModule\Forms\CallbackForm;
use SearchControler;
use Services\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Validator;
use TemplateModule\Renderers\IRenderer;

class MatrixController
{
    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @var IFeefo
     */
    private $feefo;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var CallbackEmailer
     */
    private $callbackEmailer;

    /**
     * @param IFeefo $feefo
     * @param IRenderer $renderer
     * @param Router $router
     * @param ControllerHelper $controllerHelper
     * @param CallbackEmailer $callbackEmailer
     */
    public function __construct(
        IFeefo $feefo,
        IRenderer $renderer,
        Router $router,
        ControllerHelper $controllerHelper,
        CallbackEmailer $callbackEmailer
    )
    {
        $this->renderer = $renderer;
        $this->feefo = $feefo;
        $this->router = $router;
        $this->controllerHelper = $controllerHelper;
        $this->callbackEmailer = $callbackEmailer;
    }

    /**
     * @return Response
     */
    public function matrix($namesearch = FALSE)
    {
        $searchedCompanyName = SearchControler::getCompanyName();

        $callbackForm = $this->controllerHelper->buildForm(new CallbackForm(), new CallbackData());
        if ($callbackForm->isSubmitted() && $callbackForm->isValid()) {
            /** @var CallbackData $callbackData */
            $callbackData = $callbackForm->getData();
            $callbackData->setSubject('Non-resident Package');
            $this->callbackEmailer->send($callbackData);

            return new RedirectResponse("?namesearch=$namesearch&sent=1#callback-form-container");
        }

        if ($searchedCompanyName === FALSE && $namesearch == 'yes') {
            return new RedirectResponse($this->router->generate('package_module.matrix', ['namesearch' => 'no']));
        } elseif ($searchedCompanyName !== FALSE && !$namesearch) {
            return new RedirectResponse($this->router->generate('package_module.matrix', ['namesearch' => 'yes']));
        }

        $data = [
            'feefoAverage' => $this->feefo->getSummary()->getAverageRating(),
            'feefoReviewCount' => $this->feefo->getSummary()->getReviewCount(),
            'namesearch' => (bool) $searchedCompanyName,
            'callbackForm' => $callbackForm->createView(),
            'callbackMailSent' => isset($_GET['sent']),
        ];

        if ($searchedCompanyName && $namesearch != 'unavailable') {
            $data['introductionStrip']['title'] = "<b>Good News</b>, <span class='lead__company-name'>{$searchedCompanyName}</span> is available to register!";
        }

        return $this->renderer->render($data);
    }
}
