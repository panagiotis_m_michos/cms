<?php

namespace PackageModule\Forms;

use DateTime;
use Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use ValueObject\Country;

class CallbackForm extends AbstractType
{
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'international_package_callback_form';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $countries = array_map(
            function($countryId) {
                return Country::fromId($countryId);
            },
            array_keys(Country::$countriesById)
        );

        $list = new ObjectChoiceList($countries, 'name', [], NULL, 'id');

        $builder
            ->add('asdf', 'hidden', ['data' => base64_encode(time()), 'mapped' => FALSE])
            ->add('firstName', 'text', ['label' => 'First Name'])
            ->add('lastName', 'text', ['label' => 'Last Name'])
            ->add('phoneNumber', 'text', ['label' => 'Telephone Number'])
            ->add('country', 'choice', ['label' => 'Country', 'choice_list' => $list])
            ->add('message', 'textarea', ['label' => 'Message'])
            ->add('save', 'submit', ['label' => 'Call me back']);

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvent $event) {
                $form = $event->getForm();
                $renderTimeFieldValue = $form->get('asdf')->getData();
                $renderTime = NULL;

                try {
                    $renderTime = DateTime::createFromFormat('U', base64_decode($renderTimeFieldValue));
                } catch (Exception $e) {
                    $renderTime = NULL;
                }

                if (empty($renderTimeFieldValue) || empty($renderTime) || $renderTime > new Datetime('-7 seconds') || $renderTime < new DateTime('-24 hours')) {
                    $form->addError(new FormError("Wait a few seconds before submitting the form. This is an anti-spam measure."));
                }
            }
        );
    }
}
