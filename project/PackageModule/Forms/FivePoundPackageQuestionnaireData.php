<?php

namespace PackageModule\Forms;

use Symfony\Component\Validator\Constraints as Assert;

class FivePoundPackageQuestionnaireData
{
    /**
     * @var bool
     * @Assert\NotNull()
     */
    private $firstFormation;

    /**
     * @var bool
     * @Assert\NotNull()
     */
    private $ukResident;

    /**
     * @var bool
     * @Assert\NotNull()
     */
    private $formedOnBehalf;

    /**
     * @return boolean
     */
    public function isFirstFormation()
    {
        return $this->firstFormation;
    }

    /**
     * @param boolean $firstFormation
     */
    public function setIsFirstFormation($firstFormation)
    {
        $this->firstFormation = $firstFormation;
    }

    /**
     * @return boolean
     */
    public function isUkResident()
    {
        return $this->ukResident;
    }

    /**
     * @param boolean $ukResident
     */
    public function setIsUkResident($ukResident)
    {
        $this->ukResident = $ukResident;
    }

    /**
     * @return boolean
     */
    public function isFormedOnBehalf()
    {
        return $this->formedOnBehalf;
    }

    /**
     * @param boolean $formedOnBehalf
     */
    public function setIsFormedOnBehalf($formedOnBehalf)
    {
        $this->formedOnBehalf = $formedOnBehalf;
    }

    /**
     * @return bool
     */
    public function isEligible()
    {
        return $this->firstFormation
            && !$this->formedOnBehalf
            && $this->ukResident;
    }
}
