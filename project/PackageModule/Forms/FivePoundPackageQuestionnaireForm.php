<?php

namespace PackageModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Symfony\Component\Form\FormBuilderInterface;

class FivePoundPackageQuestionnaireForm extends AbstractType
{
    /**
     * @return string The name of this type
     */
    public function getName()
    {
        return 'five_pound_package_questionnaire';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST');

        $builder->add(
            'isFirstFormation',
            'choice',
            [
                'expanded' => TRUE,
                'multiple' => FALSE,
                'label' => 'Is this your first time forming a company?',
                'choice_list' => new ChoiceList(
                    [TRUE, FALSE],
                    ['Yes', 'No']
                ),
            ]
        );

        $builder->add(
            'isFirstFormation',
            'choice',
            [
                'expanded' => TRUE,
                'multiple' => FALSE,
                'label' => 'Is this your first time forming a company?',
                'choice_list' => new ChoiceList(
                    [TRUE, FALSE],
                    ['Yes', 'No']
                ),
            ]
        );

        $builder->add(
            'isUkResident',
            'choice',
            [
                'expanded' => TRUE,
                'multiple' => FALSE,
                'label' => 'Are you a UK resident wanting to set up a UK bank account for this company?',
                'choice_list' => new ChoiceList(
                    [TRUE, FALSE],
                    ['Yes', 'No']
                ),
            ]
        );

        $builder->add(
            'isFormedOnBehalf',
            'choice',
            [
                'expanded' => TRUE,
                'multiple' => FALSE,
                'label' => 'Are you forming this company for yourself or on behalf of someone else?',
                'choice_list' => new ChoiceList(
                    [TRUE, FALSE],
                    ['For somebody else', 'For myself']
                ),
            ]
        );

        $builder->add(
            'submit',
            'submit',
            ['label' => 'Check if I qualify']
        );
    }


}
