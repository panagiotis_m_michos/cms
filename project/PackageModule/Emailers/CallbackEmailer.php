<?php

namespace PackageModule\Emailers;

use EmailService;
use FEmailFactory;
use PackageModule\Forms\CallbackData;

class CallbackEmailer
{
    const EMAIL_CALLBACK = 1656;

    /**
     * @var EmailService
     */
    private $emailService;

    /**
     * @var FEmailFactory
     */
    private $emailFactory;

    public function __construct(EmailService $emailService, FEmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
    }

    public function send(CallbackData $callbackData)
    {
        $email = $this->emailFactory->getById(self::EMAIL_CALLBACK);
        $email->replaceAllPlaceHolders(
            [
                '[COUNTRY]' => $callbackData->getCountry()->getName(),
                '[NAME]' => $callbackData->getFirstName() . ' ' . $callbackData->getLastName(),
                '[PHONE]' => $callbackData->getPhoneNumber(),
                '[MESSAGE]' => $callbackData->getMessage(),
                '[SUBJECT]' => $callbackData->getSubject(),
            ]
        );

        $this->emailService->send($email);
    }
}
