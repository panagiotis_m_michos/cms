{assign "hide" 1}
{include file="@header.tpl"}

<div id="maincontent1" style="width: 955px;">
    <div id="newprod">
        <div class="col100 padding40">
            <div class="col80">
                <h1 class="mbottom20">
                    £5 Package<br>
                    Ltd Company Formation
                </h1>
                <h5>First time company owner? This is the simplest way to get your business started.</h5>
            </div>
            <div class="col20">
                <a href="http://www.feefo.com/feefo/viewvendor.jsp?logon=www.madesimplegroup.com/companiesmadesimple" onclick="window.open(this.href, 'Feefo', 'width=1000,height=600,scrollbars,resizable');
                                    return false;">
                    <img alt="Feefo logo" border="0" src="http://www.feefo.com/feefo/feefologo.jsp?logon=www.madesimplegroup.com/companiesmadesimple&amp;template=service-percentage-white-150x150_en.png" title="See what our customers say about us">
                </a>
            </div>
            <div>
                <div><a class="btn-orange mtop20" href="{url route="package_module_five_pounds_formation_questionnaire"}">BUY NOW</a></div>
                <div class="mleft20 mtop20"><span class="blueprice">£{'[price 1668]'|cms}</span></div>
            </div>
        </div>
        <div class="mtop20 bggrey1 padding40">
            <h4>What's included:</h4>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/3-hour-online.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>3 Hour Online Formation</h5>
                    <p>Submit your company details and you could be trading in the next 3 hours (subject to Companies House workload and operating hours).</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/bank-account.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>Company Bank Account</h5>
                    <p>This package requires a business bank account referral to Barclays or TSB to get you started. No cash back is available with this package.</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/no-hidden-extras.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>No Hidden Extras or Surprise Costs</h5>
                    <p>No surprises here, even the Companies House £15 registration fee is already included in the price.</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/all-your-official.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>All Your Official Company Set Up Documents</h5>
                    <p>You’ll have digital versions of all the important stuff; Certificate of Incorporation, Memorandum &amp; Articles of Association, Share Certificates and your Companies House WebFiling Authentication Code.</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/free-lifetime-company.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>Free Lifetime Company Support</h5>
                    <p>Got a question about your company? Unlike other company formation services we’re here to help before, during and after your company is set up. We’re with you for the whole journey.</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/no-complicated-paperwork.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>No Complicated Paperwork to Complete</h5>
                    <p>Simple online process to form your company. Step by step process that guides you through the required company, director and shareholder information.</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/online-admin-portal.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>Online Admin Portal to Manage Your Companies</h5>
                    <p>Avoid late filing penalties with email reminders of key dates. Access your company’s documents anytime. Easy management of company, directors and shareholder information.</p>
                </div>
            </div>
        </div>
        <div class="mtop20 bgblue1 padding40">
            <div class="col100 txtcenter mbottom20">
                <h4 class="mnone"><strong>Need a little extra?</strong></h4>
                <h5>Upgrade to the Printed Package to include:</h5>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/printed-certificate.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>Printed Certificate of Incorporation</h5>
                    <p>A printed copy is often required by banks when opening a business bank account. The certificate is printed on Companies House approved paper, equivalent of a birth certificate for your company.</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/printed-share.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>Printed Share Certificates</h5>
                    <p>We'll immediately send you a hard copy of this legal document which certifies ownership within a limited company.</p>
                </div>
            </div>
            <div class="col100">
                <div class="col10"><img src="front/imgs/products/maintenance-of-stat-books.png" height="75" width="75" alt=""></div>
                <div class="col90">
                    <h5>Maintenance of Statutory Books</h5>
                    <p>This is a digital record of your company details. Let us maintain your Statutory Books and save on accountancy fees.</p>
                </div>
            </div>
            <h5 class="txtcenter">Only £19.99 more</h5>
            <span class="txtcenter displayblock"><a class="btn-blue" href="/page1314en.html">VIEW PRINTED PACKAGE</a></span></div>
        <div class="mtop20 bggrey1 padding40"><img src="front/imgs/products/toolkit.png" class="toolkit" alt="">
            <h2>Free Start Up Toolkit</h2>
            <h4>Optional extras to get your business off to a flying start, available with all of our packages.</h4>
            <div class="col100">
                <div class="col10 txtcenter"><img src="front/imgs/products/tick.png" height="40" width="40" alt=""></div>
                <div class="col90">
                    <h5>Free .co.uk Website</h5>
                    <p>Free 1 year domain name includes free email address, 1-page website and professional stock images.</p>
                </div>
                <div class="col10 txtcenter"><img src="front/imgs/products/tick.png" height="40" width="40" alt=""></div>
                <div class="col90">
                    <h5>£75 Google AdWords Voucher</h5>
                    <p>Find new customers online with Google. Create simple ads which appear when people search online, get found and grow your business instantly.</p>
                </div>
                <div class="col10 txtcenter"><img src="front/imgs/products/tick.png" height="40" width="40" alt=""></div>
                <div class="col90">
                    <h5>Free Accountancy &amp; Tax Consultation</h5>
                    <p>We’ve partnered with TaxAssist Accountants to provide you with local accountancy expertise and advice. If you’re forming this company for a client, don’t worry, details are only passed on by request. This offer is for UK customers only.&nbsp;</p>
                </div>
                <div class="col10 txtcenter"><img src="front/imgs/products/tick.png" height="40" width="40" alt=""></div>
                <div class="col90">
                    <h5>FREE How To Make A Profit eBook</h5>
                    <p>Everything you need to know to help your business make a profit.</p>
                </div>
                <div class="col10 txtcenter"><img src="front/imgs/products/tick.png" height="40" width="40" alt=""></div>
                <div class="col90">
                    <h5>2 Month Free Trial to FreeAgent</h5>
                    <p>Award winning online accounting tool for small business. FreeAgent is easy to use and you won't need any previous accounts experience.</p>
                </div>
            </div>
        </div>
        <div class="mtop20 padding40">
            <h4>£5 Package Package FAQs</h4>
            <h5>What type of company am I buying?</h5>
            <p>A fully trading UK Private Limited Company, limited by shares.</p>
            <h5>I'm not sure what I need right now, can I buy the £5 Package Package and add other services to it later?</h5>
            <p>Absolutely, however the services will not be available at the discounted package rates.</p>
            <h5>Can I print the Certificate of Incorporation myself?</h5>
            <p>Of course, you will have access to digital versions of both the incorporation and share certificates in your online admin portal. However, please note that if you wish to use the certificate in an official capacity then it needs to be printed on Companies House approved paper, as all our original versions are.</p>
            <h5>What if I only need the company name and nothing else?</h5>
            <p>If you're looking to start trading immediately or very soon, then our £5 Package Package is the cheapest way to get up and running quickly and easily.</p>
            <h5>How do I redeem the offers in the free start up toolkit?</h5>
            <p>After your company is incorporated, we'll email you more information about each of the toolkit extras including information on how to redeem each one. The offers are provided by our trusted partners and have been carefully selected to help your new company, you won't be contacted by any third parties without your consent.</p>
        </div>
    </div>

</div>

{include file="@footer.tpl"}
