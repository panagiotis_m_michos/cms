{assign "hide" 1}
{include file="@header.tpl"}

<div id="maincontent1" style="width: 955px;">
    <div id="newprod">
        <div id="five-pound-formation-questionnaire">
            <h1>First time company owner?</h1>
            <p>This offer is restricted to first time UK company owners. Please answer the 3 questions below to check if you qualify.</p>

            {if $showEligible}
                <div class="alert alert-success" role="alert">
                    <h3>Congratulations, you qualify! What’s next?</h3>
                    <span>
                        <a href="{url route="SearchControler::COMPANY_NAME" package_id=1668}">Click here to check your company name is available</a> and make payment.
                    </span>
                </div>
            {/if}

            {if $showNotEligible}
                <div class="alert alert-danger" role="alert">
                    <h3>Sorry, you don’t qualify for this offer.</h3>
                    <span>
                        {product id=1313 var="fromProduct"}
                        But you can still take advantage of our <a href="{url route="SearchControler::SEARCH_PAGE"}">great value packages</a> from {$fromProduct->getPrice()|currency}
                    </span>
                </div>
            {/if}

            {$formHelper->setTheme($questionnaire, 'cms.html.twig')}
            {$formHelper->form($questionnaire) nofilter}
        </div>
    </div>
</div>

{include file="@footer.tpl"}
