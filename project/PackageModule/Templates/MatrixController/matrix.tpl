{extends '@structure/layout.tpl'}

{block content}
    <div id="retail-matrix-progress" class="width100 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progress-indicator">
                        <li class="completed"><span>Search</span><span class="bubble"></span></li>
                        <li class="active"><span>Select</span><span class="bubble"></span></li>
                        <li><span>Buy</span><span class="bubble"></span></li>
                        <li><span>Complete</span><span class="bubble"></span></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    {include '@blocks/flashMessageBt.tpl'}

                    <h1 class="font-300 top0 btm30">{$introductionStrip.title nofilter}</h1>
                </div>
            </div>
            <div class="row matrix-tabs">
                <div class="col-xs-12" style="border-bottom: 1px solid #ddd;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#limited" onclick="_gaq.push(['_trackEvent', 'Matrix Tabs', 'Click', 'Limited Company']);" aria-controls="limited" role="tab" data-toggle="tab">Limited Company</a></li>
                        <li role="presentation"><a href="#contractors" onclick="_gaq.push(['_trackEvent', 'Matrix Tabs', 'Click', 'Contractors']);" aria-controls="contractors" role="tab" data-toggle="tab">Contractors</a></li>
                        <li role="presentation"><a href="#nonresidents" onclick="_gaq.push(['_trackEvent', 'Matrix Tabs', 'Click', 'Non-Residents']);" aria-controls="nonresidents" role="tab" data-toggle="tab">Non-Residents</a></li>
                        <li role="presentation"><a href="#guarantee" onclick="_gaq.push(['_trackEvent', 'Matrix Tabs', 'Click', 'Guarantee']);" aria-controls="guarantee" role="tab" data-toggle="tab">Guarantee</a></li>
                        <li role="presentation"><a href="#llp" onclick="_gaq.push(['_trackEvent', 'Matrix Tabs', 'Click', 'LLP']);" aria-controls="llp" role="tab" data-toggle="tab">LLP</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="limited">
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    <div class="row">
                        <div class="hidden-xs hidden-sm col-md-4">
                            <div class="matrix-lead-description">{$matrix.description nofilter}</div>
                        </div>
                        <div class="col-md-8 pad0">
                            {foreach $matrix.packages as $package}
                                <div class="col-xs-12 col-md-2 matrix-price-box">
                                    {if isset($package.recommended) && $package.recommended == true}
                                        <h6 class="package-featured">Most popular</h6>
                                    {/if}
                                    <h4 class="retail-matrix">{$package.title nofilter}</h4>
                                    <div class="main-txt button-wide-mobile
                                        {if isset($package.recommended) && $package.recommended == true}
                                            package-featured
                                        {/if}
                                    ">
                                        <h2 class="mtop0">{$package.price nofilter}<br><span>+VAT</span></h2>
                                        <div class="text-center">
                                            {if $namesearch}
                                                {$package.callToAction nofilter}
                                            {else}
                                                {$package.callToActionWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                        <div class="text-center hidden-xs hidden-sm moreinfo">
                                            {if $namesearch}
                                                {$package.packageLink nofilter}
                                            {else}
                                                {$package.packageLinkWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                        <div class="top10 text-center hidden-md hidden-lg">
                                            <a href="#collapse-{$package.title}" role="button" data-toggle="collapse" class="btn btn-white">{$package.secondaryTitle nofilter}</a>
                                        </div>
                                        <div class="collapse hidden-md hidden-lg" id="collapse-{$package.title}">
                                            <div class="well mobile-included-description">
                                                {foreach $package.features as $featureName => $featureValue}
                                                    {if $matrix.features.$featureName}
                                                        <p class="small">{$matrix.features.$featureName.description nofilter}</p>
                                                        <a data-toggle="tooltip" html="true" title="{$matrix.features.$featureName.tooltip nofilter}">
                                                            <i class="fa fa-info-circle"></i>
                                                        </a>
                                                    {/if}
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>

                    {foreach $matrix.features as $featureName => $feature}
                        <div class="row hidden-xs hidden-sm">
                            <div class="col-md-4 matrix-description-box">
                                <div>
                                    <p>{$feature.description nofilter}</p>
                                    <a data-toggle="tooltip" data-placement="right" html="true" title="{$feature.tooltip}">
                                        <i class="fa fa-info-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-8 pad0">
                                {foreach $matrix.packages as $package}
                                    <div class="col-md-2 matrix-check-box">
                                        {if !isset($package.features.$featureName) || $package.features.$featureName === false}
                                            <div class="matrix-check-box-bg-minus">
                                                <i class="fa fa-minus fa-fw fa-lg" aria-hidden="true"></i>
                                            </div>
                                        {elseif $package.features.$featureName === true}
                                            <div class="matrix-check-box-bg-check">
                                                <i class="fa fa-check fa-fw fa-lg" aria-hidden="true"></i>
                                            </div>
                                        {else}
                                            <div class="matrix-check-box-bg-check cashback">
                                                {$package.features.$featureName nofilter}
                                            </div>
                                        {/if}
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    {/foreach}

                    <div class="row hidden-xs hidden-sm ">
                        <div class="col-md-4 btm20"></div>
                        <div class="col-md-8 pad0">
                            {foreach $matrix.packages as $package}
                                <div class="col-xs-12 col-md-2 matrix-price-box">
                                    <div class="main-txt button-wide-mobile
                                        {if isset($package.recommended) && $package.recommended == true}
                                            package-featured
                                        {/if}
                                    ">
                                        <h2 class="mtop0">{$package.price nofilter}<br><span>+VAT</span></h2>
                                        <div class="text-center">
                                            {if $namesearch}
                                                {$package.callToAction nofilter}
                                            {else}
                                                {$package.callToActionWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                        <div class="text-center hidden-xs hidden-sm moreinfo">
                                            {if $namesearch}
                                                {$package.packageLink nofilter}
                                            {else}
                                                {$package.packageLinkWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                    </div>
                                    <h4 class="retail-matrix">{$package.title nofilter}</h4>

                                    {if isset($package.recommended) && $package.recommended == true}
                                        <h6 class="package-featured bottom">Most popular</h6>
                                    {/if}
                                </div>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="contractors">
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="top0">{$contractorsTab.title nofilter}</h2>
                            <p class="lead">{$contractorsTab.lead nofilter}</p>
                            <p>{$contractorsTab.intro nofilter}</p>
                            {if isset($contractorsTab.includes)}
                                <ul class="fa-ul">
                                    {foreach $contractorsTab.includes as $include}
                                        <li><i class="fa-li fa fa-check green"></i>{$include.description nofilter}</li>
                                    {/foreach}
                                </ul>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            {if isset($contractorsTab.blocks)}
                <div class="width100 bg-white">
                    <div class="container">
                        {foreach $contractorsTab.blocks as $block}
                            <div class="row companyservices-matrix-block">
                                <div class="col-xs-12 col-md-10">
                                    <div class="row">
                                        <h2 class="top0 btm10">{$block.title nofilter}</h2>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2 text-center">
                                    <div class="row">
                                        <p class="lead btm10"><strong>{$block.price nofilter}</strong></p>
                                        {if $namesearch}
                                            {$block.callToAction nofilter}
                                        {else}
                                            {$block.callToActionWithoutSearch nofilter}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            {/if}
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    {if isset($contractorsTab)}
                        <div class="row">
                            <div class="col-xs-12">
                                {foreach $contractorsTab.faqs as $faq}
                                    <h4>{$faq.title nofilter}</h4>
                                    {$faq.description nofilter}
                                {/foreach}
                            </div>
                        </div>
                    {/if}
                    {if isset($proofOfId)}
                        <div class="row">
                            <div class="col-xs-12">
                                {ui name='proof_of_id' data=$proofOfId}
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="nonresidents">
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    <div class="row">
                        <div class="hidden-xs hidden-sm col-md-6">
                            <div class="matrix-lead-description">{$matrixIntl.description nofilter}</div>
                        </div>
                        <div class="col-md-6 pad0">
                            {foreach $matrixIntl.packages as $index => $package}
                                <div class="col-xs-12 col-md-6 matrix-price-box">
                                    {if isset($package.recommended) && $package.recommended == true}
                                        <h6 class="package-featured">Most popular</h6>
                                    {/if}
                                    <h4 class="retail-matrix">{$package.title nofilter}</h4>
                                    <div class="main-txt button-wide-mobile
                                        {if isset($package.recommended) && $package.recommended == true}
                                            package-featured
                                        {/if}
                                    ">
                                        <h2 class="mtop0">{$package.price nofilter}<br><span>+VAT</span></h2>
                                        <div class="text-center">
                                            {if $namesearch}
                                                {$package.callToAction nofilter}
                                            {else}
                                                {$package.callToActionWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                        <div class="text-center hidden-xs hidden-sm moreinfo">
                                            {if $namesearch}
                                                {$package.packageLink nofilter}
                                            {else}
                                                {$package.packageLinkWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                        <div class="top10 text-center hidden-md hidden-lg">
                                            <a href="#collapse-{$index}" role="button" data-toggle="collapse" class="btn btn-white">{$package.secondaryTitle nofilter}</a>
                                        </div>
                                        <div class="collapse hidden-md hidden-lg" id="collapse-{$index}">
                                            <div class="well mobile-included-description">
                                                {foreach $package.features as $featureName => $featureValue}
                                                    {if $matrixIntl.features.$featureName}
                                                        <p class="small">{$matrixIntl.features.$featureName.description nofilter}</p>
                                                        <a data-toggle="tooltip" html="true" title="{$matrixIntl.features.$featureName.tooltip nofilter}">
                                                            <i class="fa fa-info-circle"></i>
                                                        </a>
                                                    {/if}
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>

                    {foreach $matrixIntl.features as $featureName => $feature}
                        <div class="row hidden-xs hidden-sm">
                            <div class="col-md-6 matrix-description-box">
                                <div>
                                    <p>{$feature.description}</p>
                                    <a data-toggle="tooltip" data-placement="right" html="true" title="{$feature.tooltip}">
                                        <i class="fa fa-info-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 pad0">
                                {foreach $matrixIntl.packages as $package}
                                    <div class="col-md-6 matrix-check-box">
                                        {if !isset($package.features.$featureName) || $package.features.$featureName === false}
                                            <div class="matrix-check-box-bg-minus">
                                                <i class="fa fa-minus fa-fw fa-lg" aria-hidden="true"></i>
                                            </div>
                                        {elseif $package.features.$featureName === true}
                                            <div class="matrix-check-box-bg-check">
                                                <i class="fa fa-check fa-fw fa-lg" aria-hidden="true"></i>
                                            </div>
                                        {else}
                                            <div class="matrix-check-box-bg-check cashback">
                                                {$package.features.$featureName nofilter}
                                            </div>
                                        {/if}
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    {/foreach}

                    <div class="row hidden-xs hidden-sm ">
                        <div class="col-md-6 btm20"></div>
                        <div class="col-md-6 pad0">
                            {foreach $matrixIntl.packages as $package}
                                <div class="col-xs-12 col-md-6 matrix-price-box">
                                    <div class="main-txt button-wide-mobile
                                        {if isset($package.recommended) && $package.recommended == true}
                                            package-featured
                                        {/if}
                                    ">
                                        <h2 class="mtop0">{$package.price nofilter}<br><span>+VAT</span></h2>
                                        <div class="text-center">
                                            {if $namesearch}
                                                {$package.callToAction nofilter}
                                            {else}
                                                {$package.callToActionWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                        <div class="text-center hidden-xs hidden-sm moreinfo">
                                            {if $namesearch}
                                                {$package.packageLink nofilter}
                                            {else}
                                                {$package.packageLinkWithoutSearch nofilter}
                                            {/if}
                                        </div>
                                    </div>
                                    <h4 class="retail-matrix">{$package.title nofilter}</h4>

                                    {if isset($package.recommended) && $package.recommended == true}
                                        <h6 class="package-featured bottom">Most popular</h6>
                                    {/if}
                                </div>
                            {/foreach}
                        </div>
                    </div>

                    <div class="row pad-topdown">
                        {include '@structure/callbackRequest.tpl'}
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="guarantee">
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="top0">{$guaranteeTab.title nofilter}</h2>
                            <p class="lead">{$guaranteeTab.lead nofilter}</p>
                            <p>{$guaranteeTab.intro nofilter}</p>
                            {if isset($guaranteeTab.includes)}
                                <ul class="fa-ul">
                                    {foreach $guaranteeTab.includes as $include}
                                        <li><i class="fa-li fa fa-check green"></i>{$include.description nofilter}</li>
                                    {/foreach}
                                </ul>
                            {/if}
                            {*<p class="small">{$guaranteeTab.disclaimer nofilter}</p>*}
                        </div>
                    </div>
                </div>
            </div>
            {if isset($guaranteeTab.blocks)}
                <div class="width100 bg-white">
                    <div class="container">
                        {foreach $guaranteeTab.blocks as $block}
                            <div class="row companyservices-matrix-block">
                                <div class="col-xs-12 col-md-10">
                                    <div class="row">
                                        <h2 class="top0 btm10">{$block.title nofilter}</h2>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2 text-center">
                                    <div class="row">
                                        <p class="lead btm10"><strong>{$block.price nofilter}</strong></p>
                                        {if $namesearch}
                                            {$block.callToAction nofilter}
                                        {else}
                                            {$block.callToActionWithoutSearch nofilter}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            {/if}
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    {if isset($guaranteeTab)}
                        <div class="row">
                            <div class="col-xs-12">
                                {foreach $guaranteeTab.faqs as $faq}
                                    <h4>{$faq.title nofilter}</h4>
                                    {$faq.description nofilter}
                                {/foreach}
                            </div>
                        </div>
                    {/if}
                    {if isset($proofOfId)}
                        <div class="row">
                            <div class="col-xs-12">
                                {ui name='proof_of_id' data=$proofOfId}
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="llp">
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="top0">{$llpTab.title nofilter}</h2>
                            <p class="lead">{$llpTab.lead nofilter}</p>
                            <p>{$llpTab.intro nofilter}</p>
                            {if isset($llpTab.includes)}
                                <ul class="fa-ul">
                                    {foreach $llpTab.includes as $include}
                                        <li><i class="fa-li fa fa-check green"></i>{$include.description nofilter}</li>
                                    {/foreach}
                                </ul>
                            {/if}
                            {*<p class="small">{$llpTab.disclaimer nofilter}</p>*}
                        </div>
                    </div>
                </div>
            </div>
            {if isset($llpTab.blocks)}
                <div class="width100 bg-white">
                    <div class="container">
                        {foreach $llpTab.blocks as $block}
                            <div class="row companyservices-matrix-block">
                                <div class="col-xs-12 col-md-10">
                                    <div class="row">
                                        <h2 class="top0 btm10">{$block.title nofilter}</h2>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-2 text-center">
                                    <div class="row">
                                        <p class="lead btm10"><strong>{$block.price nofilter}</strong></p>
                                        {if $namesearch}
                                            {$block.callToAction nofilter}
                                        {else}
                                            {$block.callToActionWithoutSearch nofilter}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            {/if}
            <div class="width100 bg-white padcard-mobile">
                <div class="container">
                    {if isset($llpTab)}
                        <div class="row">
                            <div class="col-xs-12">
                                {foreach $llpTab.faqs as $faq}
                                    <h4>{$faq.title nofilter}</h4>
                                    {$faq.description nofilter}
                                {/foreach}
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid home-banner6 padcard-mobile">
        <div class="container">
            <div class="row">
                <h2 class="top0 text-center">{$featuresStrip.title nofilter}</h2>
                {foreach $featuresStrip.features as $feature}
                    <div class="col-xs-12 col-md-4 text-center">
                        <i class="fa fa-4x fa-fw padcard-mobile {$feature.icon nofilter}" aria-hidden="true"></i>
                        <p class="lead">{$feature.title nofilter}</p>
                        <p>{$feature.description nofilter}</p>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
    <div class="width100 plus-matrix"><img src="{$urlImgs}plus-matrix.png" class="center-block" alt=""/></div>
    <div class="width100 bg-white padcard-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0">{$toolkitStrip.title nofilter}</h2>
                    <p class="lead">{$toolkitStrip.description nofilter}</p>
                </div>
            </div>
            {foreach $toolkitStrip.tools as $tool}
                <div class="matrix-toolkit-wrap col-xs-12 col-md-4 text-center btm10">
                    <div class="matrix-toolkit-in padcard-mobile">
                        <i class="fa fa-3x fa-fw top10 padcard-mobile {$tool.icon nofilter}" aria-hidden="true"></i>
                        <p><strong>{$tool.title nofilter}</strong></p>
                        <p class="small">{$tool.overlay nofilter}</p>
                    </div>
                </div>
            {/foreach}
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="lead btm10 top20">{$toolkitStrip.footer nofilter}</p>
                    <p class="margin0">{$toolkitStrip.footerDescription nofilter}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="width100 home-banner6 padcard-mobile">
        <div class="container"> 
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="top0">{$videoStrip.title nofilter}</h2>
                    <p class="lead">{$videoStrip.subtitle nofilter}</p>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{$videoStrip.videoUrl nofilter}" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-xs-12 col-md-8">
                    <p class="lead top10">{$videoStrip.description1 nofilter}</p>
                    <p class="lead margin0">{$videoStrip.description2 nofilter}</p>
                </div>
            </div>
        </div>
    </div>

    {include file="@structure/feefoStrip.tpl"}

    <div class="width100 bg-white padcard-mobile" style="border-bottom: 2px solid #ddd;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="top0 text-center">{$faqsStrip.title nofilter}</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    {if isset($faqsStrip.faqs)}
                        {foreach $faqsStrip.faqs as $faq}
                            <h4 class="display-inline">{$faq.title nofilter}</h4>
                            {$faq.description nofilter}
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
    </div>



    <script>
        (function() {
            if (window.location.hash == '#callback-form-container') {
                $('a[href=#nonresidents]').click();
            }
        })();
    </script>
{/block}

{include file="@footer.tpl"}
