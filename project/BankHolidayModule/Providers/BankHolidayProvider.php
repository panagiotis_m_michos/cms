<?php

namespace BankHolidayModule\Providers;

use Basket;
use Exception;
use Guzzle\Http\ClientInterface;
use Psr\Log\LoggerInterface;

class BankHolidayProvider
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $holidaysUrl;

    /**
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     * @param string $holidaysUrl
     */
    public function __construct(ClientInterface $client, LoggerInterface $logger, $holidaysUrl)
    {
        $this->client = $client;
        $this->logger = $logger;
        $this->holidaysUrl = $holidaysUrl;
    }

    /**
     * @return array|null
     */
    public function getHolidays()
    {
        try {
            $jsonData = $this->client->get($this->holidaysUrl)->send()->json();
        } catch (Exception $e) {
            $this->logger->error('Bank holidays JSON file could not be loaded.', ['exception' => (string) $e]);

            return [];
        }

        if (!isset($jsonData['england-and-wales']['events'])) {
            $this->logger->error('Bank holidays JSON file has invalid structure', ['jsonData' => $jsonData]);

            return [];
        }

        return array_column($jsonData['england-and-wales']['events'], 'date', 'date');
    }
}
