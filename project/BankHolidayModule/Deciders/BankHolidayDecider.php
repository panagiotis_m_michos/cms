<?php

namespace BankHolidayModule\Deciders;

use Basket;
use BankHolidayModule\Providers\BankHolidayProvider;
use DateTime;
use Doctrine\Common\Cache\Cache;

class BankHolidayDecider
{
    /**
     * @var array
     */
    private $holidays = [];

    /**
     * @param BankHolidayProvider $holidaysProvider
     * @param Cache $cache
     * @param int $cacheTime
     */
    public function __construct(BankHolidayProvider $holidaysProvider, Cache $cache, $cacheTime)
    {
        $this->holidays = $cache->contains('holidays') ? $cache->fetch('holidays') : $holidaysProvider->getHolidays();

        if ($this->holidays) {
            $cache->save('holidays', $this->holidays, $cacheTime);
        }
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public function isHoliday(DateTime $date)
    {
        return isset($this->holidays[$date->format('Y-m-d')]) || empty($this->holidays);
    }

    /**
     * @return bool
     */
    public function isHolidayNow()
    {
        return $this->isHoliday(new DateTime);
    }
}
