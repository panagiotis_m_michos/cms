<?php

namespace Validators\Constraints\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class RequiredChoicesConstraintValidator extends ConstraintValidator
{
    /**
     * @var ExecutionContextInterface
     */
    protected $context;

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value instanceof ArrayCollection && $constraint instanceof RequiredChoicesConstraint) {
            if ($value->count() < $constraint->count) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%count%', $constraint->count)
                    ->addViolation();
            }
        }
    }
}