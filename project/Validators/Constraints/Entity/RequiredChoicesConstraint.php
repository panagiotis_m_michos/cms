<?php

namespace Validators\Constraints\Entity;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class RequiredChoicesConstraint extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Please provide at least %count% item(s)';

    /**
     * @var int
     */
    public $count;

    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
        return 'count';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return array('count');
    }
}