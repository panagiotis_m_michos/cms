<?php

namespace ToolkitOfferModule\Services;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Entities\Company;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Entities\ToolkitOffer;
use ToolkitOfferModule\Repositories\CompanyToolkitOfferRepository;
use ToolkitOfferModule\Repositories\ToolkitOfferRepository;

class ToolkitOfferService
{
    /**
     * @var CompanyToolkitOfferRepository
     */
    private $companyToolkitOfferRepository;

    /**
     * @var ToolkitOfferRepository
     */
    private $toolkitOfferRepository;

    /**
     * @param CompanyToolkitOfferRepository $companyToolkitOfferRepository
     * @param ToolkitOfferRepository $toolkitOfferRepository
     */
    public function __construct(
        CompanyToolkitOfferRepository $companyToolkitOfferRepository,
        ToolkitOfferRepository $toolkitOfferRepository
    ) {
        $this->companyToolkitOfferRepository = $companyToolkitOfferRepository;
        $this->toolkitOfferRepository = $toolkitOfferRepository;
    }

    /**
     * @param Company $company
     * @param ToolkitOffer $toolkitOffer
     */
    public function addOfferToCompany(Company $company, ToolkitOffer $toolkitOffer)
    {
        $companyToolkitOffer = new CompanyToolkitOffer($company, $toolkitOffer);
        $company->addToolkitOffer($companyToolkitOffer);

        $this->companyToolkitOfferRepository->persist($companyToolkitOffer);
        $this->companyToolkitOfferRepository->flush($companyToolkitOffer);
    }

    /**
     * @return array
     */
    /**
     * @param Company $company
     * @param ArrayCollection|ToolkitOffer[] $toolkitOffers
     */
    public function assignOffersToCompany(Company $company, ArrayCollection $toolkitOffers)
    {
        $company->clearToolkitOffers();

        $allOffers = $this->getAvailableOffers();

        foreach ($allOffers as $allOffer) {
            $companyToolkitOffer = new CompanyToolkitOffer($company, $allOffer);
            if (!$toolkitOffers->contains($allOffer)) {
                $companyToolkitOffer->setSelected(FALSE);
            }

            $company->addToolkitOffer($companyToolkitOffer);
        }

        $this->companyToolkitOfferRepository->flush();

    }

    /**
     * @return ToolkitOffer[]
     */
    public function getAvailableOffers()
    {
        return $this->toolkitOfferRepository->getAvailableOffers();
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedFreeNamescoDomainOffers()
    {
        return $this->companyToolkitOfferRepository->getUnclaimedFreeNamescoDomainOffers();
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedAdwordsVoucherOffers()
    {
        return $this->companyToolkitOfferRepository->getUnclaimedAdwordsVoucherOffers();
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedGoogleAppsDiscountOffers()
    {
        return $this->companyToolkitOfferRepository->getUnclaimedGoogleAppsDiscountOffers();
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedEbookOffers()
    {
        return $this->companyToolkitOfferRepository->getUnclaimedEbookOffers();
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedFreeAgentOffers()
    {
        return $this->companyToolkitOfferRepository->getUnclaimedFreeAgentTrialOffers();
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedTaxAssistOffers()
    {
        return $this->companyToolkitOfferRepository->getUnclaimedTaxConsultationOffers();
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedFacebookGroupOffers()
    {
        return $this->companyToolkitOfferRepository->getUnclaimedFacebookGroupOffers();
    }

    /**
     * @param CompanyToolkitOffer $offer
     */
    public function claim(CompanyToolkitOffer $offer)
    {
        $offer->setDateClaimed(new DateTime());
        $this->companyToolkitOfferRepository->flush();
    }
}
