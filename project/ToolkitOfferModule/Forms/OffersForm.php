<?php

namespace ToolkitOfferModule\Forms;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OffersForm extends AbstractType
{
    const NAME = 'offersForm';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->add(
            'offers',
            'entity',
            [
                'class' => 'ToolkitOfferModule\Entities\ToolkitOffer',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('to')
                        ->where('CURRENT_DATE() >= to.dateValidFrom')
                        ->andWhere('(CURRENT_DATE() <= to.dateValidTo OR to.dateValidTo IS NULL)');
                },
                'property' => 'name',
                'expanded' => TRUE,
                'multiple' => TRUE,
            ]
        );

        $builder->add(
            'continue',
            'submit',
            [
                'label' => 'Continue',
            ]
        );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'ToolkitOfferModule\Forms\OffersData',
            ]
        );
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::NAME;
    }
}
