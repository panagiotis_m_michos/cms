<?php

namespace ToolkitOfferModule\Forms;

use Doctrine\Common\Collections\ArrayCollection;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Entities\ToolkitOffer;

class OffersData
{

    /**
     * @var ToolkitOffer[]
     */
    private $offers = [];

    /**
     * @param ToolkitOffer[] $offers
     */
    public function __construct($offers = [])
    {
        $this->offers = new ArrayCollection($offers);
    }

    /**
     * @param CompanyToolkitOffer[] $companyOffers
     * @return OffersData
     */
    public static function fromCompanyToolkitOffers($companyOffers = [])
    {
        $offers = [];
        foreach ($companyOffers as $companyOffer) {
            $offers[] = $companyOffer->getToolkitOffer();
        }

        return new self($offers);
    }

    /**
     * @return ArrayCollection|ToolkitOffer[]
     */
    public function getOffers()
    {
        return $this->offers;
    }
}
