<?php

namespace ToolkitOfferModule\Emailers;

use FEmail;

class EmailFactory
{
    const EMAIL_ID_TAX_ASSIST = 1629;
    const EMAIL_ID_FREE_AGENT = 1631;
    const EMAIL_ID_EBOOK = 1632;
    const EMAIL_ID_FACEBOOK_GROUP = 1678;

    /**
     * @return FEmail
     */
    public function getTaxAssistEmail()
    {
        return new FEmail(self::EMAIL_ID_TAX_ASSIST);
    }

    /**
     * @return FEmail
     */
    public function getFreeAgentEmail()
    {
        return new FEmail(self::EMAIL_ID_FREE_AGENT);
    }

    /**
     * @return FEmail
     */
    public function getEbookEmail()
    {
        return new FEmail(self::EMAIL_ID_EBOOK);
    }

    /**
     * @return FEmail
     */
    public function getFacebookGroupEmail()
    {
        return new FEmail(self::EMAIL_ID_FACEBOOK_GROUP);
    }
}
