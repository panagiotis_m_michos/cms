<?php

namespace ToolkitOfferModule\Emailers;

use Entities\Customer;
use FEmail;
use IEmailService;

class ToolkitOfferEmailer
{
    /**
     * @var IEmailService
     */
    private $emailService;

    /**
     * @var EmailFactory
     */
    private $emailFactory;

    /**
     * @param IEmailService $emailService
     * @param EmailFactory $emailFactory
     */
    public function __construct(IEmailService $emailService, EmailFactory $emailFactory)
    {
        $this->emailService = $emailService;
        $this->emailFactory = $emailFactory;
    }

    /**
     * @param Customer $customer
     */
    public function sendTaxAssistEmail(Customer $customer)
    {
        $email = $this->emailFactory->getTaxAssistEmail();
        $this->send($customer, $email);
    }

    /**
     * @param Customer $customer
     */
    public function sendFreeAgentEmail(Customer $customer)
    {
        $email = $this->emailFactory->getFreeAgentEmail();
        $this->send($customer, $email);

    }

    /**
     * @param Customer $customer
     */
    public function sendMakeMoreProfitGuideEmail(Customer $customer)
    {
        $email = $this->emailFactory->getEbookEmail();
        $this->send($customer, $email);
    }

    /**
     * @param Customer $customer
     */
    public function sendFacebookGroupEmail(Customer $customer)
    {
        $email = $this->emailFactory->getFacebookGroupEmail();
        $this->send($customer, $email);
    }

    /**
     * @param Customer $customer
     * @param FEmail $email
     */
    private function send(Customer $customer, FEmail $email)
    {
        $email->setTo($customer->getEmail());
        $email->replacePlaceHolders(['[FIRST_NAME]' => $customer->getFirstName()]);
        $this->emailService->send($email, $customer);
    }
}
