<?php

namespace ToolkitOfferModule\Uploaders;

use Gaufrette\Filesystem;
use Utils\File;

class FtpUploader
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param File $file
     * @param string $filename
     */
    public function upload(File $file, $filename)
    {
        $content = file_get_contents($file->getPathname());
        $this->filesystem->write($filename, $content, TRUE);
    }
}
