<?php

namespace ToolkitOfferModule\Uploaders;

use Google_Http_MediaFileUpload;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_ParentReference;
use Symfony\Component\HttpFoundation\File\File;

class GoogleDriveUploader
{
    /**
     * @var Google_Service_Drive
     */
    private $drive;

    /**
     * @var string
     */
    private $directoryId;

    /**
     * @param Google_Service_Drive $drive
     * @param string $directoryId
     */
    public function __construct(Google_Service_Drive $drive, $directoryId = NULL)
    {
        $this->drive = $drive;
        $this->directoryId = $directoryId;
    }

    /**
     * @param File $localFile
     * @param null $filename
     */
    public function upload(File $localFile, $filename = NULL)
    {
        $client = $this->drive->getClient();

        $file = new Google_Service_Drive_DriveFile();

        $file->title = $filename ?: $localFile->getFilename();

        if ($this->directoryId) {
            $parent = new Google_Service_Drive_ParentReference();
            $parent->setId($this->directoryId);
            $file->setParents([$parent]);
        }

        $existingFileId = NULL;
        $existingFiles = $this->drive->files->listFiles(['q' => "title = '{$filename}'", 'maxResults' => 1]);
        foreach ($existingFiles as $existingFile) {
            if ($existingFile instanceof Google_Service_Drive_DriveFile) {
                $existingFileId = $existingFile->getId();
            } else {
                //Temporary. Remove when investigated.
                \Registry::$container->get('loggers.cron_logger_psr')
                    ->alert('Unexpected data type in GoogleDriveUploader: ' . var_export($existingFile, TRUE));
            }
        }

        $client->setDefer(TRUE);
        if ($existingFileId) {
            $request = $this->drive->files->update($existingFileId, $file);
        } else {
            $request = $this->drive->files->insert($file);
        }

        $chunkSizeBytes = 1024 * 1024;
        $media = new Google_Http_MediaFileUpload(
            $client,
            $request,
            $localFile->getMimeType(),
            NULL,
            TRUE,
            $chunkSizeBytes
        );
        $media->setFileSize($localFile->getSize());

        $status = FALSE;
        $handle = fopen($localFile->getRealPath(), "rb");
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
        }

        fclose($handle);
    }
}
