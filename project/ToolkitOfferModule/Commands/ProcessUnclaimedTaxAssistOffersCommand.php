<?php

namespace ToolkitOfferModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Exception;
use Psr\Log\LoggerInterface;
use ToolkitOfferModule\Emailers\ToolkitOfferEmailer;
use ToolkitOfferModule\Services\ToolkitOfferService;

class ProcessUnclaimedTaxAssistOffersCommand extends CommandAbstract
{
    /**
     * @var ToolkitOfferService
     */
    private $service;

    /**
     * @var ToolkitOfferEmailer
     */
    private $emailer;

    public function __construct(
        ToolkitOfferService $service,
        ToolkitOfferEmailer $emailer,
        LoggerInterface $logger,
        INotifier $notifier
    ) {
        $this->service = $service;
        $this->emailer = $emailer;
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    public function execute()
    {
        try {
            $this->processUnclaimedTaxAssistOffers();
            $this->logger->info('TaxAssist offer mails have been sent.');
            $this->notifier->triggerSuccess(
                $this->getName(),
                time(),
                'CSV with unclaimed TaxAssist offers has been uploaded.'
            );
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->notifier->triggerFailure($this->getName(), time(), $e->getMessage());
        }
    }

    private function processUnclaimedTaxAssistOffers()
    {
        $offers = $this->service->getUnclaimedTaxAssistOffers();

        foreach ($offers as $offer) {
            $customer = $offer->getCompany()->getCustomer();
            $this->emailer->sendTaxAssistEmail($customer);
            $this->service->claim($offer);
        }
    }
}
