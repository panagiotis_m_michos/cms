<?php

namespace ToolkitOfferModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Exception;
use Psr\Log\LoggerInterface;
use ToolkitOfferModule\Exporters\CompanyToolkitOffersExporter;
use ToolkitOfferModule\Services\ToolkitOfferService;
use ToolkitOfferModule\Uploaders\GoogleDriveUploader;

class ProcessUnclaimedNamescoOffersCommand extends CommandAbstract
{
    /**
     * @var ToolkitOfferService
     */
    private $service;

    /**
     * @var CompanyToolkitOffersExporter
     */
    private $exporter;

    /**
     * @var GoogleDriveUploader
     */
    private $uploader;

    public function __construct(
        ToolkitOfferService $service,
        CompanyToolkitOffersExporter $exporter,
        GoogleDriveUploader $uploader,
        LoggerInterface $logger,
        INotifier $notifier
    ) {
        $this->service = $service;
        $this->exporter = $exporter;
        $this->uploader = $uploader;
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    public function execute()
    {
        try {
            $this->processUnclaimedNamescoOffers();
            $this->logger->info('CSV with unclaimed Namesco offers has been uploaded.');
            $this->notifier->triggerSuccess(
                $this->getName(),
                time(),
                'CSV with unclaimed Namesco offers has been uploaded.'
            );
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->notifier->triggerFailure($this->getName(), time(), $e->getMessage());
        }
    }

    private function processUnclaimedNamescoOffers()
    {
        $offers = $this->service->getUnclaimedFreeNamescoDomainOffers();
        $file = $this->exporter->exportCompanies($offers);

        $remoteFilename = 'export_' . date('Y-m-d') . '.csv';
        $this->uploader->upload($file, $remoteFilename);
        unlink($file->getPathname());

        foreach ($offers as $offer) {
            $this->service->claim($offer);
        }
    }
}
