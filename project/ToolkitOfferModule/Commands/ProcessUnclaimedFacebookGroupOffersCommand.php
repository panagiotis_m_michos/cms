<?php

namespace ToolkitOfferModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Exception;
use Psr\Log\LoggerInterface;
use ToolkitOfferModule\Emailers\ToolkitOfferEmailer;
use ToolkitOfferModule\Services\ToolkitOfferService;

class ProcessUnclaimedFacebookGroupOffersCommand extends CommandAbstract
{
    /**
     * @var ToolkitOfferService
     */
    private $service;

    /**
     * @var ToolkitOfferEmailer
     */
    private $emailer;

    public function __construct(
        ToolkitOfferService $service,
        ToolkitOfferEmailer $emailer,
        LoggerInterface $logger,
        INotifier $notifier
    ) {
        $this->service = $service;
        $this->logger = $logger;
        $this->notifier = $notifier;
        $this->emailer = $emailer;
    }

    public function execute()
    {
        try {
            $this->processUnclaimedFacebookGroupOffers();
            $this->logger->info('Facebook group email invites sent.');
            $this->notifier->triggerSuccess($this->getName(), time(), 'Facebook group email invites sent.');
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->notifier->triggerFailure($this->getName(), time(), $e->getMessage());
        }
    }

    private function processUnclaimedFacebookGroupOffers()
    {
        $offers = $this->service->getUnclaimedFacebookGroupOffers();

        foreach ($offers as $offer) {
            $this->emailer->sendFacebookGroupEmail($offer->getCompany()->getCustomer());
            $this->service->claim($offer);
        }
    }
}
