<?php

namespace ToolkitOfferModule\Commands;

use Cron\Commands\CommandAbstract;
use Cron\INotifier;
use Exception;
use Psr\Log\LoggerInterface;
use ToolkitOfferModule\Emailers\ToolkitOfferEmailer;
use ToolkitOfferModule\Services\ToolkitOfferService;

class ProcessUnclaimedEbookOffersCommand extends CommandAbstract
{
    /**
     * @var ToolkitOfferService
     */
    private $service;

    /**
     * @var ToolkitOfferEmailer
     */
    private $emailer;

    /**
     * @param ToolkitOfferService $service
     * @param ToolkitOfferEmailer $emailer
     * @param LoggerInterface $logger
     * @param INotifier $notifier
     */
    public function __construct(
        ToolkitOfferService $service,
        ToolkitOfferEmailer $emailer,
        LoggerInterface $logger,
        INotifier $notifier
    ) {
        $this->service = $service;
        $this->emailer = $emailer;
        $this->logger = $logger;
        $this->notifier = $notifier;
    }

    public function execute()
    {
        try {
            $this->processUnclaimedEbookOffers();
            $this->logger->info('Ebook offer mails have been sent.');
            $this->notifier->triggerSuccess(
                $this->getName(),
                time(),
                'Ebook offer mails have been sent.'
            );
        } catch (Exception $e) {
            $this->logger->error($e);
            $this->notifier->triggerFailure($this->getName(), time(), $e->getMessage());
        }
    }

    private function processUnclaimedEbookOffers()
    {
        $offers = $this->service->getUnclaimedEbookOffers();

        foreach ($offers as $offer) {
            $customer = $offer->getCompany()->getCustomer();
            $this->emailer->sendMakeMoreProfitGuideEmail($customer);
            $this->service->claim($offer);
        }
    }
}
