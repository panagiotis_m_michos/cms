<?php

namespace ToolkitOfferModule\Exporters;

use Goodby\CSV\Export\Standard\Exporter;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use Utils\File;

class CompanyToolkitOffersExporter
{
    /**
     * @var Exporter
     */
    private $csvExporter;

    /**
     * @var string
     */
    private $exportDirectory;

    public function __construct(Exporter $csvExporter, $exportDirectory)
    {
        $this->csvExporter = $csvExporter;
        $this->exportDirectory = $exportDirectory;
    }

    /**
     * @param CompanyToolkitOffer[] $companyToolkitOffers
     * @return File
     */
    public function exportCompanies(array $companyToolkitOffers)
    {
        $rows = [];

        foreach ($companyToolkitOffers as $companyToolkitOffer) {
            $company = $companyToolkitOffer->getCompany();
            $rows[] = [
                $company->getCompanyId(),
            ];
        }

        $filename = $this->exportDirectory . DIRECTORY_SEPARATOR . uniqid('companies_toolkit_offers_export') . '.csv';
        $this->csvExporter->export($filename, $rows);

        return new File($filename);
    }
}
