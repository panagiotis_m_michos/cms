<?php

namespace ToolkitOfferModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\Company;
use Entities\EntityAbstract;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Entity(repositoryClass="ToolkitOfferModule\Repositories\CompanyToolkitOfferRepository")
 * @Orm\Table(name="cms2_company_toolkit_offers")
 */
class CompanyToolkitOffer extends EntityAbstract
{
    /**
     * @var int
     *
     * @Orm\Column(name="companyToolkitOfferId", type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Company
     *
     * @Orm\ManyToOne(targetEntity="Entities\Company", inversedBy="toolkitOffers")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id")
     */
    private $company;

    /**
     * @var ToolkitOffer
     *
     * @Orm\ManyToOne(targetEntity="ToolkitOffer")
     * @Orm\JoinColumn(name="toolkitOfferId", referencedColumnName="toolkitOfferId")
     */
    private $toolkitOffer;

    /**
     * @var bool
     *
     * @Orm\Column(type="boolean")
     */
    private $selected = TRUE;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     */
    private $dateClaimed;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    /**
     * @param Company $company
     * @param ToolkitOffer $toolkitOffer
     */
    public function __construct(Company $company, ToolkitOffer $toolkitOffer)
    {
        $this->toolkitOffer = $toolkitOffer;
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return ToolkitOffer
     */
    public function getToolkitOffer()
    {
        return $this->toolkitOffer;
    }

    /**
     * @param ToolkitOffer $toolkitOffer
     */
    public function setToolkitOffer(ToolkitOffer $toolkitOffer)
    {
        $this->toolkitOffer = $toolkitOffer;
    }

    /**
     * @return bool
     */
    public function isSelected()
    {
        return $this->selected;
    }

    /**
     * @param bool $selected
     */
    public function setSelected($selected)
    {
        $this->selected = (bool) $selected;
    }

    /**
     * @return DateTime
     */
    public function getDateClaimed()
    {
        return $this->dateClaimed;
    }

    /**
     * @param DateTime $dateClaimed
     */
    public function setDateClaimed(DateTime $dateClaimed)
    {
        $this->dateClaimed = $dateClaimed;
    }

    /**
     * @param DateTime $dtc
     */
    public function setDtc(DateTime $dtc)
    {
        $this->dtc = $dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @param DateTime $dtm
     */
    public function setDtm(DateTime $dtm)
    {
        $this->dtm = $dtm;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }
}
