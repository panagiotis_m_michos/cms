<?php

namespace ToolkitOfferModule\Entities;

use DateTime;
use Doctrine\ORM\Mapping as Orm;
use Entities\EntityAbstract;

/**
 * @Orm\Entity(repositoryClass="ToolkitOfferModule\Repositories\ToolkitOfferRepository")
 * @Orm\Table(name="cms2_toolkit_offers")
 */
class ToolkitOffer extends EntityAbstract
{
    const TYPE_NAMESCO_DOMAIN = 'namescoDomain';
    const TYPE_ADWORDS_VOUCHER = 'adwordsVoucher';
    const TYPE_GOOGLE_APPS_DISCOUNT = 'googleAppsDiscount';
    const TYPE_TAX_CONSULTATION = 'taxConsultation';
    const TYPE_FREE_AGENT_TRIAL = 'freeAgentTrial';
    const TYPE_CREDIT_CHECKS = 'creditChecks';
    const TYPE_FACEBOOK_GROUP = 'facebookGroup';
    const TYPE_EBOOK = 'ebook';

    /**
     * @var int
     *
     * @Orm\Column(name="toolkitOfferId", type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Orm\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var string
     *
     * @Orm\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @Orm\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="dateValidFrom", type="date", nullable=true)
     */
    private $dateValidFrom;

    /**
     * @var DateTime
     *
     * @Orm\Column(name="dateValidTo", type="date", nullable=true)
     */
    private $dateValidTo;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return DateTime
     */
    public function getDateValidFrom()
    {
        return $this->dateValidFrom;
    }

    /**
     * @param DateTime $dateValidFrom
     */
    public function setDateValidFrom(DateTime $dateValidFrom = NULL)
    {
        $this->dateValidFrom = $dateValidFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateValidTo()
    {
        return $this->dateValidTo;
    }

    /**
     * @param DateTime $dateValidTo
     */
    public function setDateValidTo(DateTime $dateValidTo = NULL)
    {
        $this->dateValidTo = $dateValidTo;
    }
}
