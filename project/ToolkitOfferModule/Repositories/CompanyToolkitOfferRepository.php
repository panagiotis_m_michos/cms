<?php

namespace ToolkitOfferModule\Repositories;

use DateTime;
use Repositories\BaseRepository_Abstract;
use ToolkitOfferModule\Entities\CompanyToolkitOffer;
use ToolkitOfferModule\Entities\ToolkitOffer;

class CompanyToolkitOfferRepository extends BaseRepository_Abstract
{
    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedAdwordsVoucherOffers()
    {
        return $this->getUnclaimedOffersByType(ToolkitOffer::TYPE_ADWORDS_VOUCHER);
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedFreeNamescoDomainOffers()
    {
        return $this->getUnclaimedOffersByType(ToolkitOffer::TYPE_NAMESCO_DOMAIN);
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedGoogleAppsDiscountOffers()
    {
        return $this->getUnclaimedOffersByType(ToolkitOffer::TYPE_GOOGLE_APPS_DISCOUNT);
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedTaxConsultationOffers()
    {
        return $this->getUnclaimedOffersByType(ToolkitOffer::TYPE_TAX_CONSULTATION);
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedFreeAgentTrialOffers()
    {
        return $this->getUnclaimedOffersByType(ToolkitOffer::TYPE_FREE_AGENT_TRIAL);
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedEbookOffers()
    {
        return $this->getUnclaimedOffersByType(ToolkitOffer::TYPE_EBOOK);
    }

    /**
     * @return CompanyToolkitOffer[]
     */
    public function getUnclaimedFacebookGroupOffers()
    {
        return $this->getUnclaimedOffersByType(ToolkitOffer::TYPE_FACEBOOK_GROUP);
    }

    /**
     * @param string $type
     * @return CompanyToolkitOffer[]
     */
    private function getUnclaimedOffersByType($type)
    {
        return $this->createQueryBuilder('cto')
            ->join('cto.toolkitOffer', 'to')
            ->join('cto.company', 'c')
            ->where('cto.dateClaimed IS NULL')
            ->andWhere('cto.selected = TRUE')
            ->andWhere('c.incorporationDate < :yesterday')
            ->andWhere('to.type = :type')
            ->setParameter(':type', $type)
            ->setParameter(':yesterday', new DateTime('today'))
            ->getQuery()
            ->getResult();
    }
}
