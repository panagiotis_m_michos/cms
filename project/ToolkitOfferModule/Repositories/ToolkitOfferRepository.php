<?php

namespace ToolkitOfferModule\Repositories;

use Repositories\BaseRepository_Abstract;
use ToolkitOfferModule\Entities\ToolkitOffer;

class ToolkitOfferRepository extends BaseRepository_Abstract
{
    /**
     * @return ToolkitOffer[]
     */
    public function getAvailableOffers()
    {
        return $this->createQueryBuilder('to')
            ->where('CURRENT_DATE() >= to.dateValidFrom')
            ->andWhere('(CURRENT_DATE() <= to.dateValidTo OR to.dateValidTo IS NULL)')
            ->getQuery()
            ->getResult();
    }
}
