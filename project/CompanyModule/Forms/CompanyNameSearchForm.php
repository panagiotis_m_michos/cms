<?php

namespace CompanyModule\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class CompanyNameSearchForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('companyName', 'text', [
            'constraints' => [
                new NotBlank(),
                new CompanyName()
            ]
        ]);
    }

}