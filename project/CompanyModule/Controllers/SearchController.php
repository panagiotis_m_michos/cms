<?php

namespace CompanyModule\Controllers;

use Basket;
use CommonModule\Contracts\IRedirectionProvider;
use CompanyModule\Domain\Company\CompanyName;
use CompanyModule\Exceptions\ValidationException;
use CompanyModule\Redirections\SearchRedirection;
use CompanyModule\Renderers\SearchRenderer;
use CompanyModule\Storage\CompanyNameStorage;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class SearchController
{
    /**
     * @var CompanyNameStorage
     */
    private $companyNameStorage;

    /**
     * @var SearchRenderer
     */
    private $searchRenderer;

    /**
     * @var SearchRedirection
     */
    private $searchRedirection;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @param CompanyNameStorage $companyNameStorage
     * @param SearchRenderer $searchRenderer
     * @param IRedirectionProvider $searchRedirection
     * @param Basket $basket
     */
    public function __construct(
        CompanyNameStorage $companyNameStorage,
        SearchRenderer $searchRenderer,
        IRedirectionProvider $searchRedirection,
        Basket $basket
    )
    {
        $this->companyNameStorage = $companyNameStorage;
        $this->searchRenderer = $searchRenderer;
        $this->searchRedirection = $searchRedirection;
        $this->basket = $basket;
    }

    /**
     * old behaviour can be q or companyName
     * @param string|null $q
     * @param bool $proceed
     * @param string|null $companyName
     * @return RedirectResponse|Response
     */
    public function search($q = NULL, $proceed = FALSE, $companyName = NULL)
    {
        $searchingForName = !empty($q) ? $q : $companyName;
        if ($searchingForName) {
            $companyName = CompanyName::withLimited($searchingForName);
            if ($proceed) {
                try {
                    $this->companyNameStorage->setCompanyName($companyName);
                    return $this->searchRedirection->getRedirection($this->basket->getPackageOrNull());
                } catch (ValidationException $e) {
                    return $this->searchRenderer->renderAvailability($companyName);
                }
            } else {
                return $this->searchRenderer->renderAvailability($companyName);
            }
        }
        return $this->searchRenderer->renderSearchPage();
    }
}
