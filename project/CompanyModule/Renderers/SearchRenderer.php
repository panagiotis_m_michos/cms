<?php

namespace CompanyModule\Renderers;

use BaseControler;
use CompanyModule\Contracts\ICompanySearchGateway;
use CompanyModule\Domain\Company\CompanyName;
use CompanyModule\Exceptions\ValidationException;
use Exceptions\Business\CompaniesHouseException;
use FApplication;
use ReservedWord;
use RouterModule\Helpers\ControllerHelper;
use SearchControler;
use Symfony\Component\HttpFoundation\Response;
use TemplateModule\Renderers\IRenderer;
use TranslationModule\MsgId;
use Utils\File;

class SearchRenderer
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var ICompanySearchGateway
     */
    private $companySearchGateway;

    /**
     * @var IRenderer
     */
    private $renderer;

    /**
     * @param ControllerHelper $controllerHelper
     * @param ICompanySearchGateway $companySearchGateway
     * @param IRenderer $renderer
     */
    public function __construct(ControllerHelper $controllerHelper, ICompanySearchGateway $companySearchGateway, IRenderer $renderer)
    {
        $this->controllerHelper = $controllerHelper;
        $this->companySearchGateway = $companySearchGateway;
        $this->renderer = $renderer;
    }

    /**
     * @param CompanyName $companyName
     * @return Response
     */
    public function renderAvailability(CompanyName $companyName, $available = NULL)
    {
        try {
            $data['available'] = $available = $available === NULL ? $this->companySearchGateway->isAvailable($companyName) : $available;
            if (!$available) {
                $data['companies'] = $this->companySearchGateway->getSimiralNames($companyName);
            }
        } catch (ValidationException $e) {
        } catch (CompaniesHouseException $e) {
            $this->controllerHelper->setFlashMessage(MsgId::NAME_CHECK_NOT_AVAILABLE);
        }
        $data['companyName'] = $companyName;
        if ($available) {
            $data['form'] = $form = BaseControler::getCompanyNameForm(NULL);
            $form->setStorable(FALSE);
            $form['companyName']->setValue($companyName);
            $oldTemplate = 'companyName.tpl';
            FApplication::$nodeId = $data['nodeId'] = SearchControler::COMPANY_NAME;

        } else {
            $data['form'] = $form = BaseControler::getSearchForm();
            $form->setStorable(FALSE);
            $form['q']->setValue($companyName);
            FApplication::$nodeId = $data['nodeId'] = SearchControler::UNAVAILABLE_PAGE;
            $oldTemplate = 'unavailable.tpl';
        }
        $data['reserveWords'] = ReservedWord::match($companyName);
        return $this->renderer->renderTemplate(
            new File(__DIR__ . '/../../FrontModule/Templates/Search/' . $oldTemplate),
            $data
        );
    }

    /**
     * @return Response
     */
    public function renderSearchPage()
    {
        FApplication::$nodeId = $data['nodeId'] = SearchControler::COMPANY_NAME;
        $data['form'] = $form = BaseControler::getCompanyNameForm();
        $form->setStorable(FALSE);
        return $this->renderer->renderTemplate(
            new File(__DIR__ . '/../../FrontModule/Templates/Search/companyName.tpl'),
            $data
        );
    }
}


