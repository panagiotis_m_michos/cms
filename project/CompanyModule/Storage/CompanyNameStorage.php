<?php

namespace CompanyModule\Storage;

use Basket;
use CompanyModule\Contracts\ICompanySearchGateway;
use CompanyModule\Domain\Company\CompanyName;
use CompanyModule\Exceptions\ValidationException;
use Exceptions\Business\CompaniesHouseException;
use RouterModule\Helpers\ControllerHelper;
use SearchControler;
use TranslationModule\MsgId;

class CompanyNameStorage
{
    /**
     * @var ICompanySearchGateway
     */
    private $companySearchGateway;

    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @param ICompanySearchGateway $companySearchGateway
     * @param Basket $basket
     * @param ControllerHelper $controllerHelper
     */
    public function __construct(ICompanySearchGateway $companySearchGateway, Basket $basket, ControllerHelper $controllerHelper)
    {
        $this->companySearchGateway = $companySearchGateway;
        $this->basket = $basket;
        $this->controllerHelper = $controllerHelper;
    }

    /**
     * @param CompanyName $companyName
     * @throws ValidationException
     */
    public function setCompanyName(CompanyName $companyName)
    {
        try {
            if ($this->companySearchGateway->isAvailable($companyName)) {
                $this->basket->setIncorporationCompanyName($companyName);
                // @TODO remove once search is refactored
                SearchControler::setCompanyName((string)$companyName);
            } else {
                throw ValidationException::invalidName($companyName);
            }
        } catch (CompaniesHouseException $e) {
            $this->controllerHelper->setFlashMessage(MsgId::NAME_CHECK_NOT_AVAILABLE);
            $this->basket->setIncorporationCompanyName($companyName);
            // @TODO remove once search is refactored
            SearchControler::setCompanyName((string)$companyName);
        }
    }
}