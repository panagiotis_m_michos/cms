<?php

namespace CompanyModule\Entities;

use DateTime;
use Entities\Company;
use Doctrine\ORM\Mapping as Orm;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Orm\Table(name="cms2_company_settings")
 * @Orm\Entity(repositoryClass="CompanyModule\Repositories\CompanySettingsRepository")
 */
class CompanySetting
{
    const BANKING_OFFER = 'baking_offer_dismissed';

    /**
     * @var int
     * @Orm\Column(name="companySettingId", type="integer")
     * @Orm\Id
     * @Orm\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Company
     * @Orm\ManyToOne(targetEntity="Entities\Company", inversedBy="settings")
     * @Orm\JoinColumn(name="companyId", referencedColumnName="company_id", nullable=false)
     */
    private $company;

    /**
     * @var array
     * @Orm\Column(name="settingValue", type="json_array", nullable=true)
     */
    private $value;

    /**
     * @var string
     * @Orm\Column(name="settingKey")
     */
    private $key;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dtc;

    /**
     * @var DateTime
     *
     * @Orm\Column(type="datetime")
     * @Gedmo\Timestampable(on="create", on="update")
     */
    private $dtm;

    private function __construct()
    {
    }

    /**
     * @param Company $company
     * @return CompanySetting
     */
    public static function doNotShowBankOffer(Company $company)
    {
        $self = new self();
        $self->company = $company;
        $self->setEnabled(TRUE);
        $self->key = self::BANKING_OFFER;
        $company->addSetting($self);
        return $self;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->value === [TRUE];
    }

    /**
     * @param bool $enable
     */
    public function setEnabled($enable)
    {
        $this->value = [$enable ? TRUE : FALSE];
    }

    /**
     * @return DateTime
     */
    public function getDtc()
    {
        return $this->dtc;
    }

    /**
     * @return DateTime
     */
    public function getDtm()
    {
        return $this->dtm;
    }

    /**
     * @return bool
     */
    public function isBankingOffer()
    {
        return $this->key === self::BANKING_OFFER;
    }
}
