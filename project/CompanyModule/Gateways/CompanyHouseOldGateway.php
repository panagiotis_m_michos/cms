<?php

namespace CompanyModule\Gateways;

use CHXmlGateway;
use CompanyModule\Contracts\ICompanySearchGateway;
use CompanyModule\Domain\Company\CompanyName;
use CompanyModule\Exceptions\ValidationException;
use CompanyNameValidator;
use CompanySearch;
use Exceptions\Business\CompaniesHouseException;

class CompanyHouseOldGateway implements ICompanySearchGateway
{

    /**
     * @param CompanyName $companyName
     * @return string
     * @throws CompaniesHouseException
     * @throws ValidationException
     */
    public function isAvailable(CompanyName $companyName)
    {
        $error = CompanyNameValidator::checkName((string)$companyName);
        if ($error !== TRUE) {
            throw ValidationException::invalidError($error);
        }
        return CompanySearch::isAvailable((string)$companyName);
    }

    /**
     * @param CompanyName $companyName
     * @return array
     */
    public function getSimiralNames(CompanyName $companyName)
    {
        $companies = [];
        $xmlGateway = new CHXmlGateway();
        $nameSearch = $xmlGateway->getNameSearch((string) $companyName, 'LIVE');
        $nameSearch->setSearchRows(10);

        $res = $xmlGateway->getResponse($nameSearch);
        $xml = simplexml_load_string($res);

        if (isset($xml->Body->NameSearch->CoSearchItem)) {
            foreach ($xml->Body->NameSearch->CoSearchItem as $key => $val) {
                $companies[] = (array) $val;
            }
        }
        return $companies;
    }

}