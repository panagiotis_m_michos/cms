<?php

namespace CompanyModule\Contracts;

use CompanyModule\Domain\Company\CompanyName;

interface ICompanySearchGateway
{
    /**
     * @param CompanyName $companyName
     * @return TRUE
     */
    public function isAvailable(CompanyName $companyName);

    /**
     * @param CompanyName $companyName
     * @return array
     */
    public function getSimiralNames(CompanyName $companyName);
}