<?php

namespace CompanyModule\Exceptions;

use CompanyModule\Domain\Company\CompanyName;
use RuntimeException;
use TranslationModule\MsgId;

class ValidationException extends RuntimeException
{
    /**
     * @param string $error
     * @return ValidationException
     */
    public static function invalidError($error)
    {
        return new self($error);
    }

    /**
     * @param CompanyName $companyName
     * @return ValidationException
     */
    public static function invalidName(CompanyName $companyName)
    {
        return new self(sprintf(MsgId::COMPANY_NAME_NOT_AVAILABLE, $companyName));
    }
}