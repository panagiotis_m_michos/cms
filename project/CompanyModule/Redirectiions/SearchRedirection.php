<?php

namespace CompanyModule\Redirections;

use CommonModule\Contracts\IRedirectionProvider;
use Package;
use RouterModule\Helpers\ControllerHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;

final class SearchRedirection implements IRedirectionProvider
{
    /**
     * @var ControllerHelper
     */
    private $controllerHelper;

    /**
     * @var string
     */
    private $packageUrl;

    /**
     * @param ControllerHelper $controllerHelper
     * @param string $packageUrl
     */
    public function __construct(ControllerHelper $controllerHelper, $packageUrl)
    {
        $this->controllerHelper = $controllerHelper;
        $this->packageUrl = $packageUrl;
    }

    /**
     * @param Package $package
     * @return RedirectResponse
     */
    public function getRedirection(Package $package = NULL)
    {
        if (!$package) {
            return $this->controllerHelper->redirectionTo($this->packageUrl);
        }
        return $this->controllerHelper->redirectionTo('basket_module_package_basket_upgrade', ['package_id' => $package->getId()]);
    }
}