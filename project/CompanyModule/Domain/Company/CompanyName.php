<?php

namespace CompanyModule\Domain\Company;

final class CompanyName
{
    /**
     * @var string
     */
    private $companyName;

    /**
     * @param string $string
     * @return CompanyName
     */
    public static function uppercased($string)
    {
        $self = new self();
        $self->companyName = mb_strtoupper(trim($string));
        return $self;
    }

    /**
     * @param string $companyName
     * @return CompanyName
     */
    public static function withLimited($companyName)
    {
        $companyName = trim($companyName);
        if (!preg_match('#(LTD|LIMITED|CYF|CIC|CYFYNGEDIG)\.?$#i', $companyName)) {
            return self::uppercased(sprintf("%s LTD", $companyName));
        }
        return self::uppercased($companyName);
    }

    public function __toString()
    {
        return $this->companyName;
    }
}
