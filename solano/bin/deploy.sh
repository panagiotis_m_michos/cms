#!/bin/bash

set -o errexit -o pipefail # Exit on error

#install ansible
virtualenv env
pip install --upgrade setuptools
pip install ansible

#run ansible
echo -e "$msg_hosts" > hosts
ansible-playbook -u developer -e "DEPLOY_TO_HOST=$DEPLOY_TO_HOST commit_to_deploy=$TDDIUM_CURRENT_COMMIT project_folder=$SOLANO_DEPLOY_PROJECT_FOLDER" -vvvv -i hosts solano/bin/deploy.yml
