#!/bin/bash

### Ensure failed commands result in a failed Solano CI build ###
set -o errexit -o pipefail

# only for passed builds
if [[ "passed" == "${TDDIUM_BUILD_STATUS}" ]]; then
    # upload_build.sh bucket filename
    solano/bin/upload_build.sh madesimplegroup-static cm_approved_builds.log
    exit
fi