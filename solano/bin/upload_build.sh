#!/usr/bin/env bash

# Usage:
# 1. Set AWS environment variables as below
# 2. Invocation:
#    upload_build.sh bucket filename
#
# Set AWS environment variables
#   See http://docs.solanolabs.com/Setup/setting-environment-variables/
#
# solano config:add org UPLOAD_AWS_ACCESS_KEY_ID <key id>
# solano config:add org UPLOAD_AWS_SECRET_ACCESS_KEY <access key>
# solano config:add org UPLOAD_AWS_DEFAULT_REGION <default region>

which aws
if [ $? -ne 0 ]
then
  echo "Installing aws-cli"
  date

  pip install --install-option="--prefix=$HOME" awscli

  echo "Installation of aws-cli complete"
  date
fi

if [ -d $HOME/lib/python2.7/site-packages ]
then
  export PYTHONPATH=$HOME/lib/python2.7/site-packages
fi

export AWS_ACCESS_KEY_ID=${UPLOAD_AWS_ACCESS_KEY_ID:-${AWS_ACCESS_KEY_ID}}
export AWS_SECRET_ACCESS_KEY=${UPLOAD_AWS_SECRET_ACCESS_KEY:-${AWS_SECRET_ACCESS_KEY}}
export AWS_DEFAULT_REGION=${UPLOAD_AWS_DEFAULT_REGION:-${AWS_DEFAULT_REGION}}

if [ -z "$1" ] || [ -z "$2" ]
then
  echo "You must specify bucket and filename arguments. Usage: upload_build.sh bucket filename" 1>&2
  exit 1
fi

bucket=$1
filename=$2
branch=${TDDIUM_CURRENT_BRANCH}
commit=${TDDIUM_CURRENT_COMMIT}
local_target=${filename}
s3_target="s3://$bucket/$filename"

# create local file if does not exists on s3, copy otherwise
count=`aws s3 ls ${s3_target} | wc -l`
if [[ ${count} -gt 0 ]]; then
    aws s3 cp ${s3_target} ${local_target}
else
    touch ${local_target}
fi

# prepend commit to file
echo "$branch $commit"|cat - ${local_target} > "$local_target.tmp" && mv "$local_target.tmp" ${local_target}

# copy to s3
aws s3 cp ${local_target} ${s3_target} --acl public-read

# remove temp file
rm ${local_target}