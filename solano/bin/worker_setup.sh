#!/bin/bash

set -o errexit -o pipefail # Catch script errors

# env variables
touch env.list
echo CMS__HOST=$HOST >> env.list
echo CMS__DATABASE__HOST=db >> env.list
echo CMS__DATABASE__USERNAME=$DB_USER >> env.list
echo CMS__DATABASE__PASSWORD=$DB_PASSWORD >> env.list
echo CMS__DATABASE__DATABASE=$DB_DATABASE >> env.list
echo BEHAT_PARAMS="{\"extensions\" : {\"Behat\\\\MinkExtension\" : {\"base_url\" : \"https://app/\", \"selenium2\" : {\"wd_host\" : \"selenium:4444/wd/hub\"}}}}" >> env.list

# create own network
sudo docker network create solano_net

# run mail
sudo docker run --name=mail --net=solano_net -d helder/mailcatcher
sleep 10

# run db
sudo docker run --name=db --net=solano_net -e MYSQL_ROOT_PASSWORD=$DB_PASSWORD -d mysql:5.5
sleep 20

# create db
sudo docker exec -i db mysql -u $DB_USER -p$DB_PASSWORD <<< "CREATE DATABASE ${DB_DATABASE};"
sudo docker exec -i db mysql -u $DB_USER -p$DB_PASSWORD $DB_DATABASE < storage/database/cms_dev.sql
sudo docker exec -i db mysql -u $DB_USER -p$DB_PASSWORD $DB_DATABASE < storage/database/data.sql

# run selenium
sudo docker run --name=selenium --net=solano_net -d selenium/standalone-firefox
sleep 10

# run app
sudo docker run --name=app --net=solano_net --env-file=env.list -w /var/www -i -d made_simple/cms_app
sleep 10

# run migration
sudo docker exec -i app php console/db migrate