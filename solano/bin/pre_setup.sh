#!/usr/bin/env bash

set -o errexit -o pipefail # Catch script errors

sudo docker build -f solano/Dockerfile -t made_simple/cms_app .
sudo docker pull helder/mailcatcher
sudo docker pull mysql:5.5
sudo docker pull selenium/standalone-firefox